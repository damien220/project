package Sabbah;

import Functions.SystemMotherBoardNumber;
import Functions.dbFunc;
import Functions.utilityclass;
import com.mysql.jdbc.Connection;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;

public class Login {

    private final SystemMotherBoardNumber sysMBnum = new SystemMotherBoardNumber();
    private Connection conn = null;
    private final dbFunc dbfunc;
    private final utilityclass utility = new utilityclass();
    private final String dbfilename = "dbcreds.config";

    public Login() {
        conn = utility.connect2DB(dbfilename);
        dbfunc = new dbFunc(conn);

    }

    public Boolean CheckUser(String username) throws SQLException {
        boolean success = false;
        if (!username.isEmpty()) {
            String where = "  username like '" + username + "'";
            Vector v = dbfunc.searchBycolumn("login", "hardware_hash", where);

            if (!v.isEmpty() && String.valueOf(createHashUserMB(username)).equals(v.get(0).toString())) {
                success = true;
            }
        }

        return success;
    }

    private Boolean CheckOldPass(char[] password) throws SQLException {
        boolean success = false;

        String Stringpassword = String.valueOf(password);
        int hash = Stringpassword.hashCode();
        String hashString = String.valueOf(hash);
        String where = "  hash like '" + hashString + "'";
        Vector v = dbfunc.searchBycolumn("login", "hash", where);
        if (!v.isEmpty()) {
            success = true;
        }
        return success;
    }

    private Boolean ChecknewPasstype(char[] pass1, char[] pass2) {
        boolean success = false;
        String Stringpassword1 = String.valueOf(pass1);
        String Stringpassword2 = String.valueOf(pass2);

        if (Stringpassword1.equals(Stringpassword2)) {
            success = true;
        }
        return success;
    }

    public Boolean ChangePassword(char[] oldpass, char[] pass1, char[] pass2) throws SQLException {
        boolean returnsuccess = false;
        boolean success = false;
        success = CheckOldPass(oldpass);
        if (success) {
            boolean success1 = ChecknewPasstype(pass1, pass2);
            if (success1) {
                boolean successupdate = ChangePass(oldpass, pass1);
                if (successupdate) {
                    JOptionPane.showMessageDialog(null, "تمت عملية تغيير كلمة المرور");
                    returnsuccess = true;
                } else {
                    JOptionPane.showMessageDialog(null, "خطأ أثناء عملية التعديل");
                }
            } else {
                JOptionPane.showMessageDialog(null, "كلمة المرور غير متطابقة");
            }
        } else {
            JOptionPane.showMessageDialog(null, "الرجاء التأكد من كلمة المرور القديمة");

        }
        return returnsuccess;
    }

    private Boolean ChangePass(char[] oldpassword, char[] passwordnew1) {
        boolean success = false;
        String Stringpassword = String.valueOf(passwordnew1);
        int hash = Stringpassword.hashCode();
        Object[] fields = {"hash"};
        Object[] vals = {hash};

        String Stringpasswordold = String.valueOf(oldpassword);
        int hashold = Stringpasswordold.hashCode();
        String hashStringold = String.valueOf(hashold);
        String where = " where hash like '" + hashStringold + "'";
        success = dbfunc.updateData("login", fields, vals, where);

        return success;
    }

    public Boolean CheckLogin(String username, char[] password) throws SQLException {
        boolean success = false;
        Vector v = dbfunc.searchBycolumn("login", "username,hash", " username like '" + username + "'");
        if (!v.isEmpty()) {
            if (v.get(0).toString().equals(username)) {
                String Stringpassword = String.valueOf(password);
                int hash = Stringpassword.hashCode();
                String hashString = String.valueOf(hash);

                if (v.get(1).toString().equals(hashString)) {
                    success = true;
                }
            }
        }
        return success;
    }

    private int createHashUserMB(String username) {
        String sysnum = sysMBnum.getSystemMotherBoard_SerialNumber();
        String toHash = username + "_" + sysnum;
        int hash = toHash.hashCode();
        return hash;
    }

}
