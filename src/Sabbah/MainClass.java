package Sabbah;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;

/**
 *
 * @author Freeware Sys
 */
public class MainClass {

    public static void main(String args[]) throws ClassNotFoundException, InstantiationException, AWTException, SQLException {
        MainFrame mf = new MainFrame();
        // call the initiale function from the mainframe class to launch the frame
        mf.initial();
        //mf.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        // get the dimensions of the screen
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        // set the bounds of the frame according to the resolution of the screen
        mf.setBounds(0, 0, screenSize.width, screenSize.height);
        mf.dispose();
        //mf.setUndecorated(true);
        mf.setResizable(false);
        mf.pack();
        mf.setLocationRelativeTo(null);
        //mf.initial();
        mf.setVisible(true);
        // used to get the dimensions of the components

    }
}
