/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sabbah;

import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

class ImageRenderer extends DefaultTableCellRenderer {

    JLabel lbl = new JLabel();

    ImageIcon icon = new ImageIcon(getClass().getResource("/Images/x-mark.png"));
    ImageIcon icon1 = new ImageIcon(getClass().getResource("/Images/check.png"));

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {
        //lbl.setText((String) value);
        if ((int) value == 0) {
            lbl.setIcon(icon);
        } else if ((int) value == 1) {
            lbl.setIcon(icon1);
        }

        return lbl;
    }
}
