package Sabbah;

import Functions.dbFunc;
import Functions.utilityclass;
import com.mysql.jdbc.Connection;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ItemsClass {

    private Connection conn = null;
    private final dbFunc dbfunc;
    private final utilityclass utility = new utilityclass();
    private final String dbfilename = "dbcreds.config";

    public ItemsClass() {
        conn = utility.connect2DB(dbfilename);
        dbfunc = new dbFunc(conn);
    }

    public void SelectItems(DefaultTableModel dtm, JTable tableName, String typeCondition, String nameCondition) {
        String columns = " bit.`صنف`,bit.`الاسم`,qty.`كمية متبقية`,bit.`السعر` ";
        String on = "on bit.`كود`= qty.`كود` and bit.`صنف`  = qty.`صنف`";
        String where = whereCondition(typeCondition, nameCondition);

        utility.resetTable(dtm);

        dbfunc.selectData(dtm, tableName, columns, "bill_local_items as bit", where,
                "left join items_qty as qty", on, "", "", "");

    }

    private String whereCondition(String typeCondition, String nameCondition) {
        String where = null;

        if (where.isEmpty() && !typeCondition.isEmpty()) {
            where = " `صنف` like '" + typeCondition + "'";
        }
        if (where.isEmpty() && !nameCondition.isEmpty()) {
            where = " `الاسم` like '" + nameCondition + "'";
        }
        if (!where.isEmpty() && !nameCondition.isEmpty()) {
            where = " and  `صنف` like '" + nameCondition + "'";
        }
        return where;

    }

}
