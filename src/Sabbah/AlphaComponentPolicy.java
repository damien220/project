package Sabbah;

import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;

class AlphaComponentPolicy extends FocusTraversalPolicy {

    private SortedMap getSortedComponents(Container focusCycleRoot) {
        if (focusCycleRoot == null) {
            throw new IllegalArgumentException("focusCycleRoot can't be null");
        }
        SortedMap result = new TreeMap(); // Will sort all components by text.
        sortRecursive(result, focusCycleRoot);
        return result;
    }

    private void sortRecursive(Map components, Container container) {
        for (int i = 0; i < container.getComponentCount(); i++) {
            Component c = container.getComponent(i);
            if (c instanceof JTextField) { // Found another component to sort.
                components.put(((JTextField) c).getClientProperty("id"), c);
            }
            if (c instanceof JFormattedTextField) { // Found another component to sort.
                components.put(((JFormattedTextField) c).getClientProperty("id"), c);
            }
            if (c instanceof Container) { // Found a container to search.
                sortRecursive(components, (Container) c);
            }
        }
    }

    // The rest of the code implements the FocusTraversalPolicy interface.
    @Override
    public Component getFirstComponent(Container focusCycleRoot) {
        SortedMap components = getSortedComponents(focusCycleRoot);
        if (components.isEmpty()) {
            return null;
        }
        return (Component) components.get(components.firstKey());
    }

    @Override
    public Component getLastComponent(Container focusCycleRoot) {
        SortedMap components = getSortedComponents(focusCycleRoot);
        if (components.isEmpty()) {
            return null;
        }
        return (Component) components.get(components.lastKey());
    }

    @Override
    public Component getDefaultComponent(Container focusCycleRoot) {
        return getFirstComponent(focusCycleRoot);
    }

    @Override
    public Component getComponentAfter(Container focusCycleRoot,
            Component aComponent) {
        if (!(aComponent instanceof JTextField)) {
            return null;
        }
        SortedMap components = getSortedComponents(focusCycleRoot);
        // Find all components after the current one.
        String nextName = ((JTextField) aComponent).getClientProperty("id") + "\0";
        SortedMap nextComponents = components.tailMap(nextName);
        if (nextComponents.isEmpty()) { // Wrapped back to beginning
            if (!components.isEmpty()) {
                return (Component) components.get(components.firstKey());
            }
            return null; // Degenerate case of no components.
        }
        return (Component) nextComponents.get(nextComponents.firstKey());
    }

    @Override
    public Component getComponentBefore(Container focusCycleRoot,
            Component aComponent) {
        if (!(aComponent instanceof JTextField)) {
            return null;
        }

        SortedMap components = getSortedComponents(focusCycleRoot);
        SortedMap prevComponents
                = // Find all components before this one.
                components.headMap(((JTextField) aComponent).getClientProperty("id"));
        if (prevComponents.isEmpty()) { // Wrapped back to end.
            if (!components.isEmpty()) {
                return (Component) components.get(components.lastKey());
            }
            return null; // Degenerate case of no components.
        }
        return (Component) prevComponents.get(prevComponents.lastKey());
    }
}
