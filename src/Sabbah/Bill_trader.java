package Sabbah;

import Functions.dbFunc;
import Functions.utilityclass;
import com.mysql.jdbc.Connection;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Bill_trader {

    private Connection conn = null;
    private dbFunc dbfunc = new dbFunc(conn);
    private utilityclass utility = new utilityclass();
    private ImageRenderer img = new ImageRenderer();

    private void showBillt(JPanel[] panCard, JTable tableName, DefaultTableModel dtm, JTable table, CardLayout cl) {
        String column = " * ";
        String tablename = "bill_trader";

        utility.resetTable(dtm);
        dbfunc.selectData(dtm, tableName, column, tablename, "", "", "", "", "", "");

        panCard[1].setVisible(false);
        cl.show(panCard[0], "bill_trader");
        table.setDefaultEditor(Object.class, null);
    }
}
