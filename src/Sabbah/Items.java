/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sabbah;

import Functions.dbFunc;
import Functions.utilityclass;
import com.mysql.jdbc.Connection;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Items {

    private Connection conn = null;
    private final dbFunc dbfunc;

    private final utilityclass utility = new utilityclass();
    private final String dbfilename = "dbcreds.config";

    public Items() {
        conn = utility.connect2DB(dbfilename);
        dbfunc = new dbFunc(conn);
    }

    public void selectItems(DefaultTableModel dtm, JTable tableName, String where) {
        String columns = " bli.`الصنف`,bli.`الاسم`,bli.`سعر المبيع`,qty.`كمية متبقية`";
        String on = "on bli.`الكود`= qty.`الكود` and bli.`الصنف`= qty.`الصنف`";
        //where = " qty.`كمية متبقية` >= 0 ";
        utility.resetTable(dtm);
        dbfunc.selectData(dtm, tableName, columns, "bill_local_items as bli",
                where, "left join items_qty as qty", on, "", "", "");
        utility.setupTable(dtm, tableName);
        tableName.setDefaultEditor(Object.class, null);

    }

    public void selectItemsByCombo(DefaultTableModel dtm, JTable tableName, String valc1, String valc2) {
        String where = createWhereStatement(valc1, valc2);
        selectItems(dtm, tableName, where);
    }

    public void fillCombo(JComboBox combo, String column, String where) throws SQLException {
        dbfunc.refreshCB(combo, "bill_local_items", column);
    }

    public void searchItemsQty(DefaultTableModel dtm, JTable tableName, String value) {
        String where = "";
        if (!value.isEmpty()) {
            where = " qty.`كمية متبقية` <= " + value + "";
        }
        selectItems(dtm, tableName, where);
    }

    private String createWhereStatement(String valc1, String valc2) {
        String where = "";
        if (!valc1.isEmpty()) {
            where = " bli.`الصنف` like '" + valc1 + "'";
        }
        if (!valc2.isEmpty()) {
            where = where.isEmpty() ? " bli.`الاسم` like '" + valc2 + "'"
                    : where + " and bli.`الاسم` like '" + valc2 + "'";
        }
        return where;
    }

}
