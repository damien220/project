package Sabbah;

import Functions.CmpFunctions;
import Functions.dbFunc;
import Functions.utilityclass;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.awt.AWTException;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.NumberFormatter;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

public class MainFrame extends javax.swing.JFrame {

    // define the txt file of db creds
    private final String dbfilename = "dbcreds.config";
    private ArrayList<String> listname = new ArrayList<>();
    //define dtms for the tables
    private DefaultTableModel dtm = null, dtm1 = null, dtm2 = null, dtm3 = null, dtm4 = null,
            dtm5 = null, dtm6 = null, dtm7 = null, dtm8 = null;
    //define a cardLayout to use it for card panel
    private CardLayout cl = null, cl1 = null, cl2 = null, cl3 = null, cl4 = null, cl5 = null, cl6 = null,
            cl7 = null, cl8 = null;

    NumberFormatter formatter;

    ComboBoxRenderer renderer = new ComboBoxRenderer();

    SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");

    private Connection conn = null;

    //create instances for functions
    private final Login login = new Login();
    private final Reports report = new Reports();
    private final Bill_si billsi = new Bill_si();
    private final Items items = new Items();
    private final AllpurposeFunctions allpurpose = new AllpurposeFunctions();
    private final CmpFunctions cmp = new CmpFunctions();
    private final utilityclass utility = new utilityclass();
    private final Backup bck = new Backup();
    private final dbFunc dbfunc;
    //private AlphaComponentPolicy traverse = new AlphaComponentPolicy();

    public MainFrame() throws AWTException, SQLException {
        //AutoCompleteDecorator.decorate(txt_addbills_invoicenb, strings, true);
        //AutoCompleteDecorator.decorate(combo_addbillti_type);
        initComponents();
        //pan_bill_trader_add.setFocusTraversalPolicy(new AlphaComponentPolicy());
        formatter = utility.formattextFieltd(formatter);

        //create the card layouts
        cl = (CardLayout) CardPanel.getLayout();
        cl1 = (CardLayout) pan_Bill_trader_card.getLayout();
        cl2 = (CardLayout) pan_Bill_shop_card.getLayout();
        cl3 = (CardLayout) pan_Billt_item_card.getLayout();
        cl4 = (CardLayout) pan_control_expenses_card.getLayout();
        cl5 = (CardLayout) pan_report.getLayout();
        cl6 = (CardLayout) pan_login_card.getLayout();
        cl7 = (CardLayout) pan_backup.getLayout();
        cl8 = (CardLayout) pan_backup_card.getLayout();

        dtm = (DefaultTableModel) table_bill_trader.getModel();
        dtm1 = (DefaultTableModel) table_bill_shop.getModel();
        dtm2 = (DefaultTableModel) table_billt_item.getModel();
        dtm3 = (DefaultTableModel) table_bills_item_shop.getModel();
        dtm4 = (DefaultTableModel) table_bills_item_add.getModel();
        dtm5 = (DefaultTableModel) table_expenses.getModel();
        dtm6 = (DefaultTableModel) table_report_bills.getModel();
        dtm7 = (DefaultTableModel) table_items_show.getModel();
        dtm8 = (DefaultTableModel) table_items.getModel();

        cmp.cmporientation(pan_Bill_trader_card);
        cmp.cmporientation(pan_Bill_shop_card);
        cmp.cmporientation(pan_Billt_item_card);
        cmp.cmporientation(pan_control_expenses_card);

        //utility.setupTable(dtm, table_bill_trader);
        //utility.setupTable(dtm1, table_bill_shop);
        //utility.setupTable(dtm2, table_billt_item);
        //utility.setupTable(dtm3, table_bills_item_shop);
        //utility.setupTable(dtm4, table_bills_item_add);
        // utility.setupTable(dtm5, table_expenses);
        //utility.setupTable(dtm6, table_report_bills);
        //utility.setupTable(dtm7, table_items_show);
        //utility.setupTable(dtm8, table_items);
        conn = utility.connect2DB(dbfilename);
        dbfunc = new dbFunc(conn);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_control_items = new javax.swing.JPanel();
        lbl_control_home = new javax.swing.JLabel();
        lbl_control_trader = new javax.swing.JLabel();
        lbl_control_shop = new javax.swing.JLabel();
        lbl_control_expenses = new javax.swing.JLabel();
        lbl_control_items = new javax.swing.JLabel();
        lbl_control_reports = new javax.swing.JLabel();
        lbl_control_backup = new javax.swing.JLabel();
        lbl_control_exit = new javax.swing.JLabel();
        CardPanel = new javax.swing.JPanel();
        pan_login = new javax.swing.JPanel();
        pan_login_card = new javax.swing.JPanel();
        pan_login_change = new javax.swing.JPanel();
        jLabel104 = new javax.swing.JLabel();
        jLabel105 = new javax.swing.JLabel();
        jLabel106 = new javax.swing.JLabel();
        jLabel107 = new javax.swing.JLabel();
        txt_change_pass1 = new javax.swing.JPasswordField();
        txt_change_pass3 = new javax.swing.JPasswordField();
        txt_change_pass2 = new javax.swing.JPasswordField();
        btn_change = new javax.swing.JButton();
        pan_login_main = new javax.swing.JPanel();
        lbl_passtate = new javax.swing.JLabel();
        jLabel101 = new javax.swing.JLabel();
        jLabel103 = new javax.swing.JLabel();
        txt_login_pass = new javax.swing.JPasswordField();
        jLabel109 = new javax.swing.JLabel();
        jLabel102 = new javax.swing.JLabel();
        txt_login_user = new javax.swing.JTextField();
        jLabel111 = new javax.swing.JLabel();
        jLabel84 = new javax.swing.JLabel();
        pan_main = new javax.swing.JPanel();
        lb_add_bill_trader = new javax.swing.JLabel();
        lb_add_bill_shop = new javax.swing.JLabel();
        lb_report = new javax.swing.JLabel();
        lb_add_expenses = new javax.swing.JLabel();
        jLabel80 = new javax.swing.JLabel();
        pan_bill_shop = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_bill_shop = new javax.swing.JTable();
        pan_bills_control = new javax.swing.JPanel();
        lbl_bills_add = new javax.swing.JLabel();
        lbl_bills_searchs = new javax.swing.JLabel();
        lbl_bills_edit = new javax.swing.JLabel();
        lbl_bills_delete = new javax.swing.JLabel();
        pan_Bill_shop_card = new javax.swing.JPanel();
        pan_bill_shop_add = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        txt_addbills_invoicenb = new javax.swing.JTextField();
        txt_addbills_name = new javax.swing.JTextField();
        txt_addbills_phone = new javax.swing.JFormattedTextField(formatter);
        area_addbills_add = new javax.swing.JTextArea();
        btn_addbills_add = new javax.swing.JButton();
        txt_addbills_total_real = new javax.swing.JFormattedTextField(formatter);
        txt_addbills_payed = new javax.swing.JFormattedTextField(formatter);
        txt_addbills_remain = new javax.swing.JFormattedTextField(formatter);
        date_addbills_date = new org.jdesktop.swingx.JXDatePicker();
        pan_bill_shop_search = new javax.swing.JPanel();
        btn_billssearch_search = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        check_billssearch1 = new javax.swing.JCheckBox();
        check_billssearch2 = new javax.swing.JCheckBox();
        check_billssearch3 = new javax.swing.JCheckBox();
        check_billssearch4 = new javax.swing.JCheckBox();
        txt_billssearch1 = new javax.swing.JTextField();
        txt_billssearch2 = new javax.swing.JTextField();
        txt_billssearch3 = new javax.swing.JTextField();
        datepicker_billssearch_from = new org.jdesktop.swingx.JXDatePicker();
        datepicker_billssearch_to = new org.jdesktop.swingx.JXDatePicker();
        jLabel72 = new javax.swing.JLabel();
        combo_bills_currency = new javax.swing.JComboBox();
        jLabel113 = new javax.swing.JLabel();
        pan_Bill_trader = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table_bill_trader = new javax.swing.JTable();
        pan_billt_control = new javax.swing.JPanel();
        lbl_billt_add = new javax.swing.JLabel();
        lbl_billt_search = new javax.swing.JLabel();
        lbl_billt_edit = new javax.swing.JLabel();
        lbl_billt_delete = new javax.swing.JLabel();
        pan_Bill_trader_card = new javax.swing.JPanel();
        pan_bill_trader_add = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        txt_addbillt_invoicenb = new javax.swing.JTextField();
        txt_addbillt_name = new javax.swing.JTextField();
        txt_addbillt_phone = new javax.swing.JFormattedTextField(formatter);
        txt_addbillt_total = new javax.swing.JFormattedTextField(formatter);
        txt_addbillt_payed = new javax.swing.JFormattedTextField(formatter);
        txt_addbillt_remain = new javax.swing.JFormattedTextField(formatter);
        area_billt_add = new javax.swing.JTextArea();
        btn_addbillt_add = new javax.swing.JButton();
        date_addbillt_date = new org.jdesktop.swingx.JXDatePicker();
        pan_bill_trader_search = new javax.swing.JPanel();
        btn_billtsearch_add = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        check_billtsearch1 = new javax.swing.JCheckBox();
        check_billtsearch2 = new javax.swing.JCheckBox();
        check_billtsearch3 = new javax.swing.JCheckBox();
        check_billtsearch4 = new javax.swing.JCheckBox();
        txt_billtsearch1 = new javax.swing.JTextField();
        txt_billtsearch2 = new javax.swing.JTextField();
        txt_billtsearch3 = new javax.swing.JTextField();
        datepicker_billtsearch_from = new org.jdesktop.swingx.JXDatePicker();
        datepicker_billtsearch_to = new org.jdesktop.swingx.JXDatePicker();
        jLabel68 = new javax.swing.JLabel();
        combo_billt_currency = new javax.swing.JComboBox();
        jLabel81 = new javax.swing.JLabel();
        pan_billt_items = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        table_billt_item = new javax.swing.JTable();
        pan_billti_control = new javax.swing.JPanel();
        lbl_billti_add = new javax.swing.JLabel();
        lbl_billti_search = new javax.swing.JLabel();
        lbl_billti_edit = new javax.swing.JLabel();
        lbl_billti_delete = new javax.swing.JLabel();
        pan_Billt_item_card = new javax.swing.JPanel();
        pan_billti_add = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        txt_addbillti_serial = new javax.swing.JTextField();
        combo_addbillti_type = new javax.swing.JComboBox();
        txt_addbillti_type = new javax.swing.JTextField();
        lbl_addbillti_type = new javax.swing.JLabel();
        txt_addbillti_name = new javax.swing.JTextField();
        txt_addbillti_total = new javax.swing.JFormattedTextField(formatter);
        txt_addbillti_discount = new javax.swing.JFormattedTextField(formatter);
        txt_addbillti_real_total = new javax.swing.JFormattedTextField(formatter);
        lbl_addbillti_unit = new javax.swing.JLabel();
        combo_addbillti_unit = new javax.swing.JComboBox();
        txt_addbillti_unit = new javax.swing.JTextField();
        txt_addbillti_qty = new javax.swing.JFormattedTextField(formatter);
        txt_addbillti_unitprice = new javax.swing.JFormattedTextField(formatter);
        jLabel44 = new javax.swing.JLabel();
        area_addbilli_add = new javax.swing.JTextArea();
        btn_addbillti_add = new javax.swing.JButton();
        pan_billti_search = new javax.swing.JPanel();
        btn_billti_search = new javax.swing.JButton();
        txt_billtisearch1 = new javax.swing.JTextField();
        txt_billtisearch2 = new javax.swing.JTextField();
        combo_billtisearch1 = new javax.swing.JComboBox();
        check_billtisearch1 = new javax.swing.JCheckBox();
        check_billtisearch2 = new javax.swing.JCheckBox();
        check_billtisearch3 = new javax.swing.JCheckBox();
        jLabel21 = new javax.swing.JLabel();
        lbl_billti_invoicenb = new javax.swing.JLabel();
        lbl_billti_confirm = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        combo_billti_currency = new javax.swing.JComboBox();
        jLabel88 = new javax.swing.JLabel();
        pan_bills_items = new javax.swing.JPanel();
        jLabel77 = new javax.swing.JLabel();
        combo_billsi_currency = new javax.swing.JComboBox();
        pan_billsi_search_add = new javax.swing.JPanel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        txt_billsi_add_search1 = new javax.swing.JTextField();
        txt_billsi_add_search2 = new javax.swing.JTextField();
        combo_billsi_add_search1 = new javax.swing.JComboBox();
        lbl_billsi_searchitem = new javax.swing.JLabel();
        pan_billsi_search_shop = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        txt_billsi_shop_search1 = new javax.swing.JTextField();
        txt_billsi_shop_search2 = new javax.swing.JTextField();
        combo_billsi_shop_search1 = new javax.swing.JComboBox();
        lbl_billsi_searchadd = new javax.swing.JLabel();
        lbl_billsi_add = new javax.swing.JLabel();
        lbl_billsi_delete = new javax.swing.JLabel();
        lbl_billsi_idbills = new javax.swing.JLabel();
        lbl_billsi_total = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jLabel65 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        table_bills_item_add = new javax.swing.JTable(dtm4);
        jScrollPane7 = new javax.swing.JScrollPane();
        table_bills_item_shop = new javax.swing.JTable(dtm3);
        btn_billsi_confirm = new javax.swing.JButton();
        jLabel87 = new javax.swing.JLabel();
        pan_expenses = new javax.swing.JPanel();
        combo_expenses_currency = new javax.swing.JComboBox();
        jLabel114 = new javax.swing.JLabel();
        pan_expenses_control = new javax.swing.JPanel();
        lbl_expenses_add = new javax.swing.JLabel();
        lbl_expenses_search = new javax.swing.JLabel();
        lbl_expenses_edit = new javax.swing.JLabel();
        lbl_expenses_delete = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        table_expenses = new javax.swing.JTable();
        pan_control_expenses_card = new javax.swing.JPanel();
        pan_control_expenses_add = new javax.swing.JPanel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        txt_addexp_invoicenb = new javax.swing.JTextField();
        txt_addexp_name = new javax.swing.JTextField();
        txt_addexp_phone = new javax.swing.JFormattedTextField(formatter);
        txt_addexp_type = new javax.swing.JTextField();
        txt_addexp_total = new javax.swing.JFormattedTextField(formatter);
        txt_addexp_payed = new javax.swing.JFormattedTextField(formatter);
        txt_addexp_remain = new javax.swing.JFormattedTextField(formatter);
        area_addexp_add = new javax.swing.JTextArea();
        btn_addexp_add = new javax.swing.JButton();
        date_addexp_date = new org.jdesktop.swingx.JXDatePicker();
        pan_control_expenses_search = new javax.swing.JPanel();
        check_searchexp_search1 = new javax.swing.JCheckBox();
        check_searchexp_search2 = new javax.swing.JCheckBox();
        check_searchexp_search3 = new javax.swing.JCheckBox();
        check_searchexp_search4 = new javax.swing.JCheckBox();
        txt_searchexp_search1 = new javax.swing.JTextField();
        txt_searchexp_search2 = new javax.swing.JTextField();
        txt_searchexp_search3 = new javax.swing.JTextField();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        btn_searchexp_search = new javax.swing.JButton();
        datepicker_searchexp_from = new org.jdesktop.swingx.JXDatePicker();
        datepicker_searchexp_to = new org.jdesktop.swingx.JXDatePicker();
        jLabel115 = new javax.swing.JLabel();
        pan_items = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        table_items_show = new javax.swing.JTable();
        lbl_item_search = new javax.swing.JLabel();
        pan_item_control_search = new javax.swing.JPanel();
        jLabel82 = new javax.swing.JLabel();
        jLabel108 = new javax.swing.JLabel();
        combo_item_search = new javax.swing.JComboBox<>();
        jLabel112 = new javax.swing.JLabel();
        combo_item_search1 = new javax.swing.JComboBox<>();
        jLabel110 = new javax.swing.JLabel();
        txt_item_search = new javax.swing.JTextField();
        jLabel92 = new javax.swing.JLabel();
        pan_report = new javax.swing.JPanel();
        jLabel97 = new javax.swing.JLabel();
        pan_report_payment = new javax.swing.JPanel();
        pan_report_payment_details = new javax.swing.JPanel();
        combo_report_year = new javax.swing.JComboBox();
        jLabel23 = new javax.swing.JLabel();
        datepicker_report_from = new org.jdesktop.swingx.JXDatePicker();
        jLabel27 = new javax.swing.JLabel();
        datepicker_report_to = new org.jdesktop.swingx.JXDatePicker();
        jLabel28 = new javax.swing.JLabel();
        pan_repot_payment_control = new javax.swing.JPanel();
        lbl_report_trader = new javax.swing.JLabel();
        lbl_report_shop = new javax.swing.JLabel();
        lbl_report_expenses = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        table_report_bills = new javax.swing.JTable();
        jLabel71 = new javax.swing.JLabel();
        lbl_report_payed = new javax.swing.JLabel();
        jLabel75 = new javax.swing.JLabel();
        lbl_report_total = new javax.swing.JLabel();
        lbl_report_remaining = new javax.swing.JLabel();
        jLabel73 = new javax.swing.JLabel();
        lbl_report_total_remaining = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        lbl_report_total_remaining_get = new javax.swing.JLabel();
        lbl_report_total_remaining_pay = new javax.swing.JLabel();
        jLabel76 = new javax.swing.JLabel();
        jLabel79 = new javax.swing.JLabel();
        lbl_report_total_paysell = new javax.swing.JLabel();
        lbl_report_total_sell = new javax.swing.JLabel();
        jLabel78 = new javax.swing.JLabel();
        jLabel63 = new javax.swing.JLabel();
        jLabel69 = new javax.swing.JLabel();
        lbl_report_total_payed = new javax.swing.JLabel();
        btn_report_year = new javax.swing.JButton();
        btn_report_month = new javax.swing.JButton();
        jLabel26 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        combo_report_payement_name = new javax.swing.JComboBox<>();
        jLabel99 = new javax.swing.JLabel();
        pan_report_main = new javax.swing.JPanel();
        jLabel56 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel98 = new javax.swing.JLabel();
        pan_report_item = new javax.swing.JPanel();
        combo_reportitems_search = new javax.swing.JComboBox();
        combo_reportitems_search1 = new javax.swing.JComboBox();
        jLabel34 = new javax.swing.JLabel();
        datepicker_reportitems_from = new org.jdesktop.swingx.JXDatePicker();
        jLabel39 = new javax.swing.JLabel();
        datepicker_reportitems_to = new org.jdesktop.swingx.JXDatePicker();
        jLabel24 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        pan_report_items_control = new javax.swing.JPanel();
        lbl_report_items_shop = new javax.swing.JLabel();
        lbl_report_items_trader = new javax.swing.JLabel();
        btn_reportitems_month = new javax.swing.JButton();
        btn_reportitems_year = new javax.swing.JButton();
        jLabel62 = new javax.swing.JLabel();
        jLabel67 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        table_items = new javax.swing.JTable();
        jLabel116 = new javax.swing.JLabel();
        pan_backup = new javax.swing.JPanel();
        pan_backup_main = new javax.swing.JPanel();
        jLabel93 = new javax.swing.JLabel();
        lbl_backup_back_all = new javax.swing.JLabel();
        lbl_backup_restore_all = new javax.swing.JLabel();
        pan_backup_card = new javax.swing.JPanel();
        pan_backup_card_backup_all = new javax.swing.JPanel();
        btn_backup = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txt_bckpath = new javax.swing.JTextField();
        jLabel96 = new javax.swing.JLabel();
        pan_backup_card_restore_all = new javax.swing.JPanel();
        btn_restore = new javax.swing.JButton();
        jLabel25 = new javax.swing.JLabel();
        txt_restorepath = new javax.swing.JTextField();
        jLabel117 = new javax.swing.JLabel();
        jLabel95 = new javax.swing.JLabel();
        pan_Background = new javax.swing.JPanel();
        jLabel100 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pan_control_items.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pan_control_items.setOpaque(false);
        pan_control_items.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_control_home.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/home-icon.png"))); // NOI18N
        lbl_control_home.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_control_home.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_control_home.setName("0"); // NOI18N
        lbl_control_home.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_control_homeMouseClicked(evt);
            }
        });
        pan_control_items.add(lbl_control_home, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 10, -1, 80));

        lbl_control_trader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_control_trader.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/trader-icon.png"))); // NOI18N
        lbl_control_trader.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_control_trader.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_control_trader.setName("1"); // NOI18N
        lbl_control_trader.setPreferredSize(new java.awt.Dimension(80, 80));
        lbl_control_trader.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_control_traderMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_control_traderMouseEntered(evt);
            }
        });
        pan_control_items.add(lbl_control_trader, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 10, 98, 80));

        lbl_control_shop.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_control_shop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/shop-icon.png"))); // NOI18N
        lbl_control_shop.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_control_shop.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_control_shop.setName("2"); // NOI18N
        lbl_control_shop.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_control_shopMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_control_shopMouseEntered(evt);
            }
        });
        pan_control_items.add(lbl_control_shop, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 10, 98, 80));

        lbl_control_expenses.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_control_expenses.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/expenses-icon.png"))); // NOI18N
        lbl_control_expenses.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_control_expenses.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_control_expenses.setName("3"); // NOI18N
        lbl_control_expenses.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_control_expensesMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_control_expensesMouseEntered(evt);
            }
        });
        pan_control_items.add(lbl_control_expenses, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 10, 98, 80));

        lbl_control_items.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_control_items.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/item-icon.png"))); // NOI18N
        lbl_control_items.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_control_items.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_control_items.setName("4"); // NOI18N
        lbl_control_items.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_control_itemsMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_control_itemsMouseEntered(evt);
            }
        });
        pan_control_items.add(lbl_control_items, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 10, 98, 80));

        lbl_control_reports.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_control_reports.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/reports-icon.png"))); // NOI18N
        lbl_control_reports.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_control_reports.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_control_reports.setName("5"); // NOI18N
        lbl_control_reports.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_control_reportsMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_control_reportsMouseEntered(evt);
            }
        });
        pan_control_items.add(lbl_control_reports, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, 98, 80));

        lbl_control_backup.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_control_backup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/backup-icon.png"))); // NOI18N
        lbl_control_backup.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_control_backup.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_control_backup.setName("6"); // NOI18N
        lbl_control_backup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_control_backupMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_control_backupMouseEntered(evt);
            }
        });
        pan_control_items.add(lbl_control_backup, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, 98, 80));

        lbl_control_exit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_control_exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/exit-icon.png"))); // NOI18N
        lbl_control_exit.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_control_exit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_control_exit.setName("6"); // NOI18N
        lbl_control_exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_control_exitMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_control_exitMouseEntered(evt);
            }
        });
        pan_control_items.add(lbl_control_exit, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, 80));

        getContentPane().add(pan_control_items, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 16, 1110, 100));

        CardPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        CardPanel.setLayout(new java.awt.CardLayout());

        pan_login_card.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pan_login_card.setOpaque(false);
        pan_login_card.setPreferredSize(new java.awt.Dimension(400, 312));
        pan_login_card.setLayout(new java.awt.CardLayout());

        pan_login_change.setOpaque(false);
        pan_login_change.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel104.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel104.setText("تأكيد كلمة المرور :");
        pan_login_change.add(jLabel104, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 190, 130, 30));

        jLabel105.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel105.setText("كلمة المرور القديمة:");
        pan_login_change.add(jLabel105, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 90, 130, 30));

        jLabel106.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel106.setText("كلمة المرور الجديدة:");
        pan_login_change.add(jLabel106, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 140, 130, 30));

        jLabel107.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        jLabel107.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel107.setText("تغيير كلمة المرور:");
        pan_login_change.add(jLabel107, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 20, 180, 40));
        pan_login_change.add(txt_change_pass1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 90, 210, 40));
        pan_login_change.add(txt_change_pass3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 190, 210, 40));
        pan_login_change.add(txt_change_pass2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 140, 210, 40));

        btn_change.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btn_change.setText("موافق");
        btn_change.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_changeActionPerformed(evt);
            }
        });
        pan_login_change.add(btn_change, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 250, 100, 40));

        pan_login_card.add(pan_login_change, "card3");

        pan_login_main.setOpaque(false);
        pan_login_main.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_passtate.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_passtate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_passtate.setText("تسجيل الدخول");
        lbl_passtate.setVisible(false);
        pan_login_main.add(lbl_passtate, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 50, 270, 40));

        jLabel101.setFont(new java.awt.Font("Traditional Arabic", 0, 14)); // NOI18N
        jLabel101.setText("تغيير كلمة المرور؟");
        jLabel101.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel101.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel101MouseClicked(evt);
            }
        });
        pan_login_main.add(jLabel101, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 210, 130, 50));

        jLabel103.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel103.setText("المستخدم");
        pan_login_main.add(jLabel103, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 90, 110, 50));

        txt_login_pass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_login_passKeyPressed(evt);
            }
        });
        pan_login_main.add(txt_login_pass, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 160, 220, 50));

        jLabel109.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel109.setText("كلمة المرور");
        pan_login_main.add(jLabel109, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 150, 110, 50));

        jLabel102.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/arrow1.png"))); // NOI18N
        jLabel102.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel102.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel102MouseClicked(evt);
            }
        });
        pan_login_main.add(jLabel102, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, 60, -1));

        txt_login_user.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_login_userFocusLost(evt);
            }
        });
        pan_login_main.add(txt_login_user, new org.netbeans.lib.awtextra.AbsoluteConstraints(82, 100, 220, 50));

        pan_login_card.add(pan_login_main, "card2");

        jLabel111.setFont(new java.awt.Font("Lucida Grande", 0, 10)); // NOI18N
        jLabel111.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel111.setText("رجوع >");
        jLabel111.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel111.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel111MouseClicked(evt);
            }
        });

        jLabel84.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/login-background.jpg"))); // NOI18N

        javax.swing.GroupLayout pan_loginLayout = new javax.swing.GroupLayout(pan_login);
        pan_login.setLayout(pan_loginLayout);
        pan_loginLayout.setHorizontalGroup(
            pan_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_loginLayout.createSequentialGroup()
                .addGap(378, 378, 378)
                .addComponent(jLabel111)
                .addContainerGap(701, Short.MAX_VALUE))
            .addGroup(pan_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_loginLayout.createSequentialGroup()
                    .addContainerGap(360, Short.MAX_VALUE)
                    .addComponent(pan_login_card, javax.swing.GroupLayout.PREFERRED_SIZE, 483, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(267, Short.MAX_VALUE)))
            .addGroup(pan_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pan_loginLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel84, javax.swing.GroupLayout.PREFERRED_SIZE, 1110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        pan_loginLayout.setVerticalGroup(
            pan_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_loginLayout.createSequentialGroup()
                .addGap(206, 206, 206)
                .addComponent(jLabel111)
                .addContainerGap(499, Short.MAX_VALUE))
            .addGroup(pan_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_loginLayout.createSequentialGroup()
                    .addContainerGap(194, Short.MAX_VALUE)
                    .addComponent(pan_login_card, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(203, Short.MAX_VALUE)))
            .addGroup(pan_loginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pan_loginLayout.createSequentialGroup()
                    .addGap(0, 4, Short.MAX_VALUE)
                    .addComponent(jLabel84, javax.swing.GroupLayout.PREFERRED_SIZE, 710, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 4, Short.MAX_VALUE)))
        );

        pan_login_card.add(pan_login_main,"login_main");
        pan_login_card.add(pan_login_change,"login_change");

        CardPanel.add(pan_login, "card10");

        pan_main.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb_add_bill_trader.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lb_add_bill_trader.setForeground(new java.awt.Color(255, 255, 255));
        lb_add_bill_trader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_add_bill_trader.setText("مشتريات");
        lb_add_bill_trader.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lb_add_bill_trader.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb_add_bill_trader.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb_add_bill_traderMouseClicked(evt);
            }
        });
        pan_main.add(lb_add_bill_trader, new org.netbeans.lib.awtextra.AbsoluteConstraints(265, 112, 234, 121));

        lb_add_bill_shop.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lb_add_bill_shop.setForeground(new java.awt.Color(255, 255, 255));
        lb_add_bill_shop.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_add_bill_shop.setText("مبيعات");
        lb_add_bill_shop.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lb_add_bill_shop.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb_add_bill_shop.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb_add_bill_shopMouseClicked(evt);
            }
        });
        pan_main.add(lb_add_bill_shop, new org.netbeans.lib.awtextra.AbsoluteConstraints(725, 112, 234, 121));

        lb_report.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lb_report.setForeground(new java.awt.Color(255, 255, 255));
        lb_report.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_report.setText("تقارير");
        lb_report.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lb_report.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb_report.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb_reportMouseClicked(evt);
            }
        });
        pan_main.add(lb_report, new org.netbeans.lib.awtextra.AbsoluteConstraints(265, 336, 234, 121));

        lb_add_expenses.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lb_add_expenses.setForeground(new java.awt.Color(255, 255, 255));
        lb_add_expenses.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb_add_expenses.setText("مصاريف");
        lb_add_expenses.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lb_add_expenses.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb_add_expenses.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb_add_expensesMouseClicked(evt);
            }
        });
        pan_main.add(lb_add_expenses, new org.netbeans.lib.awtextra.AbsoluteConstraints(725, 336, 234, 121));

        jLabel80.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_main.add(jLabel80, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 710));

        CardPanel.add(pan_main, "card2");

        pan_bill_shop.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        table_bill_shop.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        table_bill_shop.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        table_bill_shop.getTableHeader().setReorderingAllowed(false);
        table_bill_shop.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_bill_shopMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(table_bill_shop);

        pan_bill_shop.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 182, 1082, 520));

        pan_bills_control.setOpaque(false);
        pan_bills_control.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_bills_add.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_bills_add.setForeground(new java.awt.Color(255, 255, 255));
        lbl_bills_add.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_bills_add.setText("إضافة فاتورة");
        lbl_bills_add.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_bills_add.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_bills_add.setName("0"); // NOI18N
        lbl_bills_add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_bills_addMouseClicked(evt);
            }
        });
        pan_bills_control.add(lbl_bills_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, 99, 27));

        lbl_bills_searchs.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_bills_searchs.setForeground(new java.awt.Color(255, 255, 255));
        lbl_bills_searchs.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_bills_searchs.setText("بحث");
        lbl_bills_searchs.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_bills_searchs.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_bills_searchs.setName("1"); // NOI18N
        lbl_bills_searchs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_bills_searchsMouseClicked(evt);
            }
        });
        pan_bills_control.add(lbl_bills_searchs, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 10, 99, 27));

        lbl_bills_edit.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_bills_edit.setForeground(new java.awt.Color(255, 255, 255));
        lbl_bills_edit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_bills_edit.setText("تعديل");
        lbl_bills_edit.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_bills_edit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_bills_edit.setName("2"); // NOI18N
        lbl_bills_edit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_bills_editMouseClicked(evt);
            }
        });
        pan_bills_control.add(lbl_bills_edit, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 10, 99, 27));

        lbl_bills_delete.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_bills_delete.setForeground(new java.awt.Color(255, 255, 255));
        lbl_bills_delete.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_bills_delete.setText("حذف");
        lbl_bills_delete.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_bills_delete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_bills_delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_bills_deleteMouseClicked(evt);
            }
        });
        pan_bills_control.add(lbl_bills_delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 99, 27));

        pan_bill_shop.add(pan_bills_control, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 10, 470, 40));

        pan_Bill_shop_card.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_Bill_shop_card.setOpaque(false);
        pan_Bill_shop_card.setLayout(new java.awt.CardLayout());

        pan_bill_shop_add.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_bill_shop_add.setOpaque(false);
        pan_bill_shop_add.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(255, 255, 255));
        jLabel30.setText("رقم الفاتورة");
        pan_bill_shop_add.add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 20, -1, -1));

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(255, 255, 255));
        jLabel31.setText("المشتري");
        pan_bill_shop_add.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 50, -1, -1));

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(255, 255, 255));
        jLabel32.setText("التاريخ");
        pan_bill_shop_add.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 20, -1, 30));

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(255, 255, 255));
        jLabel33.setText("الهاتف");
        pan_bill_shop_add.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 80, -1, -1));

        jLabel35.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(255, 255, 255));
        jLabel35.setText("المدفوع");
        pan_bill_shop_add.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 50, -1, -1));

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(255, 255, 255));
        jLabel36.setText("تفاصيل:");
        pan_bill_shop_add.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 10, -1, -1));

        jLabel37.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel37.setForeground(new java.awt.Color(255, 255, 255));
        jLabel37.setText("المتبقي");
        pan_bill_shop_add.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 80, -1, -1));

        jLabel38.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel38.setForeground(new java.awt.Color(255, 255, 255));
        jLabel38.setText("المجموع");
        pan_bill_shop_add.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 20, -1, -1));

        txt_addbills_invoicenb.setEnabled(false);
        pan_bill_shop_add.add(txt_addbills_invoicenb, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 20, 90, 20));
        pan_bill_shop_add.add(txt_addbills_name, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 50, 120, 20));

        txt_addbills_phone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        pan_bill_shop_add.add(txt_addbills_phone, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 80, 120, 20));

        area_addbills_add.setColumns(20);
        area_addbills_add.setRows(5);
        area_addbills_add.setText(" ");
        pan_bill_shop_add.add(area_addbills_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, 270, 90));

        btn_addbills_add.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btn_addbills_add.setText("اضافة");
        btn_addbills_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addbills_addActionPerformed(evt);
            }
        });
        pan_bill_shop_add.add(btn_addbills_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 100, 60));

        txt_addbills_total_real.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txt_addbills_total_real.setEnabled(false);
        pan_bill_shop_add.add(txt_addbills_total_real, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 20, 87, 20));

        txt_addbills_payed.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txt_addbills_payed.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_addbills_payedFocusLost(evt);
            }
        });
        pan_bill_shop_add.add(txt_addbills_payed, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 50, 87, 20));

        txt_addbills_remain.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txt_addbills_remain.setEnabled(false);
        pan_bill_shop_add.add(txt_addbills_remain, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 80, 87, 20));
        pan_bill_shop_add.add(date_addbills_date, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 50, 190, 30));

        pan_Bill_shop_card.add(pan_bill_shop_add, "card2");

        pan_bill_shop_search.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_bill_shop_search.setOpaque(false);
        pan_bill_shop_search.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_billssearch_search.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_billssearch_search.setText("بحث");
        btn_billssearch_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_billssearch_searchActionPerformed(evt);
            }
        });
        pan_bill_shop_search.add(btn_billssearch_search, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 20, 110, 66));

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("الى:");
        pan_bill_shop_search.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 70, -1, -1));

        jLabel5.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("من:");
        pan_bill_shop_search.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 40, -1, -1));

        check_billssearch1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_billssearch1.setForeground(new java.awt.Color(255, 255, 255));
        check_billssearch1.setText("رقم الفاتورة");
        check_billssearch1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_billssearch1StateChanged(evt);
            }
        });
        pan_bill_shop_search.add(check_billssearch1, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 10, -1, -1));

        check_billssearch2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_billssearch2.setForeground(new java.awt.Color(255, 255, 255));
        check_billssearch2.setText("الاسم");
        check_billssearch2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_billssearch2StateChanged(evt);
            }
        });
        pan_bill_shop_search.add(check_billssearch2, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 10, 80, -1));

        check_billssearch3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_billssearch3.setForeground(new java.awt.Color(255, 255, 255));
        check_billssearch3.setText("رقم الهاتف");
        check_billssearch3.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_billssearch3StateChanged(evt);
            }
        });
        pan_bill_shop_search.add(check_billssearch3, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 10, -1, -1));

        check_billssearch4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_billssearch4.setForeground(new java.awt.Color(255, 255, 255));
        check_billssearch4.setText("التاريخ");
        check_billssearch4.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_billssearch4StateChanged(evt);
            }
        });
        pan_bill_shop_search.add(check_billssearch4, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, 100, -1));

        txt_billssearch1.setEnabled(false);
        pan_bill_shop_search.add(txt_billssearch1, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 60, 131, -1));

        txt_billssearch2.setEnabled(false);
        pan_bill_shop_search.add(txt_billssearch2, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 60, 114, -1));

        txt_billssearch3.setEnabled(false);
        pan_bill_shop_search.add(txt_billssearch3, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 60, 121, -1));

        datepicker_billssearch_from.setEnabled(false);
        pan_bill_shop_search.add(datepicker_billssearch_from, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 40, 180, -1));

        datepicker_billssearch_to.setEnabled(false);
        pan_bill_shop_search.add(datepicker_billssearch_to, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 70, 180, -1));

        pan_Bill_shop_card.add(pan_bill_shop_search, "card3");

        pan_bill_shop.add(pan_Bill_shop_card, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 53, 1072, 120));
        pan_Bill_shop_card.add(pan_bill_shop_add,"bills_add");
        pan_Bill_shop_card.add(pan_bill_shop_search,"bills_search");

        jLabel72.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel72.setForeground(new java.awt.Color(255, 255, 255));
        jLabel72.setText("العملة:");
        pan_bill_shop.add(jLabel72, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 20, -1, -1));

        combo_bills_currency.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "$", "ليرة لبنانية" }));
        combo_bills_currency.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_bills_currencyItemStateChanged(evt);
            }
        });
        pan_bill_shop.add(combo_bills_currency, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 20, 120, -1));

        jLabel113.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_bill_shop.add(jLabel113, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 710));

        CardPanel.add(pan_bill_shop, "card4");

        pan_Bill_trader.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        table_bill_trader.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        table_bill_trader.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        table_bill_trader.getTableHeader().setReorderingAllowed(false);
        table_bill_trader.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_bill_traderMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table_bill_trader);

        pan_Bill_trader.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(22, 189, 1069, 495));

        pan_billt_control.setOpaque(false);

        lbl_billt_add.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_billt_add.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billt_add.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billt_add.setText("إضافة فاتورة");
        lbl_billt_add.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_billt_add.setName("0"); // NOI18N
        lbl_billt_add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billt_addMouseClicked(evt);
            }
        });

        lbl_billt_search.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_billt_search.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billt_search.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billt_search.setText("بحث");
        lbl_billt_search.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_billt_search.setName("1"); // NOI18N
        lbl_billt_search.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billt_searchMouseClicked(evt);
            }
        });

        lbl_billt_edit.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_billt_edit.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billt_edit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billt_edit.setText("تعديل");
        lbl_billt_edit.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_billt_edit.setName("2"); // NOI18N
        lbl_billt_edit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billt_editMouseClicked(evt);
            }
        });

        lbl_billt_delete.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_billt_delete.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billt_delete.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billt_delete.setText("حذف");
        lbl_billt_delete.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_billt_delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billt_deleteMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout pan_billt_controlLayout = new javax.swing.GroupLayout(pan_billt_control);
        pan_billt_control.setLayout(pan_billt_controlLayout);
        pan_billt_controlLayout.setHorizontalGroup(
            pan_billt_controlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_billt_controlLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_billt_delete, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(lbl_billt_edit, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(lbl_billt_search, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(lbl_billt_add, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(9, Short.MAX_VALUE))
        );
        pan_billt_controlLayout.setVerticalGroup(
            pan_billt_controlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_billt_controlLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pan_billt_controlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_billt_delete, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_billt_edit, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_billt_search, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_billt_add, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(7, Short.MAX_VALUE))
        );

        pan_Bill_trader.add(pan_billt_control, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 0, 450, 40));

        pan_Bill_trader_card.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_Bill_trader_card.setOpaque(false);
        pan_Bill_trader_card.setLayout(new java.awt.CardLayout());

        pan_bill_trader_add.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_bill_trader_add.setFocusTraversalPolicyProvider(true);
        pan_bill_trader_add.setOpaque(false);
        pan_bill_trader_add.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("رقم الفاتورة");
        pan_bill_trader_add.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 10, -1, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("التاجر");
        pan_bill_trader_add.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 40, -1, -1));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("التاريخ");
        pan_bill_trader_add.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 10, -1, 30));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("الهاتف");
        pan_bill_trader_add.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 70, -1, -1));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("المجموع");
        pan_bill_trader_add.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 10, -1, -1));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("المدفوع");
        pan_bill_trader_add.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 40, -1, -1));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("تفاصيل:");
        pan_bill_trader_add.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 10, -1, -1));

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(255, 255, 255));
        jLabel29.setText("المتبقي");
        pan_bill_trader_add.add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 60, -1, 30));
        pan_bill_trader_add.add(txt_addbillt_invoicenb, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 10, 90, 20));
        txt_addbillt_invoicenb.putClientProperty("id", "1");
        pan_bill_trader_add.add(txt_addbillt_name, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 40, 110, 20));
        txt_addbillt_name.putClientProperty("id", "2");

        txt_addbillt_phone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        pan_bill_trader_add.add(txt_addbillt_phone, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 70, 110, 20));
        txt_addbillt_phone.putClientProperty("id", "3");

        txt_addbillt_total.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txt_addbillt_total.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_addbillt_totalFocusLost(evt);
            }
        });
        pan_bill_trader_add.add(txt_addbillt_total, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 10, 100, 30));
        txt_addbillt_total.putClientProperty("id", "4");

        txt_addbillt_payed.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txt_addbillt_payed.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_addbillt_payedFocusLost(evt);
            }
        });
        pan_bill_trader_add.add(txt_addbillt_payed, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 40, 99, 30));
        txt_addbillt_payed.putClientProperty("id", "5");

        txt_addbillt_remain.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        txt_addbillt_remain.setEnabled(false);
        pan_bill_trader_add.add(txt_addbillt_remain, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 70, 99, 30));

        area_billt_add.setColumns(20);
        area_billt_add.setRows(5);
        area_billt_add.setText(" ");
        pan_bill_trader_add.add(area_billt_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 10, 270, 110));
        area_billt_add.putClientProperty("id", "6");

        btn_addbillt_add.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btn_addbillt_add.setText("اضافة");
        btn_addbillt_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addbillt_addActionPerformed(evt);
            }
        });
        pan_bill_trader_add.add(btn_addbillt_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 108, 60));
        btn_addbillt_add.putClientProperty("id", "7");
        pan_bill_trader_add.add(date_addbillt_date, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 40, 180, -1));
        date_addbillt_date.putClientProperty("id", "4");

        pan_Bill_trader_card.add(pan_bill_trader_add, "card2");

        pan_bill_trader_search.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_bill_trader_search.setOpaque(false);
        pan_bill_trader_search.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_billtsearch_add.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_billtsearch_add.setText("بحث");
        btn_billtsearch_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_billtsearch_addActionPerformed(evt);
            }
        });
        pan_bill_trader_search.add(btn_billtsearch_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 30, 110, 66));

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("الى:");
        pan_bill_trader_search.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 90, -1, -1));

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("من:");
        pan_bill_trader_search.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 60, -1, -1));

        check_billtsearch1.setBackground(new java.awt.Color(255, 255, 255));
        check_billtsearch1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_billtsearch1.setForeground(new java.awt.Color(255, 255, 255));
        check_billtsearch1.setText("رقم الفاتورة");
        check_billtsearch1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_billtsearch1StateChanged(evt);
            }
        });
        pan_bill_trader_search.add(check_billtsearch1, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 20, -1, -1));

        check_billtsearch2.setBackground(new java.awt.Color(255, 255, 255));
        check_billtsearch2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_billtsearch2.setForeground(new java.awt.Color(255, 255, 255));
        check_billtsearch2.setText("الاسم");
        check_billtsearch2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_billtsearch2StateChanged(evt);
            }
        });
        pan_bill_trader_search.add(check_billtsearch2, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 20, 90, -1));

        check_billtsearch3.setBackground(new java.awt.Color(255, 255, 255));
        check_billtsearch3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_billtsearch3.setForeground(new java.awt.Color(255, 255, 255));
        check_billtsearch3.setText("رقم الهاتف");
        check_billtsearch3.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_billtsearch3StateChanged(evt);
            }
        });
        pan_bill_trader_search.add(check_billtsearch3, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 20, -1, -1));

        check_billtsearch4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_billtsearch4.setForeground(new java.awt.Color(255, 255, 255));
        check_billtsearch4.setText("التاريخ");
        check_billtsearch4.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_billtsearch4StateChanged(evt);
            }
        });
        pan_bill_trader_search.add(check_billtsearch4, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 20, -1, -1));

        txt_billtsearch1.setEnabled(false);
        pan_bill_trader_search.add(txt_billtsearch1, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 70, 131, -1));

        txt_billtsearch2.setEnabled(false);
        pan_bill_trader_search.add(txt_billtsearch2, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 70, 114, -1));

        txt_billtsearch3.setEnabled(false);
        pan_bill_trader_search.add(txt_billtsearch3, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 70, 121, -1));

        datepicker_billtsearch_from.setEnabled(false);
        pan_bill_trader_search.add(datepicker_billtsearch_from, new org.netbeans.lib.awtextra.AbsoluteConstraints(319, 50, 180, -1));

        datepicker_billtsearch_to.setEnabled(false);
        pan_bill_trader_search.add(datepicker_billtsearch_to, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 90, 180, -1));

        pan_Bill_trader_card.add(pan_bill_trader_search, "card3");

        pan_Bill_trader.add(pan_Bill_trader_card, new org.netbeans.lib.awtextra.AbsoluteConstraints(22, 45, 1070, 130));
        pan_Bill_trader_card.add(pan_bill_trader_add,"billt_add");
        pan_Bill_trader_card.add(pan_bill_trader_search,"billt_search");

        jLabel68.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel68.setForeground(new java.awt.Color(255, 255, 255));
        jLabel68.setText("العملة:");
        pan_Bill_trader.add(jLabel68, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 10, -1, -1));

        combo_billt_currency.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "$", "ليرة لبنانية" }));
        combo_billt_currency.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_billt_currencyItemStateChanged(evt);
            }
        });
        pan_Bill_trader.add(combo_billt_currency, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 10, 130, -1));

        jLabel81.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_Bill_trader.add(jLabel81, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 710));

        CardPanel.add(pan_Bill_trader, "card3");

        pan_billt_items.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        table_billt_item.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        table_billt_item.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        table_billt_item.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(table_billt_item);

        pan_billt_items.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 1080, 520));

        pan_billti_control.setOpaque(false);
        pan_billti_control.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_billti_add.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_billti_add.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billti_add.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billti_add.setText("إضافة فاتورة");
        lbl_billti_add.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, null, new java.awt.Color(255, 255, 255), null, null));
        lbl_billti_add.setName("0"); // NOI18N
        lbl_billti_add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billti_addMouseClicked(evt);
            }
        });
        pan_billti_control.add(lbl_billti_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 10, 99, 27));

        lbl_billti_search.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_billti_search.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billti_search.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billti_search.setText("بحث");
        lbl_billti_search.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_billti_search.setName("1"); // NOI18N
        lbl_billti_search.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billti_searchMouseClicked(evt);
            }
        });
        pan_billti_control.add(lbl_billti_search, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 10, 99, 27));

        lbl_billti_edit.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_billti_edit.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billti_edit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billti_edit.setText("تعديل");
        lbl_billti_edit.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_billti_edit.setName("2"); // NOI18N
        lbl_billti_edit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billti_editMouseClicked(evt);
            }
        });
        pan_billti_control.add(lbl_billti_edit, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 10, 99, 27));

        lbl_billti_delete.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_billti_delete.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billti_delete.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billti_delete.setText("حذف");
        lbl_billti_delete.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_billti_delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billti_deleteMouseClicked(evt);
            }
        });
        pan_billti_control.add(lbl_billti_delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 10, 99, 27));

        pan_billt_items.add(pan_billti_control, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 0, 480, 40));

        pan_Billt_item_card.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_Billt_item_card.setOpaque(false);
        pan_Billt_item_card.setLayout(new java.awt.CardLayout());

        pan_billti_add.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_billti_add.setOpaque(false);
        pan_billti_add.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("الكود");
        pan_billti_add.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 20, -1, -1));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("الصنف");
        pan_billti_add.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 50, -1, -1));

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("الاسم");
        pan_billti_add.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 80, -1, -1));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("السعر الكلي");
        pan_billti_add.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 80, -1, -1));

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("النسبة");
        pan_billti_add.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 50, -1, 20));

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("تفاصيل:");
        pan_billti_add.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 20, -1, -1));

        jLabel40.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(255, 255, 255));
        jLabel40.setText("سعر المبيع");
        pan_billti_add.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 80, 70, 20));

        jLabel42.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel42.setForeground(new java.awt.Color(255, 255, 255));
        jLabel42.setText("الوحدة");
        pan_billti_add.add(jLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 20, -1, -1));

        jLabel43.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel43.setForeground(new java.awt.Color(255, 255, 255));
        jLabel43.setText("الكمية");
        pan_billti_add.add(jLabel43, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 50, -1, -1));
        pan_billti_add.add(txt_addbillti_serial, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 20, 150, -1));

        combo_addbillti_type.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        pan_billti_add.add(combo_addbillti_type, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 50, 110, -1));
        AutoCompleteDecorator.decorate(combo_addbillti_type);

        txt_addbillti_type.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_addbillti_typeFocusLost(evt);
            }
        });
        txt_addbillti_type.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_addbillti_typeKeyPressed(evt);
            }
        });
        pan_billti_add.add(txt_addbillti_type, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 50, 99, -1));

        lbl_addbillti_type.setText("اضافة");
        lbl_addbillti_type.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_addbillti_type.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_addbillti_typeMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbl_addbillti_typeMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_addbillti_typeMouseEntered(evt);
            }
        });
        pan_billti_add.add(lbl_addbillti_type, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 50, -1, -1));
        pan_billti_add.add(txt_addbillti_name, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 80, 150, -1));

        txt_addbillti_total.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txt_addbillti_total.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_addbillti_totalFocusLost(evt);
            }
        });
        pan_billti_add.add(txt_addbillti_total, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 80, 110, -1));

        txt_addbillti_discount.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        txt_addbillti_discount.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_addbillti_discountFocusLost(evt);
            }
        });
        pan_billti_add.add(txt_addbillti_discount, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 50, 50, 20));

        txt_addbillti_real_total.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txt_addbillti_real_total.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_addbillti_real_totalFocusLost(evt);
            }
        });
        pan_billti_add.add(txt_addbillti_real_total, new org.netbeans.lib.awtextra.AbsoluteConstraints(429, 80, 90, 20));

        lbl_addbillti_unit.setText("اضافة");
        lbl_addbillti_unit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_addbillti_unit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_addbillti_unitMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbl_addbillti_unitMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_addbillti_unitMouseEntered(evt);
            }
        });
        pan_billti_add.add(lbl_addbillti_unit, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 20, -1, -1));

        combo_addbillti_unit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        pan_billti_add.add(combo_addbillti_unit, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 20, 110, -1));
        AutoCompleteDecorator.decorate(combo_addbillti_unit);

        txt_addbillti_unit.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_addbillti_unitFocusLost(evt);
            }
        });
        txt_addbillti_unit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_addbillti_unitKeyPressed(evt);
            }
        });
        pan_billti_add.add(txt_addbillti_unit, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 20, 106, -1));

        txt_addbillti_qty.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        pan_billti_add.add(txt_addbillti_qty, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 50, 50, -1));

        txt_addbillti_unitprice.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txt_addbillti_unitprice.setEnabled(false);
        pan_billti_add.add(txt_addbillti_unitprice, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 20, 90, -1));
        txt_addbillti_unitprice.setEnabled(false);

        jLabel44.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel44.setForeground(new java.awt.Color(255, 255, 255));
        jLabel44.setText("السعرالفردي");
        pan_billti_add.add(jLabel44, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 20, -1, -1));

        area_addbilli_add.setColumns(20);
        area_addbilli_add.setRows(5);
        pan_billti_add.add(area_addbilli_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, -1, 100));

        btn_addbillti_add.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btn_addbillti_add.setText("اضافة");
        btn_addbillti_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addbillti_addActionPerformed(evt);
            }
        });
        pan_billti_add.add(btn_addbillti_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 90, 60));

        pan_Billt_item_card.add(pan_billti_add, "card2");

        pan_billti_search.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_billti_search.setOpaque(false);
        pan_billti_search.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_billti_search.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_billti_search.setText("بحث");
        btn_billti_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_billti_searchActionPerformed(evt);
            }
        });
        pan_billti_search.add(btn_billti_search, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 26, 120, 70));

        txt_billtisearch1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txt_billtisearch1.setEnabled(false);
        pan_billti_search.add(txt_billtisearch1, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 70, 146, -1));

        txt_billtisearch2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txt_billtisearch2.setEnabled(false);
        pan_billti_search.add(txt_billtisearch2, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 70, 130, -1));

        combo_billtisearch1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        pan_billti_search.add(combo_billtisearch1, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 70, 138, -1));

        check_billtisearch1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_billtisearch1.setForeground(new java.awt.Color(255, 255, 255));
        check_billtisearch1.setText("رقم التسلسلي");
        check_billtisearch1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_billtisearch1StateChanged(evt);
            }
        });
        pan_billti_search.add(check_billtisearch1, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 20, -1, 40));

        check_billtisearch2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_billtisearch2.setForeground(new java.awt.Color(255, 255, 255));
        check_billtisearch2.setText("الاسم");
        check_billtisearch2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_billtisearch2StateChanged(evt);
            }
        });
        pan_billti_search.add(check_billtisearch2, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 20, -1, 40));

        check_billtisearch3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_billtisearch3.setForeground(new java.awt.Color(255, 255, 255));
        check_billtisearch3.setText("الصنف");
        check_billtisearch3.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_billtisearch3StateChanged(evt);
            }
        });
        pan_billti_search.add(check_billtisearch3, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 20, -1, 40));

        pan_Billt_item_card.add(pan_billti_search, "card3");

        pan_billt_items.add(pan_Billt_item_card, new org.netbeans.lib.awtextra.AbsoluteConstraints(21, 50, 1080, 120));
        pan_Billt_item_card.add(pan_billti_add,"billti_add");
        pan_Billt_item_card.add(pan_billti_search,"billti_search");

        jLabel21.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setText("رقم الفاتورة:");
        pan_billt_items.add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 10, -1, -1));

        lbl_billti_invoicenb.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        lbl_billti_invoicenb.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billti_invoicenb.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billti_invoicenb.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_billt_items.add(lbl_billti_invoicenb, new org.netbeans.lib.awtextra.AbsoluteConstraints(432, 10, 110, 30));

        lbl_billti_confirm.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_billti_confirm.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billti_confirm.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billti_confirm.setText("موافق");
        lbl_billti_confirm.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_billti_confirm.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billti_confirmMouseClicked(evt);
            }
        });
        pan_billt_items.add(lbl_billti_confirm, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 99, 27));

        jLabel74.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel74.setForeground(new java.awt.Color(255, 255, 255));
        jLabel74.setText("العملة:");
        pan_billt_items.add(jLabel74, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, -1, -1));

        combo_billti_currency.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "$", "ليرة لبنانية" }));
        combo_billti_currency.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_billti_currencyItemStateChanged(evt);
            }
        });
        pan_billt_items.add(combo_billti_currency, new org.netbeans.lib.awtextra.AbsoluteConstraints(233, 10, 120, -1));

        jLabel88.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_billt_items.add(jLabel88, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 710));

        CardPanel.add(pan_billt_items, "card7");

        pan_bills_items.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel77.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel77.setForeground(new java.awt.Color(255, 255, 255));
        jLabel77.setText("العملة:");
        pan_bills_items.add(jLabel77, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 330, -1, -1));

        combo_billsi_currency.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "$", "ليرة لبنانية" }));
        combo_billsi_currency.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_billsi_currencyItemStateChanged(evt);
            }
        });
        pan_bills_items.add(combo_billsi_currency, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 330, 120, -1));

        pan_billsi_search_add.setOpaque(false);

        jLabel45.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel45.setForeground(new java.awt.Color(255, 255, 255));
        jLabel45.setText("الكود");

        jLabel46.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel46.setForeground(new java.awt.Color(255, 255, 255));
        jLabel46.setText("الاسم");

        jLabel47.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel47.setForeground(new java.awt.Color(255, 255, 255));
        jLabel47.setText("النوع");

        txt_billsi_add_search1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_billsi_add_search1KeyPressed(evt);
            }
        });

        txt_billsi_add_search2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_billsi_add_search2KeyPressed(evt);
            }
        });

        combo_billsi_add_search1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        combo_billsi_add_search1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_billsi_add_search1ItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout pan_billsi_search_addLayout = new javax.swing.GroupLayout(pan_billsi_search_add);
        pan_billsi_search_add.setLayout(pan_billsi_search_addLayout);
        pan_billsi_search_addLayout.setHorizontalGroup(
            pan_billsi_search_addLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_billsi_search_addLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(pan_billsi_search_addLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_billsi_search_addLayout.createSequentialGroup()
                        .addComponent(combo_billsi_add_search1, 0, 105, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel47))
                    .addGroup(pan_billsi_search_addLayout.createSequentialGroup()
                        .addGroup(pan_billsi_search_addLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_billsi_add_search2)
                            .addComponent(txt_billsi_add_search1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pan_billsi_search_addLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel45, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel46, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        pan_billsi_search_addLayout.setVerticalGroup(
            pan_billsi_search_addLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_billsi_search_addLayout.createSequentialGroup()
                .addGroup(pan_billsi_search_addLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel45)
                    .addComponent(txt_billsi_add_search1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pan_billsi_search_addLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel46)
                    .addComponent(txt_billsi_add_search2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pan_billsi_search_addLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel47)
                    .addComponent(combo_billsi_add_search1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        AutoCompleteDecorator.decorate(combo_billsi_add_search1);

        pan_bills_items.add(pan_billsi_search_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 530, 160, 120));

        lbl_billsi_searchitem.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_billsi_searchitem.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billsi_searchitem.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billsi_searchitem.setText("بحث");
        lbl_billsi_searchitem.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_billsi_searchitem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billsi_searchitemMouseClicked(evt);
            }
        });
        pan_bills_items.add(lbl_billsi_searchitem, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 10, 70, 40));

        pan_billsi_search_shop.setOpaque(false);

        jLabel15.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("الكود");

        jLabel20.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("الاسم");

        jLabel41.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel41.setForeground(new java.awt.Color(255, 255, 255));
        jLabel41.setText("النوع");

        txt_billsi_shop_search1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_billsi_shop_search1KeyPressed(evt);
            }
        });

        txt_billsi_shop_search2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_billsi_shop_search2KeyPressed(evt);
            }
        });

        combo_billsi_shop_search1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        combo_billsi_shop_search1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_billsi_shop_search1ItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout pan_billsi_search_shopLayout = new javax.swing.GroupLayout(pan_billsi_search_shop);
        pan_billsi_search_shop.setLayout(pan_billsi_search_shopLayout);
        pan_billsi_search_shopLayout.setHorizontalGroup(
            pan_billsi_search_shopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_billsi_search_shopLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pan_billsi_search_shopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_billsi_search_shopLayout.createSequentialGroup()
                        .addComponent(combo_billsi_shop_search1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel41))
                    .addGroup(pan_billsi_search_shopLayout.createSequentialGroup()
                        .addComponent(txt_billsi_shop_search2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel20))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_billsi_search_shopLayout.createSequentialGroup()
                        .addComponent(txt_billsi_shop_search1, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel15)))
                .addGap(24, 24, 24))
        );
        pan_billsi_search_shopLayout.setVerticalGroup(
            pan_billsi_search_shopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_billsi_search_shopLayout.createSequentialGroup()
                .addGroup(pan_billsi_search_shopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(txt_billsi_shop_search1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pan_billsi_search_shopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel20)
                    .addComponent(txt_billsi_shop_search2, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pan_billsi_search_shopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel41)
                    .addComponent(combo_billsi_shop_search1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        AutoCompleteDecorator.decorate(txt_billsi_shop_search2, listname, true);
        AutoCompleteDecorator.decorate(combo_billsi_shop_search1);

        pan_bills_items.add(pan_billsi_search_shop, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 60, 160, 120));

        lbl_billsi_searchadd.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_billsi_searchadd.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billsi_searchadd.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billsi_searchadd.setText("بحث");
        lbl_billsi_searchadd.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_billsi_searchadd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billsi_searchaddMouseClicked(evt);
            }
        });
        pan_bills_items.add(lbl_billsi_searchadd, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 480, 70, 40));

        lbl_billsi_add.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_billsi_add.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billsi_add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/add-icon-2.png"))); // NOI18N
        lbl_billsi_add.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_billsi_add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billsi_addMouseClicked(evt);
            }
        });
        pan_bills_items.add(lbl_billsi_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 330, 80, 80));

        lbl_billsi_delete.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_billsi_delete.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billsi_delete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/remove-icon-2.png"))); // NOI18N
        lbl_billsi_delete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_billsi_delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_billsi_deleteMouseClicked(evt);
            }
        });
        pan_bills_items.add(lbl_billsi_delete, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 330, 80, 80));

        lbl_billsi_idbills.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        lbl_billsi_idbills.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billsi_idbills.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billsi_idbills.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_bills_items.add(lbl_billsi_idbills, new org.netbeans.lib.awtextra.AbsoluteConstraints(940, 420, 50, 40));

        lbl_billsi_total.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        lbl_billsi_total.setForeground(new java.awt.Color(255, 255, 255));
        lbl_billsi_total.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_billsi_total.setText("0");
        lbl_billsi_total.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_bills_items.add(lbl_billsi_total, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 500, 110, 40));

        jLabel57.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel57.setForeground(new java.awt.Color(255, 255, 255));
        jLabel57.setText("المجموع:");
        pan_bills_items.add(jLabel57, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 470, -1, -1));

        jLabel65.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel65.setForeground(new java.awt.Color(255, 255, 255));
        jLabel65.setText("رقم الفاتورة:");
        pan_bills_items.add(jLabel65, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 430, -1, -1));

        table_bills_item_add.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        table_bills_item_add.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        table_bills_item_add.getTableHeader().setReorderingAllowed(false);
        table_bills_item_add.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                table_bills_item_addFocusGained(evt);
            }
        });
        jScrollPane4.setViewportView(table_bills_item_add);

        pan_bills_items.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 420, 800, 290));

        table_bills_item_shop.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        table_bills_item_shop.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        table_bills_item_shop.getTableHeader().setReorderingAllowed(false);
        jScrollPane7.setViewportView(table_bills_item_shop);
        table_bills_item_shop.setDefaultEditor(Object.class, null);

        pan_bills_items.add(jScrollPane7, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 13, 800, 310));

        btn_billsi_confirm.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btn_billsi_confirm.setForeground(new java.awt.Color(255, 255, 255));
        btn_billsi_confirm.setText("موافق");
        btn_billsi_confirm.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_billsi_confirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_billsi_confirmActionPerformed(evt);
            }
        });
        pan_bills_items.add(btn_billsi_confirm, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 660, 110, 40));

        jLabel87.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_bills_items.add(jLabel87, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 720));

        CardPanel.add(pan_bills_items, "card7");

        pan_expenses.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        combo_expenses_currency.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "$", "ليرة لبنانية" }));
        combo_expenses_currency.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_expenses_currencyItemStateChanged(evt);
            }
        });
        pan_expenses.add(combo_expenses_currency, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 10, 130, -1));

        jLabel114.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel114.setForeground(new java.awt.Color(255, 255, 255));
        jLabel114.setText("العملة:");
        pan_expenses.add(jLabel114, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 10, -1, 30));

        pan_expenses_control.setOpaque(false);

        lbl_expenses_add.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_expenses_add.setForeground(new java.awt.Color(255, 255, 255));
        lbl_expenses_add.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_expenses_add.setText("إضافة فاتورة");
        lbl_expenses_add.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_expenses_add.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_expenses_add.setName("0"); // NOI18N
        lbl_expenses_add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_expenses_addMouseClicked(evt);
            }
        });

        lbl_expenses_search.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_expenses_search.setForeground(new java.awt.Color(255, 255, 255));
        lbl_expenses_search.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_expenses_search.setText("بحث");
        lbl_expenses_search.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_expenses_search.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_expenses_search.setName("1"); // NOI18N
        lbl_expenses_search.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_expenses_searchMouseClicked(evt);
            }
        });

        lbl_expenses_edit.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_expenses_edit.setForeground(new java.awt.Color(255, 255, 255));
        lbl_expenses_edit.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_expenses_edit.setText("تعديل");
        lbl_expenses_edit.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_expenses_edit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_expenses_edit.setName("2"); // NOI18N
        lbl_expenses_edit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_expenses_editMouseClicked(evt);
            }
        });

        lbl_expenses_delete.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_expenses_delete.setForeground(new java.awt.Color(255, 255, 255));
        lbl_expenses_delete.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_expenses_delete.setText("حذف");
        lbl_expenses_delete.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_expenses_delete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_expenses_delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_expenses_deleteMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout pan_expenses_controlLayout = new javax.swing.GroupLayout(pan_expenses_control);
        pan_expenses_control.setLayout(pan_expenses_controlLayout);
        pan_expenses_controlLayout.setHorizontalGroup(
            pan_expenses_controlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 437, Short.MAX_VALUE)
            .addGroup(pan_expenses_controlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pan_expenses_controlLayout.createSequentialGroup()
                    .addGap(11, 11, 11)
                    .addComponent(lbl_expenses_delete, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(lbl_expenses_edit, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(lbl_expenses_search, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(lbl_expenses_add, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(12, Short.MAX_VALUE)))
        );
        pan_expenses_controlLayout.setVerticalGroup(
            pan_expenses_controlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
            .addGroup(pan_expenses_controlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pan_expenses_controlLayout.createSequentialGroup()
                    .addGap(13, 13, 13)
                    .addGroup(pan_expenses_controlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pan_expenses_controlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_expenses_edit, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbl_expenses_delete, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(pan_expenses_controlLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_expenses_add, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbl_expenses_search, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(10, Short.MAX_VALUE)))
        );

        pan_expenses.add(pan_expenses_control, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 0, -1, 50));

        table_expenses.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        table_expenses.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane8.setViewportView(table_expenses);

        pan_expenses.add(jScrollPane8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, 1080, 510));

        pan_control_expenses_card.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pan_control_expenses_card.setOpaque(false);
        pan_control_expenses_card.setLayout(new java.awt.CardLayout());

        pan_control_expenses_add.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_control_expenses_add.setOpaque(false);
        pan_control_expenses_add.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel48.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel48.setForeground(new java.awt.Color(255, 255, 255));
        jLabel48.setText("رقم الفاتورة");
        pan_control_expenses_add.add(jLabel48, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 10, -1, -1));

        jLabel49.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel49.setForeground(new java.awt.Color(255, 255, 255));
        jLabel49.setText("الاسم");
        pan_control_expenses_add.add(jLabel49, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 70, -1, -1));

        jLabel50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel50.setForeground(new java.awt.Color(255, 255, 255));
        jLabel50.setText("التاريخ");
        pan_control_expenses_add.add(jLabel50, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 10, -1, 20));

        jLabel51.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel51.setForeground(new java.awt.Color(255, 255, 255));
        jLabel51.setText("الهاتف");
        pan_control_expenses_add.add(jLabel51, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 100, -1, -1));

        jLabel52.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel52.setForeground(new java.awt.Color(255, 255, 255));
        jLabel52.setText("المجموع");
        pan_control_expenses_add.add(jLabel52, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 10, -1, -1));

        jLabel53.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel53.setForeground(new java.awt.Color(255, 255, 255));
        jLabel53.setText("المدفوع");
        pan_control_expenses_add.add(jLabel53, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 40, -1, -1));

        jLabel54.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel54.setForeground(new java.awt.Color(255, 255, 255));
        jLabel54.setText("تفاصيل:");
        pan_control_expenses_add.add(jLabel54, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 10, 60, 20));

        jLabel55.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel55.setForeground(new java.awt.Color(255, 255, 255));
        jLabel55.setText("المتبقي");
        pan_control_expenses_add.add(jLabel55, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 60, 40, 30));

        jLabel60.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel60.setForeground(new java.awt.Color(255, 255, 255));
        jLabel60.setText("العنوان");
        pan_control_expenses_add.add(jLabel60, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 40, -1, -1));

        txt_addexp_invoicenb.setEnabled(false);
        pan_control_expenses_add.add(txt_addexp_invoicenb, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 10, 50, -1));
        pan_control_expenses_add.add(txt_addexp_name, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 70, 100, -1));

        txt_addexp_phone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        pan_control_expenses_add.add(txt_addexp_phone, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 100, 100, -1));
        pan_control_expenses_add.add(txt_addexp_type, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 40, 100, -1));

        txt_addexp_total.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txt_addexp_total.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_addexp_totalFocusLost(evt);
            }
        });
        pan_control_expenses_add.add(txt_addexp_total, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 10, 87, -1));

        txt_addexp_payed.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txt_addexp_payed.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_addexp_payedFocusLost(evt);
            }
        });
        pan_control_expenses_add.add(txt_addexp_payed, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 40, 87, -1));

        txt_addexp_remain.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txt_addexp_remain.setEnabled(false);
        pan_control_expenses_add.add(txt_addexp_remain, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 70, 87, -1));

        area_addexp_add.setColumns(20);
        area_addexp_add.setRows(5);
        pan_control_expenses_add.add(area_addexp_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 10, 250, 110));

        btn_addexp_add.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btn_addexp_add.setText("اضافة");
        btn_addexp_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addexp_addActionPerformed(evt);
            }
        });
        pan_control_expenses_add.add(btn_addexp_add, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 27, -1, 49));
        pan_control_expenses_add.add(date_addexp_date, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 20, 170, -1));

        pan_control_expenses_card.add(pan_control_expenses_add, "card2");

        pan_control_expenses_search.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_control_expenses_search.setOpaque(false);
        pan_control_expenses_search.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        check_searchexp_search1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_searchexp_search1.setForeground(new java.awt.Color(255, 255, 255));
        check_searchexp_search1.setText("رقم الفاتورة");
        check_searchexp_search1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_searchexp_search1StateChanged(evt);
            }
        });
        pan_control_expenses_search.add(check_searchexp_search1, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 20, -1, -1));

        check_searchexp_search2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_searchexp_search2.setForeground(new java.awt.Color(255, 255, 255));
        check_searchexp_search2.setText("الاسم");
        check_searchexp_search2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_searchexp_search2StateChanged(evt);
            }
        });
        pan_control_expenses_search.add(check_searchexp_search2, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 20, 114, -1));

        check_searchexp_search3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_searchexp_search3.setForeground(new java.awt.Color(255, 255, 255));
        check_searchexp_search3.setText("التوصيف");
        check_searchexp_search3.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_searchexp_search3StateChanged(evt);
            }
        });
        pan_control_expenses_search.add(check_searchexp_search3, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 20, -1, -1));

        check_searchexp_search4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        check_searchexp_search4.setForeground(new java.awt.Color(255, 255, 255));
        check_searchexp_search4.setText("التاريخ");
        check_searchexp_search4.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                check_searchexp_search4StateChanged(evt);
            }
        });
        pan_control_expenses_search.add(check_searchexp_search4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, -1, -1));

        txt_searchexp_search1.setEnabled(false);
        pan_control_expenses_search.add(txt_searchexp_search1, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 70, 131, -1));

        txt_searchexp_search2.setEnabled(false);
        pan_control_expenses_search.add(txt_searchexp_search2, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 70, 114, -1));

        txt_searchexp_search3.setEnabled(false);
        pan_control_expenses_search.add(txt_searchexp_search3, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 70, 105, -1));

        jLabel58.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel58.setForeground(new java.awt.Color(255, 255, 255));
        jLabel58.setText("الى:");
        pan_control_expenses_search.add(jLabel58, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 80, -1, -1));

        jLabel59.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel59.setForeground(new java.awt.Color(255, 255, 255));
        jLabel59.setText("من:");
        pan_control_expenses_search.add(jLabel59, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 50, -1, -1));

        btn_searchexp_search.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_searchexp_search.setText("بحث");
        btn_searchexp_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_searchexp_searchActionPerformed(evt);
            }
        });
        pan_control_expenses_search.add(btn_searchexp_search, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 30, 110, 66));

        datepicker_searchexp_from.setEnabled(false);
        pan_control_expenses_search.add(datepicker_searchexp_from, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 50, 180, -1));

        datepicker_searchexp_to.setEnabled(false);
        pan_control_expenses_search.add(datepicker_searchexp_to, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 80, 180, -1));

        pan_control_expenses_card.add(pan_control_expenses_search, "card3");

        pan_expenses.add(pan_control_expenses_card, new org.netbeans.lib.awtextra.AbsoluteConstraints(14, 50, 1070, 130));
        pan_control_expenses_card.add(pan_control_expenses_add,"expenses_add");
        pan_control_expenses_card.add(pan_control_expenses_search,"expenses_search");

        jLabel115.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_expenses.add(jLabel115, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 710));

        CardPanel.add(pan_expenses, "card5");

        pan_items.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        table_items_show.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane6.setViewportView(table_items_show);

        pan_items.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 1070, 590));

        lbl_item_search.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        lbl_item_search.setForeground(new java.awt.Color(255, 255, 255));
        lbl_item_search.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_item_search.setText("البحث:");
        lbl_item_search.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_item_search.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_item_search.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_item_searchMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbl_item_searchMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbl_item_searchMouseEntered(evt);
            }
        });
        pan_items.add(lbl_item_search, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 30, 90, 60));

        pan_item_control_search.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_item_control_search.setOpaque(false);
        pan_item_control_search.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel82.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel82.setForeground(new java.awt.Color(255, 255, 255));
        jLabel82.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel82.setText("بحث عن:");
        pan_item_control_search.add(jLabel82, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 20, 70, -1));

        jLabel108.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel108.setForeground(new java.awt.Color(255, 255, 255));
        jLabel108.setText("النوع");
        pan_item_control_search.add(jLabel108, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 20, 50, 30));

        combo_item_search.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        combo_item_search.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_item_searchItemStateChanged(evt);
            }
        });
        pan_item_control_search.add(combo_item_search, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 20, 120, -1));
        AutoCompleteDecorator.decorate(combo_item_search);

        jLabel112.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel112.setForeground(new java.awt.Color(255, 255, 255));
        jLabel112.setText("الاسم");
        pan_item_control_search.add(jLabel112, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 20, 50, 30));

        combo_item_search1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        combo_item_search1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_item_search1ItemStateChanged(evt);
            }
        });
        pan_item_control_search.add(combo_item_search1, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 20, 150, -1));
        AutoCompleteDecorator.decorate(combo_item_search1);

        jLabel110.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel110.setForeground(new java.awt.Color(255, 255, 255));
        jLabel110.setText(" الكمية أقل من :");
        pan_item_control_search.add(jLabel110, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, 120, 30));

        txt_item_search.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        txt_item_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_item_searchKeyPressed(evt);
            }
        });
        pan_item_control_search.add(txt_item_search, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 50, 30));

        pan_items.add(pan_item_control_search, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 30, 700, 60));

        jLabel92.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_items.add(jLabel92, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 720));

        CardPanel.add(pan_items, "card9");

        pan_report.setOpaque(false);
        pan_report.setLayout(new java.awt.CardLayout());

        jLabel97.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_report.add(jLabel97, "card6");

        pan_report_payment.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pan_report_payment.setOpaque(false);
        pan_report_payment.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pan_report_payment_details.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pan_report_payment_details.setOpaque(false);
        pan_report_payment_details.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        combo_report_year.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        combo_report_year.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_report_yearItemStateChanged(evt);
            }
        });
        pan_report_payment_details.add(combo_report_year, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 20, 100, 30));
        //AutoCompleteDecorator.decorate(combo_report_year);

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("التاريخ:");
        pan_report_payment_details.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 50, 90, 40));
        pan_report_payment_details.add(datepicker_report_from, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 60, 170, 31));

        jLabel27.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setText("من:");
        pan_report_payment_details.add(jLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 60, -1, -1));

        datepicker_report_to.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                datepicker_report_toActionPerformed(evt);
            }
        });
        pan_report_payment_details.add(datepicker_report_to, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 60, 170, 31));

        jLabel28.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("الاسم:");
        pan_report_payment_details.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 40, 40, 20));

        pan_repot_payment_control.setOpaque(false);
        pan_repot_payment_control.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_report_trader.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_trader.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_trader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_report_trader.setText("مشتريات");
        lbl_report_trader.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_report_trader.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_report_trader.setName("0"); // NOI18N
        lbl_report_trader.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_report_traderMouseClicked(evt);
            }
        });
        pan_repot_payment_control.add(lbl_report_trader, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 111, 60));

        lbl_report_shop.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_shop.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_shop.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_report_shop.setText("مبيعات");
        lbl_report_shop.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_report_shop.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_report_shop.setName("1"); // NOI18N
        lbl_report_shop.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_report_shopMouseClicked(evt);
            }
        });
        pan_repot_payment_control.add(lbl_report_shop, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 111, 60));

        lbl_report_expenses.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_expenses.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_expenses.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_report_expenses.setText("مدفوعات");
        lbl_report_expenses.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_report_expenses.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_report_expenses.setName("2"); // NOI18N
        lbl_report_expenses.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_report_expensesMouseClicked(evt);
            }
        });
        pan_repot_payment_control.add(lbl_report_expenses, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, 111, 60));

        pan_report_payment_details.add(pan_repot_payment_control, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 90, 120, 220));

        table_report_bills.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        table_report_bills.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        table_report_bills.getTableHeader().setReorderingAllowed(false);
        jScrollPane9.setViewportView(table_report_bills);

        pan_report_payment_details.add(jScrollPane9, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 90, 578, 480));

        jLabel71.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel71.setForeground(new java.awt.Color(255, 255, 255));
        jLabel71.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel71.setText("المدفوعات:");
        pan_report_payment_details.add(jLabel71, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 590, 120, -1));

        lbl_report_payed.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_payed.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_payed.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        pan_report_payment_details.add(lbl_report_payed, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 590, 120, 30));

        jLabel75.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel75.setForeground(new java.awt.Color(255, 255, 255));
        jLabel75.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel75.setText("المجموع:");
        pan_report_payment_details.add(jLabel75, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 610, 100, -1));

        lbl_report_total.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_total.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_total.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        pan_report_payment_details.add(lbl_report_total, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 610, 120, 30));

        lbl_report_remaining.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_remaining.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_remaining.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        pan_report_payment_details.add(lbl_report_remaining, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 640, 120, 30));

        jLabel73.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel73.setForeground(new java.awt.Color(255, 255, 255));
        jLabel73.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel73.setText("المتبقي:");
        pan_report_payment_details.add(jLabel73, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 630, 80, 50));

        lbl_report_total_remaining.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_total_remaining.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_total_remaining.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_report_total_remaining.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        pan_report_payment_details.add(lbl_report_total_remaining, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 387, 140, 30));

        jLabel64.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel64.setForeground(new java.awt.Color(255, 255, 255));
        jLabel64.setText("المجموع:");
        pan_report_payment_details.add(jLabel64, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 387, 170, -1));

        jLabel70.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel70.setForeground(new java.awt.Color(255, 255, 255));
        jLabel70.setText("الديون المستحقة:");
        pan_report_payment_details.add(jLabel70, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 337, 170, -1));

        lbl_report_total_remaining_get.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_total_remaining_get.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_total_remaining_get.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_report_total_remaining_get.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        pan_report_payment_details.add(lbl_report_total_remaining_get, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 337, 140, 30));

        lbl_report_total_remaining_pay.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_total_remaining_pay.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_total_remaining_pay.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_report_total_remaining_pay.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        pan_report_payment_details.add(lbl_report_total_remaining_pay, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 287, 140, 30));

        jLabel76.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel76.setForeground(new java.awt.Color(255, 255, 255));
        jLabel76.setText("الديون المتوجبة:");
        pan_report_payment_details.add(jLabel76, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 287, 170, -1));

        jLabel79.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel79.setForeground(new java.awt.Color(255, 255, 255));
        jLabel79.setText("المجموع:");
        pan_report_payment_details.add(jLabel79, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 217, 170, -1));

        lbl_report_total_paysell.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_total_paysell.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_total_paysell.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_report_total_paysell.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        pan_report_payment_details.add(lbl_report_total_paysell, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 217, 140, 30));

        lbl_report_total_sell.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_total_sell.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_total_sell.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_report_total_sell.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        pan_report_payment_details.add(lbl_report_total_sell, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 167, 140, 30));

        jLabel78.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel78.setForeground(new java.awt.Color(255, 255, 255));
        jLabel78.setText("مجموع المشتريات:");
        pan_report_payment_details.add(jLabel78, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 167, 170, -1));

        jLabel63.setFont(new java.awt.Font("Tahoma", 1, 20)); // NOI18N
        jLabel63.setForeground(new java.awt.Color(255, 255, 255));
        jLabel63.setText("مجموع المدفوعات:");
        pan_report_payment_details.add(jLabel63, new org.netbeans.lib.awtextra.AbsoluteConstraints(162, 117, 170, -1));

        jLabel69.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel69.setForeground(new java.awt.Color(255, 255, 255));
        jLabel69.setText("الى:");
        pan_report_payment_details.add(jLabel69, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 60, -1, -1));

        lbl_report_total_payed.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_total_payed.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_total_payed.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_report_total_payed.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        pan_report_payment_details.add(lbl_report_total_payed, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 117, 140, 30));

        btn_report_year.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btn_report_year.setText("كشف سنوي");
        btn_report_year.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_report_yearActionPerformed(evt);
            }
        });
        pan_report_payment_details.add(btn_report_year, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 10, 110, 40));

        btn_report_month.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btn_report_month.setText("كشف شهري");
        btn_report_month.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_report_monthActionPerformed(evt);
            }
        });
        pan_report_payment_details.add(btn_report_month, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 10, 110, 40));

        jLabel26.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_report_payment_details.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 100, 340, 380));

        jLabel66.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_report_payment_details.add(jLabel66, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 576, 580, 110));

        combo_report_payement_name.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        combo_report_payement_name.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_report_payement_nameItemStateChanged(evt);
            }
        });
        pan_report_payment_details.add(combo_report_payement_name, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 60, 120, -1));
        AutoCompleteDecorator.decorate(combo_report_payement_name);

        pan_report_payment.add(pan_report_payment_details, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 720));

        jLabel99.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_report_payment.add(jLabel99, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 720));

        pan_report.add(pan_report_payment, "card2");

        jLabel56.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel56.setForeground(new java.awt.Color(255, 255, 255));
        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel56.setText("كشف مالي");
        jLabel56.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jLabel56.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel56.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel56MouseClicked(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("كشف بضاعة");
        jLabel22.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jLabel22.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel22MouseClicked(evt);
            }
        });

        jLabel98.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N

        javax.swing.GroupLayout pan_report_mainLayout = new javax.swing.GroupLayout(pan_report_main);
        pan_report_main.setLayout(pan_report_mainLayout);
        pan_report_mainLayout.setHorizontalGroup(
            pan_report_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_report_mainLayout.createSequentialGroup()
                .addContainerGap(301, Short.MAX_VALUE)
                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(72, 72, 72)
                .addComponent(jLabel56, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(180, 180, 180))
            .addGroup(pan_report_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pan_report_mainLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel98, javax.swing.GroupLayout.PREFERRED_SIZE, 1108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        pan_report_mainLayout.setVerticalGroup(
            pan_report_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_report_mainLayout.createSequentialGroup()
                .addGap(266, 266, 266)
                .addGroup(pan_report_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel56, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(332, Short.MAX_VALUE))
            .addGroup(pan_report_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pan_report_mainLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel98, javax.swing.GroupLayout.PREFERRED_SIZE, 718, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pan_report.add(pan_report_main, "card2");

        pan_report_item.setOpaque(false);
        pan_report_item.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        combo_reportitems_search.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        combo_reportitems_search.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_reportitems_searchItemStateChanged(evt);
            }
        });
        pan_report_item.add(combo_reportitems_search, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 60, 100, -1));
        AutoCompleteDecorator.decorate(combo_reportitems_search);

        combo_reportitems_search1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));
        combo_reportitems_search1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combo_reportitems_search1ItemStateChanged(evt);
            }
        });
        pan_report_item.add(combo_reportitems_search1, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 60, 130, -1));
        AutoCompleteDecorator.decorate(combo_reportitems_search1);

        jLabel34.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(255, 255, 255));
        jLabel34.setText("من:");
        pan_report_item.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 20, -1, -1));
        pan_report_item.add(datepicker_reportitems_from, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 20, -1, -1));

        jLabel39.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(255, 255, 255));
        jLabel39.setText("الى:");
        pan_report_item.add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 20, -1, -1));

        datepicker_reportitems_to.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                datepicker_reportitems_toActionPerformed(evt);
            }
        });
        pan_report_item.add(datepicker_reportitems_to, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 20, -1, -1));

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setText("الاسم:");
        pan_report_item.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 60, -1, -1));

        jLabel61.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel61.setForeground(new java.awt.Color(255, 255, 255));
        jLabel61.setText("التاريخ");
        pan_report_item.add(jLabel61, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 10, -1, 40));

        pan_report_items_control.setOpaque(false);
        pan_report_items_control.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_report_items_shop.setBackground(new java.awt.Color(255, 255, 255));
        lbl_report_items_shop.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_items_shop.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_items_shop.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_report_items_shop.setText("مبيعات");
        lbl_report_items_shop.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_report_items_shop.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_report_items_shop.setName("0"); // NOI18N
        lbl_report_items_shop.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_report_items_shopMouseClicked(evt);
            }
        });
        pan_report_items_control.add(lbl_report_items_shop, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 110, 70));

        lbl_report_items_trader.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_report_items_trader.setForeground(new java.awt.Color(255, 255, 255));
        lbl_report_items_trader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_report_items_trader.setText("مشتريات");
        lbl_report_items_trader.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_report_items_trader.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_report_items_trader.setName("1"); // NOI18N
        lbl_report_items_trader.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_report_items_traderMouseClicked(evt);
            }
        });
        pan_report_items_control.add(lbl_report_items_trader, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 110, 70));

        pan_report_item.add(pan_report_items_control, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 110, 130, 190));

        btn_reportitems_month.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btn_reportitems_month.setText("كشف شهري");
        btn_reportitems_month.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reportitems_monthActionPerformed(evt);
            }
        });
        pan_report_item.add(btn_reportitems_month, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 20, -1, 40));

        btn_reportitems_year.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btn_reportitems_year.setText("كشف سنوي");
        btn_reportitems_year.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reportitems_yearActionPerformed(evt);
            }
        });
        pan_report_item.add(btn_reportitems_year, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 20, -1, 40));

        jLabel62.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel62.setForeground(new java.awt.Color(255, 255, 255));
        jLabel62.setText("الصنف:");
        pan_report_item.add(jLabel62, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 60, -1, -1));

        jLabel67.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        pan_report_item.add(jLabel67, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 1080, 90));

        table_items.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        table_items.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        table_items.getTableHeader().setReorderingAllowed(false);
        jScrollPane5.setViewportView(table_items);

        pan_report_item.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 940, 590));

        jLabel116.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_report_item.add(jLabel116, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 720));

        pan_report.add(pan_report_item, "card2");

        CardPanel.add(pan_report, "card9");
        pan_report.add(pan_report_item,"report_item");
        pan_report.add(pan_report_main,"report_main");
        pan_report.add(pan_report_payment,"report_payment");

        pan_backup.setOpaque(false);
        pan_backup.setLayout(new java.awt.CardLayout());

        jLabel93.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N

        lbl_backup_back_all.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_backup_back_all.setForeground(new java.awt.Color(255, 255, 255));
        lbl_backup_back_all.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_backup_back_all.setText("نسخ جميع المعلومات");
        lbl_backup_back_all.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_backup_back_all.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_backup_back_all.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_backup_back_allMouseClicked(evt);
            }
        });

        lbl_backup_restore_all.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbl_backup_restore_all.setForeground(new java.awt.Color(255, 255, 255));
        lbl_backup_restore_all.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_backup_restore_all.setText("استرجاع جميع المعلومات");
        lbl_backup_restore_all.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lbl_backup_restore_all.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_backup_restore_all.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_backup_restore_allMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout pan_backup_mainLayout = new javax.swing.GroupLayout(pan_backup_main);
        pan_backup_main.setLayout(pan_backup_mainLayout);
        pan_backup_mainLayout.setHorizontalGroup(
            pan_backup_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1110, Short.MAX_VALUE)
            .addGroup(pan_backup_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pan_backup_mainLayout.createSequentialGroup()
                    .addGap(0, 236, Short.MAX_VALUE)
                    .addComponent(lbl_backup_restore_all, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(74, 74, 74)
                    .addComponent(lbl_backup_back_all, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 236, Short.MAX_VALUE)))
            .addGroup(pan_backup_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pan_backup_mainLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel93, javax.swing.GroupLayout.PREFERRED_SIZE, 1110, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        pan_backup_mainLayout.setVerticalGroup(
            pan_backup_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 720, Short.MAX_VALUE)
            .addGroup(pan_backup_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pan_backup_mainLayout.createSequentialGroup()
                    .addGap(0, 298, Short.MAX_VALUE)
                    .addGroup(pan_backup_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pan_backup_mainLayout.createSequentialGroup()
                            .addGap(4, 4, 4)
                            .addComponent(lbl_backup_restore_all, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(lbl_backup_back_all, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 298, Short.MAX_VALUE)))
            .addGroup(pan_backup_mainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pan_backup_mainLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel93, javax.swing.GroupLayout.PREFERRED_SIZE, 720, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pan_backup.add(pan_backup_main, "card2");

        pan_backup_card.setLayout(new java.awt.CardLayout());

        pan_backup_card_backup_all.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_backup.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btn_backup.setText("بدء عملية النسخ");
        btn_backup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_backupActionPerformed(evt);
            }
        });
        pan_backup_card_backup_all.add(btn_backup, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 200, 210, 60));

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("اختيار مسار حفظ النسخة:");
        jLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_backup_card_backup_all.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 80, -1, -1));

        txt_bckpath.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        txt_bckpath.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txt_bckpath.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txt_bckpath.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_bckpathMouseClicked(evt);
            }
        });
        pan_backup_card_backup_all.add(txt_bckpath, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 120, 720, 50));

        jLabel96.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_backup_card_backup_all.add(jLabel96, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 720));

        pan_backup_card.add(pan_backup_card_backup_all, "card3");

        pan_backup_card_restore_all.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btn_restore.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btn_restore.setText("بدء عملية الاسترجاع");
        btn_restore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_restoreActionPerformed(evt);
            }
        });
        pan_backup_card_restore_all.add(btn_restore, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 200, 210, 60));

        jLabel25.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setText("اختيار مسار  النسخة:");
        jLabel25.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_backup_card_restore_all.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 80, -1, -1));

        txt_restorepath.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        txt_restorepath.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txt_restorepath.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txt_restorepath.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_restorepathMouseClicked(evt);
            }
        });
        pan_backup_card_restore_all.add(txt_restorepath, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 120, 720, 50));

        jLabel117.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_backup_card_restore_all.add(jLabel117, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 720));

        pan_backup_card.add(pan_backup_card_restore_all, "card4");

        jLabel95.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/background.jpg"))); // NOI18N
        pan_backup_card.add(jLabel95, "card2");

        pan_backup.add(pan_backup_card, "card3");
        pan_backup_card.add(pan_backup_card_backup_all,"backup_pan_all");
        pan_backup_card.add(pan_backup_card_restore_all,"restore_pan_all");

        CardPanel.add(pan_backup, "card11");
        pan_backup.add(pan_backup_main,"backup_main");
        pan_backup.add(pan_backup_card,"backup_card");

        getContentPane().add(CardPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 123, 1110, 720));
        CardPanel.add(pan_Bill_trader,"bill_trader");
        CardPanel.add(pan_main,"main");
        CardPanel.add(pan_bill_shop,"bill_shop");
        CardPanel.add(pan_expenses,"expenses");
        CardPanel.add(pan_report,"report");
        CardPanel.add(pan_billt_items,"billt_items");
        CardPanel.add(pan_bills_items,"bills_items");
        CardPanel.add(pan_items,"items");
        CardPanel.add(pan_login,"login");
        CardPanel.add(pan_backup,"backup");

        pan_Background.setBackground(new java.awt.Color(255, 255, 255));

        jLabel100.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/icons-background.png"))); // NOI18N

        javax.swing.GroupLayout pan_BackgroundLayout = new javax.swing.GroupLayout(pan_Background);
        pan_Background.setLayout(pan_BackgroundLayout);
        pan_BackgroundLayout.setHorizontalGroup(
            pan_BackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_BackgroundLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel100, javax.swing.GroupLayout.PREFERRED_SIZE, 1109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(277, Short.MAX_VALUE))
        );
        pan_BackgroundLayout.setVerticalGroup(
            pan_BackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_BackgroundLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jLabel100, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(736, Short.MAX_VALUE))
        );

        getContentPane().add(pan_Background, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1400, 850));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lb_add_bill_shopMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb_add_bill_shopMouseClicked

        lbl_control_shopMouseClicked(evt);
    }//GEN-LAST:event_lb_add_bill_shopMouseClicked

    private void lb_add_bill_traderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb_add_bill_traderMouseClicked

        lbl_control_traderMouseClicked(evt);
    }//GEN-LAST:event_lb_add_bill_traderMouseClicked

    private void lb_add_expensesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb_add_expensesMouseClicked

        lbl_control_expensesMouseClicked(evt);
    }//GEN-LAST:event_lb_add_expensesMouseClicked

    private void lb_reportMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb_reportMouseClicked
        lbl_control_reportsMouseClicked(evt);
    }//GEN-LAST:event_lb_reportMouseClicked

    private void lbl_billt_addMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billt_addMouseClicked
        showHidePanel(pan_Bill_trader_card);

        LabelSelectionByColor(pan_billt_control, lbl_billt_add, lbl_billt_add.getName());

        cl1.show(pan_Bill_trader_card, "billt_add");
        txt_addbillt_invoicenb.requestFocusInWindow();
        cmp.ClearCmp(pan_bill_trader_add);
        area_billt_add.setText("");
        btn_addbillt_add.setText("اضافة");
    }//GEN-LAST:event_lbl_billt_addMouseClicked

    private void lbl_billt_searchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billt_searchMouseClicked
        showHidePanel(pan_Bill_trader_card);

        LabelSelectionByColor(pan_billt_control, lbl_billt_search, lbl_billt_search.getName());

        cmp.ClearCmp(pan_bill_trader_search);
        cl1.show(pan_Bill_trader_card, "billt_search");

    }//GEN-LAST:event_lbl_billt_searchMouseClicked

    private void check_billtsearch1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_billtsearch1StateChanged
        if (check_billtsearch1.isSelected()) {
            txt_billtsearch1.setEnabled(true);
            txt_billtsearch1.requestFocusInWindow();
        } else {
            txt_billtsearch1.setEnabled(false);
        }
    }//GEN-LAST:event_check_billtsearch1StateChanged

    private void check_billtsearch2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_billtsearch2StateChanged
        if (check_billtsearch2.isSelected()) {
            txt_billtsearch2.setEnabled(true);
            txt_billtsearch2.requestFocusInWindow();
        } else {
            txt_billtsearch2.setEnabled(false);
        }
    }//GEN-LAST:event_check_billtsearch2StateChanged

    private void check_billtsearch3StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_billtsearch3StateChanged
        if (check_billtsearch3.isSelected()) {
            txt_billtsearch3.setEnabled(true);
            txt_billtsearch3.requestFocusInWindow();
        } else {
            txt_billtsearch3.setEnabled(false);
        }
    }//GEN-LAST:event_check_billtsearch3StateChanged

    private void check_billtsearch4StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_billtsearch4StateChanged
        if (check_billtsearch4.isSelected()) {
            datepicker_billtsearch_from.setEnabled(true);
            datepicker_billtsearch_to.setEnabled(true);
        } else {
            datepicker_billtsearch_from.setEnabled(false);
            datepicker_billtsearch_to.setEnabled(false);
        }
    }//GEN-LAST:event_check_billtsearch4StateChanged

    private void btn_addbillt_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addbillt_addActionPerformed
        if (txt_addbillt_invoicenb.getText().isEmpty() || txt_addbillt_name.getText().isEmpty()
                || date_addbillt_date.getDate() == null || txt_addbillt_total.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "رقم الفاتورة أو الاسم أو التاريخ أو المجموع فارغ");
        } else {
            combo_billt_currency.setSelectedItem("$");
            CheckTextArea(area_billt_add);

            String[] starray
                    = {
                        txt_addbillt_invoicenb.getText(), txt_addbillt_name.getText(),
                        txt_addbillt_phone.getText(), Datesubstring(date_addbillt_date.getDate().toString()),
                        formater.format(date_addbillt_date.getDate()),
                        txt_addbillt_total.getText(), txt_addbillt_payed.getText(),
                        txt_addbillt_remain.getText(),
                        area_billt_add.getText()
                    };
            String[] fields
                    = {
                        "`رقم الفاتورة`", "`الاسم`", "`الهاتف`",
                        "`التاريخ`", "`تاريخ`", "`المجموع`", "`المدفوع`",
                        "`المتبقي`", "`تفاصيل`"
                    };
            if (btn_addbillt_add.getText().equals("اضافة")) {
                boolean insert = dbfunc.insertData("bill_trader", starray, "");
                if (insert) {
                    JOptionPane.showMessageDialog(null, "تمت عملية الاضافة");
                    cl.show(CardPanel, "billt_items");
                    lbl_billti_invoicenb.setText(txt_addbillt_invoicenb.getText());
                    ShowBilltitems();
                } else {
                    JOptionPane.showMessageDialog(null, "خطأ في عملية الاضافة");
                }
            } else if (btn_addbillt_add.getText().equals("تعديل")) {

                String where = " where `رقم الفاتورة` like '" + txt_addbillt_invoicenb.getText() + "'";
                boolean update = dbfunc.updateData("bill_trader", fields, starray, where);
                if (update) {
                    JOptionPane.showMessageDialog(null, "تمت عملية التعديل");
                    cmp.ClearCmp(pan_bill_trader_add);
                    area_billt_add.setText("");

                    showBillt();

                } else {
                    JOptionPane.showMessageDialog(null, "خطأ في عملية التعديل");
                }
            }

        }

    }//GEN-LAST:event_btn_addbillt_addActionPerformed

    private void btn_billtsearch_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_billtsearch_addActionPerformed
        String column = " `رقم الفاتورة`,`الاسم`,`الهاتف`,`التاريخ`,`المجموع`,`المدفوع`,`المتبقي`,`تفاصيل` ";
        String table = "bill_trader";
        String where = "";
        String tempwhere = "";
        // check the selected checkbox to make the query
        if (check_billtsearch1.isSelected()) {
            where = " `رقم الفاتورة` like '%" + txt_billtsearch1.getText() + "%' ";
        }
        if (check_billtsearch2.isSelected()) {
            tempwhere = " `الاسم` like '%" + txt_billtsearch2.getText() + "%'";
            where = where.isEmpty() ? tempwhere : where + " And " + tempwhere;

        }
        if (check_billtsearch3.isSelected()) {
            tempwhere = " `الهاتف` = " + txt_billtsearch3.getText() + " ";
            where = where.isEmpty() ? tempwhere : where + " And " + tempwhere;
        }
        if (check_billtsearch4.isSelected()) {
            String[] dateCondition = allpurpose.CreateDateCondition(datepicker_billtsearch_from, datepicker_billtsearch_to);
            if (dateCondition.length != 0) {
                tempwhere = " `تاريخ` >= " + dateCondition[0] + " and `تاريخ` <= " + dateCondition[1] + " ";
                where = where.isEmpty() ? tempwhere : where + " And " + tempwhere;
            }

        }
        utility.resetTable(dtm);
        combo_billt_currency.setSelectedItem("$");
        dbfunc.selectData(dtm, table_bill_trader, column, table, where, "", "", "", "", "");
        utility.setupTable(dtm, table_bill_trader);
        table_bill_trader.setDefaultEditor(Object.class, null);
    }//GEN-LAST:event_btn_billtsearch_addActionPerformed

    private void btn_addbills_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addbills_addActionPerformed
        boolean success = false;
        Vector v = new Vector();
        String where = " where `رقم الفاتورة` = " + txt_addbills_invoicenb.getText();

        if (txt_addbills_name.getText().isEmpty() || date_addbills_date.getDate() == null) {
            JOptionPane.showMessageDialog(null, " الاسم أو التاريخ  فارغ");
        } else {
            combo_bills_currency.setSelectedItem("$");
            try {
                v = dbfunc.searchBycolumn("bill_local", "`رقم الفاتورة`", where);
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            CheckTextArea(area_addbills_add);

            String[] starray1 = {
                txt_addbills_invoicenb.getText(), txt_addbills_name.getText(),
                txt_addbills_phone.getText(), Datesubstring(date_addbills_date.getDate().toString()),
                formater.format(date_addbills_date.getDate()),
                txt_addbills_total_real.getText(), txt_addbills_payed.getText(),
                txt_addbills_remain.getText(),
                area_addbills_add.getText()
            };
            String[] fields = {
                "`رقم الفاتورة`", "`الاسم`", "`الهاتف`",
                "`التاريخ`", "`تاريخ`", "`المجموع`", "`المدفوع`",
                "`المتبقي`", "`تفاصيل`"
            };

            if (btn_addbills_add.getText().equals("اضافة")) {
                lbl_billsi_idbills.setText(txt_addbills_invoicenb.getText());
                showPanBillsi();
                cl.show(CardPanel, "bills_items");

            } else if (btn_addbills_add.getText().equals("تعديل")) {
                boolean update = dbfunc.updateData("bill_local", fields, starray1, where);
                if (update) {
                    JOptionPane.showMessageDialog(null, "تمت عملية التعديل");
                } else {
                    JOptionPane.showMessageDialog(null, "خطأ في عملية التعديل");
                }
            } else if (btn_addbills_add.getText().equals("موافق")) {
                if (v.isEmpty()) {
                    success = dbfunc.insertData("bill_local", starray1, "");
                    if (success) {
                        JOptionPane.showMessageDialog(null, "تمت عملية الاضافة");
                    } else {
                        JOptionPane.showMessageDialog(null, "خطأ في عملية الاضافة");
                    }
                } else {
                    boolean update = dbfunc.updateData("bill_local", fields, starray1, where);
                    if (update) {
                        JOptionPane.showMessageDialog(null, "تمت عملية التعديل");
                    } else {
                        JOptionPane.showMessageDialog(null, "خطأ في عملية التعديل");
                    }
                }

            }
            cmp.ClearCmp(pan_bill_shop_add);
            showBills();
            newBillS();

        }
    }//GEN-LAST:event_btn_addbills_addActionPerformed

    private void btn_billssearch_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_billssearch_searchActionPerformed
        String column = "`رقم الفاتورة`,`الاسم`,`الهاتف`,`التاريخ`,`المجموع`,`المدفوع`,"
                + "`المتبقي`,`تفاصيل` ";

        String table = "bill_local";
        String where = "";
        String tempwhere = "";
        // check the selected checkbox to make the query
        if (check_billssearch1.isSelected()) {
            where = " `رقم الفاتورة` like '%" + txt_billssearch1.getText() + "%' ";

        }
        if (check_billssearch2.isSelected()) {
            tempwhere = " `الاسم` like '%" + txt_billssearch2.getText() + "%'";
            where = where.isEmpty() ? tempwhere : where + " And " + tempwhere;

        }
        if (check_billssearch3.isSelected()) {
            tempwhere = " `الهاتف` like '%" + txt_billssearch3.getText() + "%'";
            where = where.isEmpty() ? tempwhere : where + " And " + tempwhere;

        }
        if (check_billssearch4.isSelected()) {
            String[] dateCondition = allpurpose.CreateDateCondition(datepicker_billssearch_from, datepicker_billssearch_to);
            if (dateCondition.length != 0) {
                tempwhere = " `تاريخ` >= " + dateCondition[0] + " and `تاريخ` <= " + dateCondition[1] + " ";
            }

            where = where.isEmpty() ? tempwhere : where + " And " + tempwhere;
        }
        utility.resetTable(dtm1);
        combo_bills_currency.setSelectedItem("$");
        dbfunc.selectData(dtm1, table_bill_shop, column, table, where, "", "", "", "", "");
        utility.setupTable(dtm1, table_bill_shop);
        table_bill_shop.setDefaultEditor(Object.class, null);
    }//GEN-LAST:event_btn_billssearch_searchActionPerformed

    private void check_billssearch1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_billssearch1StateChanged
        if (check_billssearch1.isSelected()) {
            txt_billssearch1.setEnabled(true);
            txt_billssearch1.requestFocusInWindow();
        } else {
            txt_billssearch1.setEnabled(false);
        }
    }//GEN-LAST:event_check_billssearch1StateChanged

    private void check_billssearch2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_billssearch2StateChanged
        if (check_billssearch2.isSelected()) {
            txt_billssearch2.setEnabled(true);
            txt_billssearch2.requestFocusInWindow();
        } else {
            txt_billssearch2.setEnabled(false);
        }
    }//GEN-LAST:event_check_billssearch2StateChanged

    private void check_billssearch3StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_billssearch3StateChanged
        if (check_billssearch3.isSelected()) {
            txt_billssearch3.setEnabled(true);
            txt_billssearch3.requestFocusInWindow();
        } else {
            txt_billssearch3.setEnabled(false);
        }
    }//GEN-LAST:event_check_billssearch3StateChanged

    private void check_billssearch4StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_billssearch4StateChanged
        if (check_billssearch4.isSelected()) {
            datepicker_billssearch_from.setEnabled(true);
            datepicker_billssearch_to.setEnabled(true);
        } else {
            datepicker_billssearch_from.setEnabled(false);
            datepicker_billssearch_to.setEnabled(false);
        }
    }//GEN-LAST:event_check_billssearch4StateChanged

    private void lbl_bills_addMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_bills_addMouseClicked
        newBillS();

    }//GEN-LAST:event_lbl_bills_addMouseClicked

    private void lbl_bills_searchsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_bills_searchsMouseClicked
        showHidePanel(pan_Bill_shop_card);

        LabelSelectionByColor(pan_bills_control, lbl_bills_searchs, lbl_bills_searchs.getName());

        cmp.ClearCmp(pan_bill_shop_search);
        cl2.show(pan_Bill_shop_card, "bills_search");

    }//GEN-LAST:event_lbl_bills_searchsMouseClicked

    private void lbl_billt_editMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billt_editMouseClicked
        int row = table_bill_trader.getSelectedRow();

        if (row == -1) {
            JOptionPane.showMessageDialog(null, "يجب تحديد سطر من الجدول");
        } else {

            LabelSelectionByColor(pan_billt_control, lbl_billt_edit, lbl_billt_edit.getName());

            String Table_click = (table_bill_trader.getModel().getValueAt(row, 0).toString());
            String table = "bill_trader";
            String where = "  `رقم الفاتورة` like '" + Table_click + "' ";
            String columns = " `رقم الفاتورة`,`الاسم`,`الهاتف`,`تاريخ`,"
                    + "`المجموع`,`المدفوع`,"
                    + "`المتبقي`,`تفاصيل` ";
            try {
                Component[] comp
                        = {
                            txt_addbillt_invoicenb, txt_addbillt_name, txt_addbillt_phone,
                            date_addbillt_date, txt_addbillt_total, txt_addbillt_payed, txt_addbillt_remain,
                            area_billt_add
                        };
                combo_billt_currency.setSelectedItem("$");
                Vector vec = dbfunc.searchBycolumn(table, columns, where);
                utility.ValueToField(comp, vec);
                btn_addbillt_add.setText("تعديل");
                cl1.show(pan_Bill_trader_card, "billt_add");
                pan_Bill_trader_card.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_lbl_billt_editMouseClicked

    private void lbl_billt_deleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billt_deleteMouseClicked
        int row = table_bill_trader.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(null, "يجب تحديد سطر من الجدول");
        } else {
            String Table_click = (table_bill_trader.getModel().getValueAt(row, 0).toString());
            String table = "bill_trader";
            String[] fields
                    = {
                        "`رقم الفاتورة`"
                    };
            String[] vals
                    = {
                        Table_click
                    };
            boolean deleted = dbfunc.deleteData(table, fields, vals);
            if (deleted) {
                JOptionPane.showMessageDialog(null, "تمت عملية الحذف");
                cmp.ClearCmp(pan_Bill_trader_card);
                utility.resetTable(dtm);

                showBillt();

            } else {
                JOptionPane.showMessageDialog(null, "حصل خطأ أثناء عملية الحذف الرجاء اعادة المحاولة");
            }
        }
    }//GEN-LAST:event_lbl_billt_deleteMouseClicked

    private void lbl_bills_editMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_bills_editMouseClicked
        int row = table_bill_shop.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(null, "يجب تحديد سطر من الجدول");
        } else {
            LabelSelectionByColor(pan_bills_control, lbl_bills_edit, lbl_bills_edit.getName());

            String Table_click = (table_bill_shop.getModel().getValueAt(row, 0).toString());
            String table = "bill_local";
            String where = "  `رقم الفاتورة` = " + Table_click + " ";
            String columns = " `رقم الفاتورة`,`الاسم`,`الهاتف`,`تاريخ`,`المجموع`,`المدفوع`,`المتبقي`,`تفاصيل` ";

            try {
                Component[] comp
                        = {
                            txt_addbills_invoicenb, txt_addbills_name, txt_addbills_phone,
                            date_addbills_date,
                            txt_addbills_total_real, txt_addbills_payed, txt_addbills_remain,
                            area_addbills_add
                        };
                Vector vec = dbfunc.searchBycolumn(table, columns, where);
                combo_bills_currency.setSelectedItem("$");
                utility.ValueToField(comp, vec);
                btn_addbills_add.setText("تعديل");
                cl2.show(pan_Bill_shop_card, "bills_add");
                pan_Bill_shop_card.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }//GEN-LAST:event_lbl_bills_editMouseClicked

    private void lbl_bills_deleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_bills_deleteMouseClicked
        int row = table_bill_shop.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(null, "يجب تحديد سطر من الجدول");
        } else {
            String Table_click = (table_bill_shop.getModel().getValueAt(row, 0).toString());
            String table = "bill_local";
            String[] fields
                    = {
                        "`رقم الفاتورة`"
                    };
            String[] vals
                    = {
                        Table_click
                    };
            boolean deleted = dbfunc.deleteData(table, fields, vals);
            if (deleted) {
                JOptionPane.showMessageDialog(null, "تمت عملية الحذف");
                cmp.ClearCmp(pan_Bill_shop_card);
                combo_bills_currency.setSelectedItem("$");
                showBills();
                newBillS();
            } else {
                JOptionPane.showMessageDialog(null, "حصل خطأ أثناء عملية الحذف الرجاء اعادة المحاولة");
            }
        }
    }//GEN-LAST:event_lbl_bills_deleteMouseClicked

    private void btn_addbillti_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addbillti_addActionPerformed
        if (txt_addbillti_serial.getText().isEmpty()
                || combo_addbillti_type.getSelectedIndex() == 0
                || txt_addbillti_name.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, " الاسم أو الصنف أو الرقم التسلسلي فارغ");
        } else {
            combo_billti_currency.setSelectedItem("$");
            CheckTextArea(area_addbilli_add);
            String[] starray = {
                txt_addbillti_serial.getText(),
                combo_addbillti_type.getSelectedItem().toString(),
                txt_addbillti_name.getText(),
                combo_addbillti_unit.getSelectedItem().toString(), txt_addbillti_qty.getText(),
                txt_addbillti_unitprice.getText(), txt_addbillti_total.getText(),
                txt_addbillti_discount.getText(), txt_addbillti_real_total.getText(),
                area_addbilli_add.getText(), lbl_billti_invoicenb.getText()
            };
            String[] fields = {
                "الكود", "الصنف", "الاسم",
                "الوحدة", "الكمية", "`السعر الفردي`", "المجموع", "النسبة",
                "`سعر المبيع`", "تفاصيل", "`رقم الفاتورة`"
            };
            String where = " where `الكود` like '" + txt_addbillti_serial.getText() + "' and "
                    + "`الصنف` like '" + combo_addbillti_type.getSelectedItem().toString() + "' "
                    + "and `رقم الفاتورة` like '" + lbl_billti_invoicenb.getText() + "'";

            if (btn_addbillti_add.getText().equals("اضافة")) {
                String[] values = {
                    txt_addbillti_serial.getText(),
                    combo_addbillti_type.getSelectedItem().toString()};
                int row = Compare2Tables(dtm2, values);

                if (row == -1) {
                    boolean insert = dbfunc.insertData("bill_trader_items", starray, "");

                    if (insert) {
                        JOptionPane.showMessageDialog(null, "تمت عملية الاضافة");
                        dtm2.addRow(starray);

                    } else {
                        JOptionPane.showMessageDialog(null, "خطأ في عملية الاضافة");
                    }
                } else {
                    Object[] newqty = {
                        Float.parseFloat(String.valueOf(dtm2.getValueAt(row, 4)))
                        + Float.parseFloat(txt_addbillti_qty.getText())
                    };
                    dtm2.setValueAt(newqty[0], row, 4);
                    String[] fields1 = {"`الكمية`"
                    };
                    dbfunc.updateData("bill_trader_items", fields1, newqty, where);
                }

            } else if (btn_addbillti_add.getText().equals("تعديل")) {

                boolean update = dbfunc.updateData("bill_trader_items", fields, starray, where);
                if (update) {
                    JOptionPane.showMessageDialog(null, "تمت عملية التعديل");
                    ShowBilltitems();
                } else {
                    JOptionPane.showMessageDialog(null, "خطأ في عملية التعديل");
                }
            }
            cmp.ClearCmp(pan_billti_add);
            dbfunc.refreshCB(combo_addbillti_type, "type", "type");
            dbfunc.refreshCB(combo_addbillti_unit, "unit", "unit");
            txt_addbillti_serial.setEnabled(true);
            combo_addbillti_type.setEnabled(true);
            area_addbilli_add.setText("");
            utility.setupTable(dtm2, table_billt_item);
        }

    }//GEN-LAST:event_btn_addbillti_addActionPerformed

    private void btn_billti_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_billti_searchActionPerformed
        String column = " `الكود`,`الصنف`,`الاسم`,`الوحدة`,`الكمية`,`السعر الفردي`,`المجموع`,`النسبة`,`سعر المبيع`,`تفاصيل` ";
        String table = "bill_trader_items";
        String where = " `رقم الفاتورة` like '" + lbl_billti_invoicenb.getText() + "'";
        String tempwhere;
        // check the selected checkbox to make the query
        if (check_billtisearch1.isSelected()) {
            tempwhere = " and `الكود` like '%" + txt_billtisearch1.getText() + "%' ";
            where = where + tempwhere;
        }
        if (check_billtisearch2.isSelected()) {
            tempwhere = " and `الاسم` like '%" + txt_billtisearch2.getText() + "%'";
            where = where + tempwhere;

        }
        if (check_billtisearch3.isSelected()) {
            tempwhere = " and `الصنف` like '%" + combo_billtisearch1.getSelectedItem().toString() + "%' ";
            where = where + tempwhere;

        }
        utility.resetTable(dtm2);
        dbfunc.selectData(dtm2, table_billt_item, column, table, where, "", "", "", "", "");
        utility.setupTable(dtm2, table_billt_item);
        table_billt_item.setDefaultEditor(Object.class, null);
    }//GEN-LAST:event_btn_billti_searchActionPerformed

    private void check_billtisearch1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_billtisearch1StateChanged
        if (check_billtisearch1.isSelected()) {
            txt_billtisearch1.setEnabled(true);
        } else {
            txt_billtisearch1.setEnabled(false);
        }
    }//GEN-LAST:event_check_billtisearch1StateChanged

    private void check_billtisearch2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_billtisearch2StateChanged
        if (check_billtisearch2.isSelected()) {
            txt_billtisearch2.setEnabled(true);
        } else {
            txt_billtisearch2.setEnabled(false);
        }
    }//GEN-LAST:event_check_billtisearch2StateChanged

    private void check_billtisearch3StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_billtisearch3StateChanged
        if (check_billtisearch3.isSelected()) {
            combo_billtisearch1.setEnabled(true);
            dbfunc.refreshCB(combo_billtisearch1, "type", "type");
        } else {
            combo_billtisearch1.setEnabled(false);
        }
    }//GEN-LAST:event_check_billtisearch3StateChanged

    private void lbl_billti_addMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billti_addMouseClicked
        showHidePanel(pan_Billt_item_card);

        LabelSelectionByColor(pan_billti_control, lbl_billti_add, lbl_billti_add.getName());

        cl3.show(pan_Billt_item_card, "billti_add");
        dbfunc.refreshCB(combo_addbillti_type, "type", "type");
        dbfunc.refreshCB(combo_addbillti_unit, "unit", "unit");
        txt_addbillti_type.setVisible(false);
        txt_addbillti_unit.setVisible(false);
        txt_addbillti_serial.setEnabled(true);
        combo_addbillti_type.setEnabled(true);
        txt_addbillti_serial.requestFocusInWindow();
        cmp.ClearCmp(pan_billti_add);
        btn_addbillti_add.setText("اضافة");

    }//GEN-LAST:event_lbl_billti_addMouseClicked

    private void lbl_billti_searchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billti_searchMouseClicked

        showHidePanel(pan_Billt_item_card);

        LabelSelectionByColor(pan_billti_control, lbl_billti_search, lbl_billti_search.getName());

        cl3.show(pan_Billt_item_card, "billti_search");
        cmp.ClearCmp(pan_billti_search);

    }//GEN-LAST:event_lbl_billti_searchMouseClicked

    private void lbl_billti_editMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billti_editMouseClicked
        int row = table_billt_item.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(null, "يجب تحديد سطر من الجدول");
        } else {
            LabelSelectionByColor(pan_billti_control, lbl_billti_edit, lbl_billti_edit.getName());

            String Table_click = (table_billt_item.getModel().getValueAt(row, 0).toString());
            String Table_click1 = (table_billt_item.getModel().getValueAt(row, 1).toString());
            String table = "bill_trader_items";
            String where = "  `الكود` like '" + Table_click + "'"
                    + " and `الصنف` like '" + Table_click1 + "' and `رقم الفاتورة` like '" + lbl_billti_invoicenb.getText() + "'";
            String columns = " * ";

            try {
                Component[] comp
                        = {
                            txt_addbillti_serial, combo_addbillti_type, txt_addbillti_name,
                            combo_addbillti_unit, txt_addbillti_qty, txt_addbillti_unitprice,
                            txt_addbillti_total, txt_addbillti_discount, txt_addbillti_real_total,
                            area_addbilli_add, lbl_billti_invoicenb
                        };
                combo_billti_currency.setSelectedItem("$");
                Vector vec = dbfunc.searchBycolumn(table, columns, where);
                utility.ValueToField(comp, vec);
                btn_addbillti_add.setText("تعديل");
                txt_addbillti_serial.setEnabled(false);
                combo_addbillti_type.setEnabled(false);
                cl3.show(pan_Billt_item_card, "billti_add");
                pan_Billt_item_card.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_lbl_billti_editMouseClicked

    private void lbl_billti_deleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billti_deleteMouseClicked
        int row = table_billt_item.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(null, "يجب تحديد سطر من الجدول");
        } else {
            String Table_click = (table_billt_item.getModel().getValueAt(row, 0).toString());
            String Table_click1 = (table_billt_item.getModel().getValueAt(row, 1).toString());
            String table = "bill_trader_items";
            String[] fields
                    = {
                        "`الكود`", "`الصنف`"
                    };
            String[] values
                    = {
                        Table_click, Table_click1
                    };

            boolean deleted = dbfunc.deleteData(table, fields, values);
            if (deleted) {
                JOptionPane.showMessageDialog(null, "تمت عملية الحذف");
                dtm2.removeRow(row);
                cmp.ClearCmp(pan_Billt_item_card);
            } else {
                JOptionPane.showMessageDialog(null, "حصل خطأ أثناء عملية الحذف الرجاء اعادة المحاولة");
            }
        }
    }//GEN-LAST:event_lbl_billti_deleteMouseClicked

    private void lbl_addbillti_typeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_addbillti_typeMouseClicked

        combo_addbillti_type.setVisible(false);
        txt_addbillti_type.setVisible(true);
        btn_addbillti_add.setEnabled(false);
        txt_addbillti_type.requestFocusInWindow();
    }//GEN-LAST:event_lbl_addbillti_typeMouseClicked

    private void txt_addbillti_typeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_addbillti_typeKeyPressed
        // method handling the is keypressed actoin on the text field after adding a type
        if (txt_addbillti_type.isFocusOwner()) {
            if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
                txt_addbillti_type.setText("");
                combo_addbillti_type.setVisible(true);
                txt_addbillti_type.setVisible(false);
                btn_addbillti_add.setEnabled(true);

            } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

                if (!txt_addbillti_type.getText().isEmpty()) {
                    String[] starray = {
                        null, txt_addbillti_type.getText()
                    };
                    dbfunc.insertData("type", starray, "");
                    JOptionPane.showMessageDialog(null, "تمت عملية الاضافة");
                    dbfunc.refreshCB(combo_addbillti_type, "type", "type");
                }
                txt_addbillti_type.setText("");
                combo_addbillti_type.setVisible(true);
                txt_addbillti_type.setVisible(false);
                btn_addbillti_add.setEnabled(true);
            }

        }
    }//GEN-LAST:event_txt_addbillti_typeKeyPressed

    private void txt_addbillti_typeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_addbillti_typeFocusLost
        combo_addbillti_type.setVisible(true);
        txt_addbillti_type.setVisible(false);
        txt_addbillti_type.setText("");
        btn_addbillti_add.setEnabled(true);
    }//GEN-LAST:event_txt_addbillti_typeFocusLost

    private void lbl_addbillti_unitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_addbillti_unitMouseClicked
        combo_addbillti_unit.setVisible(false);
        txt_addbillti_unit.setVisible(true);
        btn_addbillti_add.setEnabled(false);
        txt_addbillti_unit.requestFocusInWindow();
    }//GEN-LAST:event_lbl_addbillti_unitMouseClicked

    private void txt_addbillti_unitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_addbillti_unitKeyPressed
        // method handling the is keypressed actoin on the text field after adding a unit
        if (txt_addbillti_unit.isFocusOwner()) {
            if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
                txt_addbillti_unit.setText("");
                combo_addbillti_unit.setVisible(true);
                txt_addbillti_unit.setVisible(false);
                btn_addbillti_add.setEnabled(true);

            } else if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

                if (!txt_addbillti_unit.getText().isEmpty()) {
                    String[] starray = {
                        null, txt_addbillti_unit.getText()
                    };
                    dbfunc.insertData("unit", starray, "");
                    JOptionPane.showMessageDialog(null, "تمت عملية الاضافة");
                    dbfunc.refreshCB(combo_addbillti_unit, "unit", "unit");
                }
                txt_addbillti_unit.setText("");
                combo_addbillti_unit.setVisible(true);
                txt_addbillti_unit.setVisible(false);
                btn_addbillti_add.setEnabled(true);

            }

        }
    }//GEN-LAST:event_txt_addbillti_unitKeyPressed

    private void txt_addbillti_unitFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_addbillti_unitFocusLost
        combo_addbillti_unit.setVisible(true);
        txt_addbillti_unit.setVisible(false);
        txt_addbillti_unit.setText("");
        btn_addbillti_add.setEnabled(true);
    }//GEN-LAST:event_txt_addbillti_unitFocusLost

    private void lbl_billsi_searchitemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billsi_searchitemMouseClicked
        showHidePanel(pan_billsi_search_shop);
        dbfunc.refreshCB(combo_billsi_shop_search1, "type", "type");
    }//GEN-LAST:event_lbl_billsi_searchitemMouseClicked

    private void lbl_billsi_searchaddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billsi_searchaddMouseClicked
        showHidePanel(pan_billsi_search_add);
        dbfunc.refreshCB(combo_billsi_add_search1, "type", "type");
    }//GEN-LAST:event_lbl_billsi_searchaddMouseClicked

    private void lbl_billsi_addMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billsi_addMouseClicked
        Vector v = new Vector();
        Vector v1 = new Vector();
        int row1 = 0;
        int row = table_bills_item_shop.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(null, "يجب تحديد سطر من الجدول");
        } else {
            Object[] cell = {dtm3.getValueAt(row, 0), dtm3.getValueAt(row, 1)};
            row1 = Compare2Tables(dtm4, cell);

            try {
                v1 = dbfunc.searchBycolumn("bill_local", "`رقم الفاتورة`", "where `رقم الفاتورة`= " + lbl_billsi_idbills.getText());
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

            int[] cellqty = {row, 3};
            int[] cellqty1 = {row1, 3};
            int[][] cells = {cellqty, cellqty1};

            if (row1 == -1) {
                CalculateQty(dtm3, null, cells);
                for (int i = 0; i < dtm3.getColumnCount(); i++) {
                    if (i == 3) {
                        v.add(1);
                        continue;
                    }
                    v.add(dtm3.getValueAt(row, i));
                }
                v.add(0);
                v.add(dtm3.getValueAt(row, 4));
                v.add(dtm3.getValueAt(row, 4));

                dtm4.addRow(v.toArray());
                v.add(lbl_billsi_idbills.getText());
                if (!v1.isEmpty()) {
                    dbfunc.insertData("bill_local_items", v.toArray(), "");
                }

            } else {
                CalculateQty(dtm3, dtm4, cells);
                CalculateTotalSIRow(dtm4, row1);
                if (!v1.isEmpty()) {
                    Object[] fields = {"`الكمية`", "`المجموع`"};
                    Object[] vals = {dtm4.getValueAt(row1, 3), dtm4.getValueAt(row1, 7)};
                    String where = "where `الكود` like '" + dtm4.getValueAt(row1, 0) + "' and"
                            + " `الصنف` like '" + dtm4.getValueAt(row1, 1) + "' and"
                            + " `رقم الفاتورة` = " + lbl_billsi_idbills.getText() + "";
                    dbfunc.updateData("bill_local_items", fields, vals, where);
                }
            }

            try {

                String query = "Update items_qty set  `كمية متبقية`=`كمية متبقية`-1, `كمية البيع`=`كمية البيع`+1 where "
                        + " `الكود` like '" + dtm3.getValueAt(row, 0) + "' and `الصنف` like '" + dtm3.getValueAt(row, 1) + "'";
                Statement stmt = (Statement) conn.createStatement();
                stmt.executeUpdate(query);
                CheckZero(dtm3, cellqty);
                int[] cellscolumn = {3, 6};
                lbl_billsi_total.setText(String.format("%.2f", CalculateTotalSI(dtm4, cellscolumn)));
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }//GEN-LAST:event_lbl_billsi_addMouseClicked

    private void lbl_billsi_deleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billsi_deleteMouseClicked
        Vector v = new Vector();
        Vector v1 = new Vector();
        int row1 = 0;
        int row = table_bills_item_add.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(null, "يجب تحديد سطر من الجدول");
        } else {
            Object[] cell = {dtm4.getValueAt(row, 0), dtm4.getValueAt(row, 1)};
            row1 = Compare2Tables(dtm3, cell);
            try {
                v1 = dbfunc.searchBycolumn("bill_local", "`رقم الفاتورة`",
                        " `رقم الفاتورة`= " + lbl_billsi_idbills.getText());
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

            int[] cellqty1 = {row, 3};
            int[] cellqty = {row1, 3};
            int[][] cells = {cellqty1, cellqty};

            if (row1 == -1) {
                CalculateQty(dtm4, null, cells);
                for (int i = 0; i < dtm4.getColumnCount() - 3; i++) {
                    if (i == 3) {
                        v.add("1");
                        continue;
                    }

                    v.add(dtm4.getValueAt(row, i));
                }
                dtm3.addRow(v.toArray());

            } else {
                CalculateQty(dtm4, dtm3, cells);
                CalculateTotalSIRow(dtm4, row);
            }

            try {
                if (!v1.isEmpty()) {
                    //CalculateTotalSIRow(dtm4,row);
                    Object[] fields = {"`الكمية`", "`المجموع`"};
                    Object[] vals = {dtm4.getValueAt(row, 3), dtm4.getValueAt(row, 7)};
                    String where = "where `الكود` like '" + dtm4.getValueAt(row, 0) + "' and"
                            + " `الصنف` like '" + dtm4.getValueAt(row, 1) + "' and"
                            + " `رقم الفاتورة` = " + lbl_billsi_idbills.getText() + "";
                    dbfunc.updateData("bill_local_items", fields, vals, where);
                }
                String query = "Update items_qty set  `كمية متبقية`=`كمية متبقية`+1, `كمية البيع`=`كمية البيع`-1 where "
                        + " `الكود` like '" + dtm4.getValueAt(row, 0) + "' and `الصنف` like '" + dtm4.getValueAt(row, 1) + "'";

                Statement stmt = (Statement) conn.createStatement();
                stmt.executeUpdate(query);

                CheckZeroItems(dtm4, cellqty1, lbl_billsi_idbills.getText());
                int[] cellscolumn = {3, 6};
                lbl_billsi_total.setText(String.format("%.2f", CalculateTotalSI(dtm4, cellscolumn)));
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }//GEN-LAST:event_lbl_billsi_deleteMouseClicked

    private void txt_billsi_shop_search1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_billsi_shop_search1KeyPressed

        if (txt_billsi_shop_search1.isFocusOwner() || txt_billsi_shop_search2.isFocusOwner()) {
            String column = " bit.`الكود`,bit.`الصنف`,bit.`الاسم`,qty.`كمية متبقية`,bit.`سعر المبيع` ";
            String where = " qty.`كمية متبقية` > 0 ";
            String on = " on bit.`الكود`=qty.`الكود` and bit.`الصنف` = qty.`الصنف`";
            String[] comp
                    = {
                        txt_billsi_shop_search1.getText(), txt_billsi_shop_search2.getText(),
                        combo_billsi_shop_search1.getSelectedItem().toString()
                    };
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                utility.resetTable(dtm3);
                String where1 = returnWhereBillsItem(comp, "bit.");
                where = where1.isEmpty() ? where : where1 + " and " + where;
                dbfunc.selectData(dtm3, table_bills_item_shop, column, "bill_trader_items as bit", where,
                        "left join items_qty as qty", on, "", "", "");
                utility.setupTable(dtm3, table_bills_item_shop);
                table_bills_item_shop.setDefaultEditor(Object.class, null);
            }
        }
    }//GEN-LAST:event_txt_billsi_shop_search1KeyPressed

    private void txt_billsi_shop_search2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_billsi_shop_search2KeyPressed
        txt_billsi_shop_search1KeyPressed(evt);
    }//GEN-LAST:event_txt_billsi_shop_search2KeyPressed

    private void txt_billsi_add_search1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_billsi_add_search1KeyPressed
        String where = " `رقم الفاتورة` like " + lbl_billsi_idbills.getText();
        String column = " `الكود`,`الصنف`,`الاسم`,`الكمية`,`السعر الفردي`,`حسم`,`سعر المبيع`,`المجموع`";
        String[] comp
                = {
                    txt_billsi_add_search1.getText(), txt_billsi_add_search2.getText(),
                    combo_billsi_add_search1.getSelectedItem().toString()
                };
        if (txt_billsi_add_search1.isFocusOwner() || txt_billsi_add_search2.isFocusOwner()) {
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                utility.resetTable(dtm4);
                String where1 = returnWhereBillsItem(comp, "");
                where = where1.isEmpty() ? where : where1 + " and " + where;
                dbfunc.selectData(dtm4, table_bills_item_add, column, "bill_local_items", where, "", "", "", "", "");

                utility.setupTable(dtm4, table_bills_item_add);
            }
        }
    }//GEN-LAST:event_txt_billsi_add_search1KeyPressed

    private void txt_billsi_add_search2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_billsi_add_search2KeyPressed
        txt_billsi_add_search1KeyPressed(evt);
    }//GEN-LAST:event_txt_billsi_add_search2KeyPressed

    private void btn_addexp_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addexp_addActionPerformed
        if (txt_addexp_name.getText().isEmpty() || txt_addexp_type.getText().isEmpty()
                || date_addexp_date.getDate() == null) {
            JOptionPane.showMessageDialog(null, "  الاسم أو التاريخ أو النوع فراغ");
        } else {
            combo_expenses_currency.setSelectedItem("$");
            CheckTextArea(area_addexp_add);

            String[] starray
                    = {
                        txt_addexp_invoicenb.getText(), txt_addexp_type.getText(),
                        txt_addexp_name.getText(),
                        txt_addexp_phone.getText(), Datesubstring(date_addexp_date.getDate().toString()),
                        formater.format(date_addexp_date.getDate()),
                        txt_addexp_total.getText(), txt_addexp_payed.getText(),
                        txt_addexp_remain.getText(),
                        area_addexp_add.getText()
                    };

            if (btn_addexp_add.getText().equals("اضافة")) {
                boolean insert = dbfunc.insertData("expenses", starray, "");

                if (insert) {
                    JOptionPane.showMessageDialog(null, "تمت عملية الاضافة");
                    ShowExpensesBill();
                } else {
                    JOptionPane.showMessageDialog(null, "حصل خطأ أثناء عملية الاضافةالرجاء اعادة المحاولة");
                }
            } else if (btn_addexp_add.getText().equals("تعديل")) {

                String[] fields
                        = {"`رقم الفاتورة`", "`العنوان`", "`الاسم`",
                            "`الهاتف`", "`التاريخ`", "`تاريخ`", "`المجموع`",
                            "`المدفوع`", "`المتبقي`", "`تفاصيل`"};

                String where = " where `رقم الفاتورة` = " + txt_addexp_invoicenb.getText();
                boolean update = dbfunc.updateData("expenses", fields, starray, where);
                if (update) {
                    JOptionPane.showMessageDialog(null, "تمت عملية التعديل");
                    ShowExpensesBill();
                } else {
                    JOptionPane.showMessageDialog(null, "خطأ في عملية التعديل");
                }
            }
            cmp.ClearCmp(pan_control_expenses_add);
            area_addexp_add.setText("");

        }

    }//GEN-LAST:event_btn_addexp_addActionPerformed

    private void btn_searchexp_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_searchexp_searchActionPerformed
        String column = "`رقم الفاتورة`,`العنوان`,`الاسم`,`الهاتف`,`التاريخ`,`المجموع`,`المدفوع`,"
                + "`المتبقي`,`تفاصيل` ";
        String table = "expenses";
        String where = "";
        String tempwhere = "";

        // check the selected checkbox to make the query
        if (check_searchexp_search1.isSelected()) {
            where = " `رقم الفاتورة` like '" + txt_searchexp_search1.getText() + "' ";
        }
        if (check_searchexp_search2.isSelected()) {
            tempwhere = " `الاسم` like '%" + txt_searchexp_search2.getText() + "%'";
            where = where.isEmpty() ? tempwhere : where + " And " + tempwhere;
        }
        if (check_searchexp_search3.isSelected()) {
            tempwhere = " `فاتورة` like '%" + txt_searchexp_search3.getText() + "%' ";
            where = where.isEmpty() ? tempwhere : where + " And " + tempwhere;
        }
        if (check_searchexp_search4.isSelected()) {
            // for the datepicker check the value of it and if null then give it a value rand but not null.
            String[] dateCondition = allpurpose.CreateDateCondition(datepicker_searchexp_from, datepicker_searchexp_to);
            tempwhere = " تاريخ >= " + dateCondition[0] + " and تاريخ <= " + dateCondition[1] + " ";
            where = where.isEmpty() ? tempwhere : where + " And " + tempwhere;
        }
        utility.resetTable(dtm5);
        combo_expenses_currency.setSelectedItem("$");
        dbfunc.selectData(dtm5, table_expenses, column, table, where, "", "", "", "", "");
        utility.setupTable(dtm5, table_expenses);
        table_expenses.setDefaultEditor(Object.class, null);
    }//GEN-LAST:event_btn_searchexp_searchActionPerformed

    private void check_searchexp_search1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_searchexp_search1StateChanged
        if (check_searchexp_search1.isSelected()) {
            txt_searchexp_search1.setEnabled(true);
            txt_searchexp_search1.requestFocusInWindow();
        } else {
            txt_searchexp_search1.setEnabled(false);
        }
    }//GEN-LAST:event_check_searchexp_search1StateChanged

    private void check_searchexp_search2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_searchexp_search2StateChanged
        if (check_searchexp_search2.isSelected()) {
            txt_searchexp_search2.setEnabled(true);
            txt_searchexp_search2.requestFocusInWindow();
        } else {
            txt_searchexp_search2.setEnabled(false);
        }
    }//GEN-LAST:event_check_searchexp_search2StateChanged

    private void check_searchexp_search3StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_searchexp_search3StateChanged
        if (check_searchexp_search3.isSelected()) {
            txt_searchexp_search3.setEnabled(true);
            txt_searchexp_search3.requestFocusInWindow();
        } else {
            txt_searchexp_search3.setEnabled(false);
        }
    }//GEN-LAST:event_check_searchexp_search3StateChanged

    private void check_searchexp_search4StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_check_searchexp_search4StateChanged
        if (check_searchexp_search4.isSelected()) {
            datepicker_searchexp_from.setEnabled(true);
            datepicker_searchexp_to.setEnabled(true);
        } else {
            datepicker_searchexp_from.setEnabled(false);
            datepicker_searchexp_to.setEnabled(false);
        }
    }//GEN-LAST:event_check_searchexp_search4StateChanged

    private void lbl_expenses_addMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_expenses_addMouseClicked
        showHidePanel(pan_control_expenses_card);

        Vector v1 = new Vector();
        v1.add(0);
        v1.add(lbl_expenses_add);
        allpurpose.LabelSelection(v1, pan_expenses_control);

        cl4.show(pan_control_expenses_card, "expenses_add");
        txt_addexp_name.requestFocusInWindow();
        cmp.ClearCmp(pan_control_expenses_add);
        area_addexp_add.setText("");
        btn_addexp_add.setText("اضافة");
        try {
            IncrementID("expenses", "`رقم الفاتورة`", txt_addexp_invoicenb);
        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_lbl_expenses_addMouseClicked

    private void lbl_expenses_searchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_expenses_searchMouseClicked
        showHidePanel(pan_control_expenses_card);

        LabelSelectionByColor(pan_expenses_control, lbl_expenses_search, lbl_expenses_search.getName());

        cmp.ClearCmp(pan_control_expenses_search);
        cl4.show(pan_control_expenses_card, "expenses_search");

    }//GEN-LAST:event_lbl_expenses_searchMouseClicked

    private void lbl_expenses_editMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_expenses_editMouseClicked

        int row = table_expenses.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(null, "يجب تحديد سطر من الجدول");
        } else {
            LabelSelectionByColor(pan_expenses_control, lbl_expenses_edit, lbl_expenses_edit.getName());
            String Table_click = (table_expenses.getModel().getValueAt(row, 0).toString());
            String table = "expenses";
            String where = "  `رقم الفاتورة` = " + Table_click + " ";
            String columns = "`رقم الفاتورة`,`العنوان`,`الاسم`,`الهاتف`,`تاريخ`,`المجموع`,"
                    + "`المدفوع`,`المتبقي`,`تفاصيل`";
            try {
                Component[] comp
                        = {
                            txt_addexp_invoicenb, txt_addexp_type, txt_addexp_name,
                            txt_addexp_phone, date_addexp_date, txt_addexp_total, txt_addexp_payed,
                            txt_addexp_remain, area_addexp_add
                        };
                Vector vec = dbfunc.searchBycolumn(table, columns, where);
                utility.ValueToField(comp, vec);
                btn_addexp_add.setText("تعديل");
                cl4.show(pan_control_expenses_card, "expenses_add");
                pan_control_expenses_card.setVisible(true);
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_lbl_expenses_editMouseClicked

    private void lbl_expenses_deleteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_expenses_deleteMouseClicked
        int row = table_expenses.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(null, "يجب تحديد سطر من الجدول");
        } else {
            String Table_click = table_expenses.getModel().getValueAt(row, 0).toString();
            String table = "expenses";
            String[] fields
                    = {
                        "`رقم الفاتورة`"
                    };
            String[] vals = {Table_click};
            boolean deleted = dbfunc.deleteData(table, fields, vals);
            if (deleted) {
                JOptionPane.showMessageDialog(null, "تمت عملية الحذف");
                ShowExpensesBill();
            } else {
                JOptionPane.showMessageDialog(null, "حصل خطأ أثناء عملية الحذف الرجاء اعادة المحاولة");
            }
        }
    }//GEN-LAST:event_lbl_expenses_deleteMouseClicked

    private void txt_addbillt_totalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_addbillt_totalFocusLost
        if (txt_addbillt_payed.getText().isEmpty()) {
            txt_addbillt_remain.setText(txt_addbillt_total.getText());
            txt_addbillt_payed.setText("0");
        } else {
            System.out.println("Base Plan Cost: $%.2f%n");
            Float remaining = Float.parseFloat(txt_addbillt_total.getText())
                    - Float.parseFloat(txt_addbillt_payed.getText());
            txt_addbillt_remain.setText(String.format("%.2f", remaining));
        }
    }//GEN-LAST:event_txt_addbillt_totalFocusLost

    private void txt_addbillt_payedFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_addbillt_payedFocusLost
        if (txt_addbillt_total.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "يجب ادخال المجموع اولا");
            txt_addbillt_payed.setText("");
        } else {
            Float remaining = Float.parseFloat(txt_addbillt_total.getText())
                    - Float.parseFloat(txt_addbillt_payed.getText());
            txt_addbillt_remain.setText(String.format("%.2f", remaining));
        }
    }//GEN-LAST:event_txt_addbillt_payedFocusLost

    private void txt_addbillti_totalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_addbillti_totalFocusLost
        if (txt_addbillti_qty.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "يجب تحديد الكمية اولا");
            txt_addbillti_total.setText("");
        } else if (!txt_addbillti_qty.getText().isEmpty()) {
            txt_addbillti_discount.setText("0");
            float unitprice = Float.parseFloat(txt_addbillti_total.getText())
                    / Float.parseFloat(txt_addbillti_qty.getText());
            txt_addbillti_unitprice.setText(String.format("%.2f", unitprice));
            txt_addbillti_real_total.setText(txt_addbillti_unitprice.getText());
        }
    }//GEN-LAST:event_txt_addbillti_totalFocusLost

    private void txt_addbillti_discountFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_addbillti_discountFocusLost
        if (txt_addbillti_qty.getText().isEmpty() || txt_addbillti_unitprice.getText().isEmpty()) {
            txt_addbillti_discount.setText("0");
        } else if (!txt_addbillti_unitprice.getText().isEmpty()) {
            if (txt_addbillti_discount.getText().isEmpty()) {
                txt_addbillti_discount.setText("0");
            } else {
                float newtotal = Float.parseFloat(txt_addbillti_unitprice.getText())
                        + (Float.parseFloat(txt_addbillti_unitprice.getText())
                        * Float.parseFloat(txt_addbillti_discount.getText()) / 100);

                txt_addbillti_real_total.setText(String.format("%.2f", newtotal));
            }

        }
    }//GEN-LAST:event_txt_addbillti_discountFocusLost

    private void txt_addbillti_real_totalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_addbillti_real_totalFocusLost
        if (txt_addbillti_qty.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "يجب تحديد الكمية اولا");
            txt_addbillti_real_total.setText("");
        } else if (!txt_addbillti_qty.getText().isEmpty() && txt_addbillti_total.getText().isEmpty()) {
            txt_addbillti_discount.setText("");
            txt_addbillti_real_total.setText("");
        } else if (!txt_addbillti_qty.getText().isEmpty() && !txt_addbillti_total.getText().isEmpty()) {
            float discount = ((Float.parseFloat(txt_addbillti_real_total.getText())
                    - Float.parseFloat(txt_addbillti_unitprice.getText()))
                    / Float.parseFloat(txt_addbillti_unitprice.getText()))
                    * 100;
            txt_addbillti_discount.setText(String.format("%.0f", discount));
        }
    }//GEN-LAST:event_txt_addbillti_real_totalFocusLost

    private void txt_addexp_totalFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_addexp_totalFocusLost
        if (txt_addexp_payed.getText().isEmpty()) {
            txt_addexp_remain.setText(txt_addexp_total.getText());
            txt_addexp_payed.setText("0");
        } else {
            Float remaining = Float.parseFloat(txt_addexp_total.getText())
                    - Float.parseFloat(txt_addexp_payed.getText());
            System.out.println(remaining);
            txt_addexp_remain.setText(String.valueOf(remaining));
        }
    }//GEN-LAST:event_txt_addexp_totalFocusLost

    private void txt_addexp_payedFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_addexp_payedFocusLost
        if (txt_addexp_total.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "يجب ادخال المجموع اولا");
            txt_addexp_payed.setText("");
        } else {
            Float remaining = Float.parseFloat(txt_addexp_total.getText())
                    - Float.parseFloat(txt_addexp_payed.getText());
            txt_addexp_remain.setText(String.valueOf(remaining));
        }
    }//GEN-LAST:event_txt_addexp_payedFocusLost

    private void table_bill_traderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_bill_traderMouseClicked
        if (evt.getClickCount() == 2) {
            evt.consume();
            int row = table_bill_trader.getSelectedRow();
            lbl_billti_invoicenb.setText((String) table_bill_trader.getValueAt(row, 0));
            ShowBilltitems();
            cl.show(CardPanel, "billt_items");
            cl3.show(pan_Billt_item_card, "billti_add");
            pan_Billt_item_card.setVisible(true);

        }
    }//GEN-LAST:event_table_bill_traderMouseClicked

    private void lbl_billti_confirmMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_billti_confirmMouseClicked
        lbl_billti_invoicenb.setText("");
        for (int i = 0; i < dtm2.getRowCount(); i++) {

            try {
                CalculateQtyDB(dtm2.getValueAt(i, 4), dtm2.getValueAt(i, 0), dtm2.getValueAt(i, 1));

            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        lb_add_bill_traderMouseClicked(evt);
        //lbl_billt_addMouseClicked(evt);
    }//GEN-LAST:event_lbl_billti_confirmMouseClicked

    private void btn_billsi_confirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_billsi_confirmActionPerformed
        Vector v1 = new Vector();
        Vector v = new Vector();
        int[] cellscolumn = {3, 6};
        try {
            v1 = dbfunc.searchBycolumn("bill_local", "`رقم الفاتورة`", " `رقم الفاتورة`="
                    + " " + lbl_billsi_idbills.getText());
        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (v1.isEmpty()) {
            for (int i = 0; i < dtm4.getRowCount(); i++) {
                v.removeAllElements();
                for (int j = 0; j < dtm4.getColumnCount(); j++) {
                    v.add(dtm4.getValueAt(i, j));
                }
                v.add(lbl_billsi_idbills.getText());
                dbfunc.insertData("bill_local_items", v.toArray(), "");
            }
        }

        txt_addbills_payed.setText("0");
        combo_billsi_currency.setSelectedItem("$");
        txt_addbills_total_real.setText(String.format("%.2f", CalculateTotalSI(dtm4, cellscolumn)));
        txt_addbills_remain.setText(String.format("%.2f", CalculateTotalSI(dtm4, cellscolumn)));

        btn_addbills_add.setText("موافق");
        cl.show(CardPanel, "bill_shop");
    }//GEN-LAST:event_btn_billsi_confirmActionPerformed

    private void txt_addbills_payedFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_addbills_payedFocusLost
        Float remaining = Float.parseFloat(txt_addbills_total_real.getText())
                - Float.parseFloat(txt_addbills_payed.getText());
        txt_addbills_remain.setText(String.format("%.2f", remaining));
    }//GEN-LAST:event_txt_addbills_payedFocusLost

    private void table_bill_shopMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_bill_shopMouseClicked
        if (evt.getClickCount() == 2) {
            evt.consume();
            int row = table_bill_shop.getSelectedRow();
            lbl_billsi_idbills.setText(String.valueOf(table_bill_shop.getValueAt(row, 0)));
            lbl_bills_editMouseClicked(evt);
            showPanBillsi();
            int[] cellscolumn = {3, 6};
            lbl_billsi_total.setText(String.format("%.2f", CalculateTotalSI(dtm4, cellscolumn)));
            cl.show(CardPanel, "bills_items");

        }
    }//GEN-LAST:event_table_bill_shopMouseClicked

    private void btn_report_monthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_report_monthActionPerformed
        report.dateButtonClicked(btn_report_month, datepicker_report_from, datepicker_report_to);
        combo_report_year.setVisible(false);
        datepicker_report_toActionPerformed(evt);
    }//GEN-LAST:event_btn_report_monthActionPerformed

    private void btn_report_yearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_report_yearActionPerformed

        combo_report_year.setVisible(true);
        combo_report_year.removeAllItems();
        for (int years = 2017; years <= Calendar.getInstance().get(Calendar.YEAR); years++) {
            combo_report_year.addItem(years);
        }
        report.dateButtonClicked(btn_report_year, datepicker_report_from, datepicker_report_to);
        datepicker_report_toActionPerformed(evt);

    }//GEN-LAST:event_btn_report_yearActionPerformed

    private void combo_reportitems_searchItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_reportitems_searchItemStateChanged
        if (evt.getStateChange() == 1) {

            String[] dateCondition = allpurpose.CreateDateCondition(datepicker_reportitems_from, datepicker_reportitems_to);
            utility.resetTable(dtm8);
            String[] comp
                    = {
                        combo_reportitems_search.getSelectedItem().toString(),
                        combo_reportitems_search1.getSelectedItem().toString()};
            report.SelectByConditionForItems(pan_report_items_control, table_items, comp, dateCondition, dtm8);

        }
    }//GEN-LAST:event_combo_reportitems_searchItemStateChanged

    private void combo_billt_currencyItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_billt_currencyItemStateChanged

        if (evt.getStateChange() == 1) {
            Component[] cmp = {txt_addbillt_total, txt_addbillt_payed, txt_addbillt_remain};
            allpurpose.checkCurrency(combo_billt_currency.getSelectedItem().toString(), cmp);
            allpurpose.checkCurrencyForTable(combo_billt_currency.getSelectedItem().toString(), dtm, 4);
            allpurpose.checkCurrencyForTable(combo_billt_currency.getSelectedItem().toString(), dtm, 5);
            allpurpose.checkCurrencyForTable(combo_billt_currency.getSelectedItem().toString(), dtm, 6);
        }

    }//GEN-LAST:event_combo_billt_currencyItemStateChanged

    private void combo_bills_currencyItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_bills_currencyItemStateChanged
        if (evt.getStateChange() == 1) {
            Component[] cmp = {txt_addbills_payed, txt_addbills_remain, txt_addbills_total_real};
            allpurpose.checkCurrency(combo_bills_currency.getSelectedItem().toString(), cmp);
            allpurpose.checkCurrencyForTable(combo_bills_currency.getSelectedItem().toString(), dtm1, 4);
            allpurpose.checkCurrencyForTable(combo_bills_currency.getSelectedItem().toString(), dtm1, 5);
            allpurpose.checkCurrencyForTable(combo_bills_currency.getSelectedItem().toString(), dtm1, 6);
        }

    }//GEN-LAST:event_combo_bills_currencyItemStateChanged

    private void jLabel56MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel56MouseClicked
        cl5.show(pan_report, "report_payment");

        pan_report_payment.setVisible(true);
        pan_report_payment_details.setVisible(true);
        combo_report_year.setVisible(false);
    }//GEN-LAST:event_jLabel56MouseClicked

    private void jLabel22MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel22MouseClicked
        cl5.show(pan_report, "report_item");

        //btn_reportitems_month.doClick();
        dbfunc.refreshCB(combo_reportitems_search, "type", "type");

    }//GEN-LAST:event_jLabel22MouseClicked

    private void jLabel101MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel101MouseClicked
        cl6.show(pan_login_card, "login_change");
        jLabel111.setVisible(true);
        cmp.ClearCmp(pan_login_change);
        txt_change_pass1.requestFocusInWindow();
    }//GEN-LAST:event_jLabel101MouseClicked

    private void jLabel102MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel102MouseClicked
        LoginSuccess();
    }//GEN-LAST:event_jLabel102MouseClicked

    private void btn_changeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_changeActionPerformed
        boolean success = false;
        try {
            success = login.ChangePassword(txt_change_pass1.getPassword(), txt_change_pass2.getPassword(), txt_change_pass3.getPassword());
            if (success) {
                cmp.ClearCmp(pan_login_change);
                cl6.show(pan_login_card, "login_main");
                jLabel111.setVisible(false);
                cmp.ClearCmp(pan_login_main);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_btn_changeActionPerformed

    private void txt_login_userFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_login_userFocusLost
        try {
            boolean success = login.CheckUser(txt_login_user.getText());
            if (success) {
                txt_login_pass.setEnabled(true);
            } else {
                JOptionPane.showMessageDialog(null, "المستخدم غير موجود");
                txt_login_user.requestFocusInWindow();
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_txt_login_userFocusLost

    private void jLabel111MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel111MouseClicked
        cmp.ClearCmp(pan_login_main);
        cl6.show(pan_login_card, "login_main");
    }//GEN-LAST:event_jLabel111MouseClicked

    private void txt_login_passKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_login_passKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            LoginSuccess();
        }
    }//GEN-LAST:event_txt_login_passKeyPressed

    private void txt_item_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_item_searchKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            items.searchItemsQty(dtm7, table_items_show, txt_item_search.getText());
        }
    }//GEN-LAST:event_txt_item_searchKeyPressed

    private void combo_billti_currencyItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_billti_currencyItemStateChanged
        if (evt.getStateChange() == 1) {
            Component[] cmp = {txt_addbillti_total, txt_addbillti_real_total, txt_addbillti_unitprice};
            allpurpose.checkCurrency(combo_billti_currency.getSelectedItem().toString(), cmp);
            allpurpose.checkCurrencyForTable(combo_billti_currency.getSelectedItem().toString(), dtm2, 5);
            allpurpose.checkCurrencyForTable(combo_billti_currency.getSelectedItem().toString(), dtm2, 6);
            allpurpose.checkCurrencyForTable(combo_billti_currency.getSelectedItem().toString(), dtm2, 8);
        }
    }//GEN-LAST:event_combo_billti_currencyItemStateChanged

    private void table_bills_item_addFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_table_bills_item_addFocusGained
        System.out.println("focus gained");
        billsi.discountPrice(dtm4, table_bills_item_add, 5, Integer.parseInt(lbl_billsi_idbills.getText()));
        int[] cellscolumn = {3, 6};
        lbl_billsi_total.setText(String.format("%.2f", CalculateTotalSI(dtm4, cellscolumn)));
    }//GEN-LAST:event_table_bills_item_addFocusGained

    private void combo_expenses_currencyItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_expenses_currencyItemStateChanged
        if (evt.getStateChange() == 1) {
            Component[] cmp = {txt_addexp_total, txt_addexp_payed, txt_addexp_remain};
            allpurpose.checkCurrency(combo_expenses_currency.getSelectedItem().toString(), cmp);
            allpurpose.checkCurrencyForTable(combo_expenses_currency.getSelectedItem().toString(), dtm5, 6);
            allpurpose.checkCurrencyForTable(combo_expenses_currency.getSelectedItem().toString(), dtm5, 7);
            allpurpose.checkCurrencyForTable(combo_expenses_currency.getSelectedItem().toString(), dtm5, 8);
        }
    }//GEN-LAST:event_combo_expenses_currencyItemStateChanged

    private void datepicker_reportitems_toActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_datepicker_reportitems_toActionPerformed
        String[] dateCondition = allpurpose.CreateDateCondition(datepicker_reportitems_from, datepicker_reportitems_to);
        String[] compValue = {combo_reportitems_search.getSelectedItem().toString(),
            combo_reportitems_search1.getSelectedItem().toString()};
        utility.resetTable(dtm8);
        report.SelectByConditionForItems(pan_report_items_control, table_items, compValue, dateCondition, dtm8);
    }//GEN-LAST:event_datepicker_reportitems_toActionPerformed

    private void lbl_report_items_traderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_report_items_traderMouseClicked

        LabelSelectionByColor(pan_report_items_control, lbl_report_items_trader, lbl_report_items_trader.getName());
        dbfunc.refreshCB(combo_reportitems_search1, "bill_trader_items", "الاسم");

        String[] dateCondition = allpurpose.CreateDateCondition(datepicker_reportitems_from, datepicker_reportitems_to);
        //utility.resetTable(dtm8);
        if (combo_reportitems_search1.getSelectedIndex() != 0) {
            String[] comp
                    = {
                        combo_reportitems_search.getSelectedItem().toString(),
                        combo_reportitems_search1.getSelectedItem().toString()};
            report.SelectByConditionForItems(pan_report_items_control, table_items, comp, dateCondition, dtm8);
        }
    }//GEN-LAST:event_lbl_report_items_traderMouseClicked

    private void lbl_report_items_shopMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_report_items_shopMouseClicked

        LabelSelectionByColor(pan_report_items_control, lbl_report_items_shop, lbl_report_items_shop.getName());
        dbfunc.refreshCB(combo_reportitems_search1, " bill_local_items ", "الاسم");
        String[] dateCondition = allpurpose.CreateDateCondition(datepicker_reportitems_from, datepicker_reportitems_to);
        //utility.resetTable(dtm8);
        if (combo_reportitems_search1.getSelectedIndex() != 0) {
            String[] comp
                    = {
                        combo_reportitems_search.getSelectedItem().toString(),
                        combo_reportitems_search1.getSelectedItem().toString()};
            report.SelectByConditionForItems(pan_report_items_control, table_items, comp, dateCondition, dtm8);
        }

    }//GEN-LAST:event_lbl_report_items_shopMouseClicked

    private void btn_reportitems_monthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_reportitems_monthActionPerformed
        report.dateButtonClicked(btn_reportitems_month, datepicker_reportitems_from, datepicker_reportitems_to);

        //LabelSelectionByColor(pan_report_items_control, lbl_report_items_shop, lbl_report_items_shop.getName());
        //datepicker_reportitems_toActionPerformed(evt);
    }//GEN-LAST:event_btn_reportitems_monthActionPerformed

    private void btn_reportitems_yearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_reportitems_yearActionPerformed
        report.dateButtonClicked(btn_reportitems_year, datepicker_reportitems_from, datepicker_reportitems_to);
        datepicker_reportitems_toActionPerformed(evt);
    }//GEN-LAST:event_btn_reportitems_yearActionPerformed

    private void lbl_report_expensesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_report_expensesMouseClicked

        LabelSelectionByColor(pan_repot_payment_control, lbl_report_expenses, lbl_report_expenses.getName());

        Component[] cmp = {lbl_report_total, lbl_report_payed, lbl_report_remaining};
        String[] dateCondition = allpurpose.CreateDateCondition(datepicker_report_from, datepicker_report_to);
        dbfunc.refreshCB(combo_report_payement_name, "expenses", "الاسم");
        utility.resetTable(dtm6);
        report.calculateTableReport(dtm6, table_report_bills, "expenses", dateCondition, "", cmp);

    }//GEN-LAST:event_lbl_report_expensesMouseClicked

    private void lbl_report_shopMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_report_shopMouseClicked

        LabelSelectionByColor(pan_repot_payment_control, lbl_report_shop, lbl_report_shop.getName());

        Component[] cmp = {lbl_report_total, lbl_report_payed, lbl_report_remaining};
        String[] dateCondition = allpurpose.CreateDateCondition(datepicker_report_from, datepicker_report_to);
        dbfunc.refreshCB(combo_report_payement_name, "bill_local", "الاسم");
        utility.resetTable(dtm6);
        report.calculateTableReport(dtm6, table_report_bills, "bill_local", dateCondition, "", cmp);
    }//GEN-LAST:event_lbl_report_shopMouseClicked

    private void lbl_report_traderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_report_traderMouseClicked

        LabelSelectionByColor(pan_repot_payment_control, lbl_report_trader, lbl_report_trader.getName());

        String[] dateCondition = allpurpose.CreateDateCondition(datepicker_report_from, datepicker_report_to);
        Component[] cmp = {lbl_report_total, lbl_report_payed, lbl_report_remaining};
        dbfunc.refreshCB(combo_report_payement_name, "bill_trader", "الاسم");
        utility.resetTable(dtm6);
        report.calculateTableReport(dtm6, table_report_bills, "bill_trader", dateCondition, "", cmp);
    }//GEN-LAST:event_lbl_report_traderMouseClicked

    private void datepicker_report_toActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_datepicker_report_toActionPerformed

        Component[] cmp = {lbl_report_total_payed, lbl_report_total_sell, lbl_report_total_paysell, lbl_report_total_remaining_pay,
            lbl_report_total_remaining_get, lbl_report_total_remaining};

        String[] datecondition = allpurpose.CreateDateCondition(datepicker_report_from, datepicker_report_to);

        try {
            report.CalculateMonthReport(datecondition, cmp);
        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_datepicker_report_toActionPerformed

    private void combo_report_yearItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_report_yearItemStateChanged
        if (evt.getStateChange() == 1) {
            report.ComboYearClicked(combo_report_year.getSelectedItem(), datepicker_report_from, datepicker_report_to);
            String[] dateCondition = allpurpose.CreateDateCondition(datepicker_report_from, datepicker_report_to);

            Component[] cmp = {lbl_report_total_payed, lbl_report_total_sell, lbl_report_total_paysell, lbl_report_total_remaining_pay,
                lbl_report_total_remaining_get, lbl_report_total_remaining};

            try {
                report.CalculateMonthReport(dateCondition, cmp);

            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_combo_report_yearItemStateChanged

    private void lbl_addbillti_typeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_addbillti_typeMouseEntered
        lbl_addbillti_type.setForeground(Color.red);
    }//GEN-LAST:event_lbl_addbillti_typeMouseEntered

    private void lbl_addbillti_typeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_addbillti_typeMouseExited
        lbl_addbillti_type.setForeground(Color.black);
    }//GEN-LAST:event_lbl_addbillti_typeMouseExited

    private void lbl_addbillti_unitMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_addbillti_unitMouseEntered
        lbl_addbillti_unit.setForeground(Color.red);
    }//GEN-LAST:event_lbl_addbillti_unitMouseEntered

    private void lbl_addbillti_unitMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_addbillti_unitMouseExited
        lbl_addbillti_unit.setForeground(Color.black);
    }//GEN-LAST:event_lbl_addbillti_unitMouseExited

    private void combo_billsi_shop_search1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_billsi_shop_search1ItemStateChanged
        if (evt.getStateChange() == 1) {
            String column = " bit.`الكود`,bit.`الصنف`,bit.`الاسم`,qty.`كمية متبقية`,bit.`سعر المبيع` ";
            String where = " qty.`كمية متبقية` > 0 ";
            String on = " on bit.`الكود`=qty.`الكود` and bit.`الصنف` = qty.`الصنف`";
            String[] comp
                    = {
                        txt_billsi_shop_search1.getText(), txt_billsi_shop_search2.getText(),
                        combo_billsi_shop_search1.getSelectedItem().toString()
                    };

            String where1 = returnWhereBillsItem(comp, "bit.");
            where = where1.isEmpty() ? where : where1 + " and " + where;
            combo_billsi_currency.setSelectedItem("$");
            utility.resetTable(dtm3);
            dbfunc.selectData(dtm3, table_bills_item_shop, column, "bill_trader_items as bit", where,
                    "left join items_qty as qty", on, "", "", "");
            utility.setupTable(dtm3, table_bills_item_shop);
            table_bills_item_shop.setDefaultEditor(Object.class, null);
        }
    }//GEN-LAST:event_combo_billsi_shop_search1ItemStateChanged

    private void lbl_control_exitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_exitMouseClicked
        String value = promptForDiscardValues(pan_control_items);
        if (!value.isEmpty()) {
            int choice = JOptionPane.showConfirmDialog(null, "هل تريد إغلاق الفاتورة الحالية؟", "اغلاق",
                    JOptionPane.YES_NO_OPTION);
            if (choice == 0) {
                int choice1 = JOptionPane.showConfirmDialog(null, "هل تريد إغلاق البرنامج؟", "اغلاق", JOptionPane.YES_NO_OPTION);
                if (choice1 == 0) {
                    dbfunc.closeApp(conn);
                }
            }
        } else {
            int choice1 = JOptionPane.showConfirmDialog(null, "هل تريد إغلاق البرنامج؟", "اغلاق", JOptionPane.YES_NO_OPTION);
            if (choice1 == 0) {
                dbfunc.closeApp(conn);
            }
        }

    }//GEN-LAST:event_lbl_control_exitMouseClicked

    private void lbl_control_homeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_homeMouseClicked
        String value = promptForDiscardValues(pan_control_items);
        if (!value.isEmpty()) {
            int choice = JOptionPane.showConfirmDialog(null, "هل تريد إغلاق الفاتورة الحالية؟", "اغلاق",
                    JOptionPane.YES_NO_OPTION);
            if (choice == 0) {
                LabelSelectionByBorder(pan_control_items, lbl_control_home, lbl_control_home.getName());
                cl.show(CardPanel, "main");
            }
        } else {
            LabelSelectionByBorder(pan_control_items, lbl_control_home, lbl_control_home.getName());
            cl.show(CardPanel, "main");
        }

    }//GEN-LAST:event_lbl_control_homeMouseClicked

    private void lbl_control_shopMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_shopMouseClicked
        String value = promptForDiscardValues(pan_control_items);
        if (!value.isEmpty()) {
            int choice = JOptionPane.showConfirmDialog(null, "هل تريد إغلاق الفاتورة الحالية؟", "اغلاق",
                    JOptionPane.YES_NO_OPTION);
            if (choice == 0) {
                LabelSelectionByBorder(pan_control_items, lbl_control_shop, lbl_control_shop.getName());
                showBills();
                lbl_bills_addMouseClicked(evt);
            }
        } else {
            LabelSelectionByBorder(pan_control_items, lbl_control_shop, lbl_control_shop.getName());
            showBills();
            lbl_bills_addMouseClicked(evt);
        }

    }//GEN-LAST:event_lbl_control_shopMouseClicked

    private void lbl_control_itemsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_itemsMouseClicked
        String value = promptForDiscardValues(pan_control_items);
        if (!value.isEmpty()) {
            int choice = JOptionPane.showConfirmDialog(null, "هل تريد إغلاق الفاتورة الحالية؟", "اغلاق",
                    JOptionPane.YES_NO_OPTION);
            if (choice == 0) {
                try {
                    LabelSelectionByBorder(pan_control_items, lbl_control_items, lbl_control_items.getName());
                    ShowItemPan();
                } catch (SQLException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            try {
                LabelSelectionByBorder(pan_control_items, lbl_control_items, lbl_control_items.getName());
                ShowItemPan();
            } catch (SQLException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_lbl_control_itemsMouseClicked

    private void lbl_control_traderMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_traderMouseClicked
        String value = promptForDiscardValues(pan_control_items);
        if (!value.isEmpty()) {
            int choice = JOptionPane.showConfirmDialog(null, "هل تريد إغلاق الفاتورة الحالية؟", "اغلاق",
                    JOptionPane.YES_NO_OPTION);
            if (choice == 0) {
                LabelSelectionByBorder(pan_control_items, lbl_control_trader, lbl_control_trader.getName());

                showBillt();

            }
        } else {
            LabelSelectionByBorder(pan_control_items, lbl_control_trader, lbl_control_trader.getName());

            showBillt();

        }

    }//GEN-LAST:event_lbl_control_traderMouseClicked

    private void lbl_control_reportsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_reportsMouseClicked
        String value = promptForDiscardValues(pan_control_items);
        if (!value.isEmpty()) {
            int choice = JOptionPane.showConfirmDialog(null, "هل تريد إغلاق الفاتورة الحالية؟", "اغلاق",
                    JOptionPane.YES_NO_OPTION);
            if (choice == 0) {
                LabelSelectionByBorder(pan_control_items, lbl_control_reports, lbl_control_reports.getName());
                cl.show(CardPanel, "report");
                cl5.show(pan_report, "report_main");
            }
        } else {
            LabelSelectionByBorder(pan_control_items, lbl_control_reports, lbl_control_reports.getName());
            cl.show(CardPanel, "report");
            cl5.show(pan_report, "report_main");
        }

    }//GEN-LAST:event_lbl_control_reportsMouseClicked

    private void lbl_control_expensesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_expensesMouseClicked
        String value = promptForDiscardValues(pan_control_items);
        if (!value.isEmpty()) {
            int choice = JOptionPane.showConfirmDialog(null, "هل تريد إغلاق الفاتورة الحالية؟", "اغلاق",
                    JOptionPane.YES_NO_OPTION);
            if (choice == 0) {
                LabelSelectionByBorder(pan_control_items, lbl_control_expenses, lbl_control_expenses.getName());
                ShowExpensesBill();
            }
        } else {
            LabelSelectionByBorder(pan_control_items, lbl_control_expenses, lbl_control_expenses.getName());
            ShowExpensesBill();
        }
//lb_add_expensesMouseClicked(evt);

    }//GEN-LAST:event_lbl_control_expensesMouseClicked

    private void combo_reportitems_search1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_reportitems_search1ItemStateChanged
        if (evt.getStateChange() == 1) {
            combo_reportitems_searchItemStateChanged(evt);
        }
    }//GEN-LAST:event_combo_reportitems_search1ItemStateChanged

    private void combo_item_searchItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_item_searchItemStateChanged

        if (evt.getStateChange() == 1) {
            //utility.resetTable(dtm7);
            items.selectItemsByCombo(dtm7, table_items_show, combo_item_search.getSelectedItem().toString(),
                    combo_item_search1.getSelectedItem().toString());
        }
    }//GEN-LAST:event_combo_item_searchItemStateChanged

    private void combo_item_search1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_item_search1ItemStateChanged

        combo_item_searchItemStateChanged(evt);
    }//GEN-LAST:event_combo_item_search1ItemStateChanged

    private void combo_billsi_currencyItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_billsi_currencyItemStateChanged
        if (evt.getStateChange() == 1) {
            allpurpose.checkCurrencyForTable(combo_billsi_currency.getSelectedItem().toString(), dtm3, 4);
            allpurpose.checkCurrencyForTable(combo_billsi_currency.getSelectedItem().toString(), dtm4, 4);
            allpurpose.checkCurrencyForTable(combo_billsi_currency.getSelectedItem().toString(), dtm4, 6);
            allpurpose.checkCurrencyForTable(combo_billsi_currency.getSelectedItem().toString(), dtm4, 7);
            Component[] cmp = {lbl_billsi_total};
            allpurpose.checkCurrency(combo_billsi_currency.getSelectedItem().toString(), cmp);
        }
    }//GEN-LAST:event_combo_billsi_currencyItemStateChanged

    private void lbl_control_traderMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_traderMouseEntered
        lbl_control_trader.setToolTipText("مشتريات");
    }//GEN-LAST:event_lbl_control_traderMouseEntered

    private void lbl_control_shopMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_shopMouseEntered
        lbl_control_shop.setToolTipText("مبيعات");
    }//GEN-LAST:event_lbl_control_shopMouseEntered

    private void lbl_control_expensesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_expensesMouseEntered
        lbl_control_expenses.setToolTipText("مدفوعات");
    }//GEN-LAST:event_lbl_control_expensesMouseEntered

    private void lbl_control_itemsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_itemsMouseEntered
        lbl_control_items.setToolTipText("عرض البضاعة");
    }//GEN-LAST:event_lbl_control_itemsMouseEntered

    private void lbl_control_reportsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_reportsMouseEntered
        lbl_control_reports.setToolTipText("تقارير");
    }//GEN-LAST:event_lbl_control_reportsMouseEntered

    private void lbl_control_exitMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_exitMouseEntered
        lbl_control_exit.setToolTipText("خروج");
    }//GEN-LAST:event_lbl_control_exitMouseEntered

    private void combo_report_payement_nameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_report_payement_nameItemStateChanged
        String table = "";
        if (evt.getStateChange() == 1) {
            Vector v = allpurpose.CheckSelectedLabelInPanel(pan_repot_payment_control);
            System.out.println(v.get(0));
            if (!v.get(0).equals(-1)) {
                switch (v.get(0).toString()) {

                    case "0":
                        table = "bill_trader";
                        break;
                    case "1":
                        table = "bill_local";
                        break;
                    case "2":
                        table = "expenses";
                        break;
                    default:
                        break;

                }
                String[] dateCondition = allpurpose.CreateDateCondition(datepicker_report_from, datepicker_report_to);
                Component[] cmp = {lbl_report_total, lbl_report_payed, lbl_report_remaining};
                utility.resetTable(dtm6);
                report.calculateTableReport(dtm6, table_report_bills, table, dateCondition,
                        combo_report_payement_name.getSelectedItem().toString(), cmp);

            }

        }
    }//GEN-LAST:event_combo_report_payement_nameItemStateChanged

    private void lbl_item_searchMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_item_searchMouseClicked
        if (!pan_item_control_search.isVisible()) {
            pan_item_control_search.setVisible(true);
            dbfunc.refreshCB(combo_item_search, " bill_local_items ", "الصنف");
            dbfunc.refreshCB(combo_item_search1, " bill_local_items ", "الاسم");
        }

    }//GEN-LAST:event_lbl_item_searchMouseClicked

    private void lbl_item_searchMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_item_searchMouseEntered
        lbl_item_search.setForeground(Color.red);
    }//GEN-LAST:event_lbl_item_searchMouseEntered

    private void lbl_item_searchMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_item_searchMouseExited
        lbl_item_search.setForeground(Color.white);
    }//GEN-LAST:event_lbl_item_searchMouseExited

    private void lbl_control_backupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_backupMouseClicked
        String value = promptForDiscardValues(pan_control_items);
        if (!value.isEmpty()) {
            int choice = JOptionPane.showConfirmDialog(null, "هل تريد إغلاق الفاتورة الحالية؟", "اغلاق",
                    JOptionPane.YES_NO_OPTION);
            if (choice == 0) {
                LabelSelectionByBorder(pan_control_items, lbl_control_backup, lbl_control_backup.getName());
                cl.show(CardPanel, "backup");
                cl7.show(pan_backup, "backup_main");
            }
        } else {
            LabelSelectionByBorder(pan_control_items, lbl_control_backup, lbl_control_backup.getName());
            cl.show(CardPanel, "backup");
            cl7.show(pan_backup, "backup_main");
        }
    }//GEN-LAST:event_lbl_control_backupMouseClicked

    private void lbl_control_backupMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_control_backupMouseEntered
        lbl_control_backup.setToolTipText("النسخة الاحتياطية");
    }//GEN-LAST:event_lbl_control_backupMouseEntered

    private void lbl_backup_back_allMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_backup_back_allMouseClicked
        cl7.show(pan_backup, "backup_card");
        cl8.show(pan_backup_card, "backup_pan_all");
    }//GEN-LAST:event_lbl_backup_back_allMouseClicked

    private void lbl_backup_restore_allMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_backup_restore_allMouseClicked
        cl7.show(pan_backup, "backup_card");
        cl8.show(pan_backup_card, "restore_pan_all");
    }//GEN-LAST:event_lbl_backup_restore_allMouseClicked

    private void txt_bckpathMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_bckpathMouseClicked
        utility.createchooser(txt_bckpath);
    }//GEN-LAST:event_txt_bckpathMouseClicked

    private void btn_backupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_backupActionPerformed
        bck.doBackup(txt_bckpath, "backup");
    }//GEN-LAST:event_btn_backupActionPerformed

    private void btn_restoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_restoreActionPerformed
        bck.dorestore(txt_restorepath, "restore");
    }//GEN-LAST:event_btn_restoreActionPerformed

    private void txt_restorepathMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_restorepathMouseClicked
        utility.createFileChooser(txt_restorepath);
    }//GEN-LAST:event_txt_restorepathMouseClicked

    private void combo_billsi_add_search1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combo_billsi_add_search1ItemStateChanged

        if (evt.getStateChange() == 1) {
            String where = " `رقم الفاتورة` like " + lbl_billsi_idbills.getText();
            String column = " `الكود`,`الصنف`,`الاسم`,`الكمية`,`السعر الفردي`,`حسم`,`سعر المبيع`,`المجموع`";
            String[] comp
                    = {
                        txt_billsi_add_search1.getText(), txt_billsi_add_search2.getText(),
                        combo_billsi_add_search1.getSelectedItem().toString()
                    };
            utility.resetTable(dtm4);
            String where1 = returnWhereBillsItem(comp, "");
            where = where1.isEmpty() ? where : where + "' and " + where1;
            dbfunc.selectData(dtm4, table_bills_item_add, column, "bill_local_items", where, "", "", "", "", "");

            utility.setupTable(dtm4, table_bills_item_add);
        }
    }//GEN-LAST:event_combo_billsi_add_search1ItemStateChanged

    public void initial() throws ClassNotFoundException, InstantiationException {
        showLogin();
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (IllegalAccessException ex) {
            try {
                PrintStream ps = new PrintStream(new FileOutputStream(new File("")));
                ex.printStackTrace(ps);
            } catch (Exception e) {

            }
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel CardPanel;
    private javax.swing.JTextArea area_addbilli_add;
    private javax.swing.JTextArea area_addbills_add;
    private javax.swing.JTextArea area_addexp_add;
    private javax.swing.JTextArea area_billt_add;
    private javax.swing.JButton btn_addbills_add;
    private javax.swing.JButton btn_addbillt_add;
    private javax.swing.JButton btn_addbillti_add;
    private javax.swing.JButton btn_addexp_add;
    private javax.swing.JButton btn_backup;
    private javax.swing.JButton btn_billsi_confirm;
    private javax.swing.JButton btn_billssearch_search;
    private javax.swing.JButton btn_billti_search;
    private javax.swing.JButton btn_billtsearch_add;
    private javax.swing.JButton btn_change;
    private javax.swing.JButton btn_report_month;
    private javax.swing.JButton btn_report_year;
    private javax.swing.JButton btn_reportitems_month;
    private javax.swing.JButton btn_reportitems_year;
    private javax.swing.JButton btn_restore;
    private javax.swing.JButton btn_searchexp_search;
    private javax.swing.JCheckBox check_billssearch1;
    private javax.swing.JCheckBox check_billssearch2;
    private javax.swing.JCheckBox check_billssearch3;
    private javax.swing.JCheckBox check_billssearch4;
    private javax.swing.JCheckBox check_billtisearch1;
    private javax.swing.JCheckBox check_billtisearch2;
    private javax.swing.JCheckBox check_billtisearch3;
    private javax.swing.JCheckBox check_billtsearch1;
    private javax.swing.JCheckBox check_billtsearch2;
    private javax.swing.JCheckBox check_billtsearch3;
    private javax.swing.JCheckBox check_billtsearch4;
    private javax.swing.JCheckBox check_searchexp_search1;
    private javax.swing.JCheckBox check_searchexp_search2;
    private javax.swing.JCheckBox check_searchexp_search3;
    private javax.swing.JCheckBox check_searchexp_search4;
    private javax.swing.JComboBox combo_addbillti_type;
    private javax.swing.JComboBox combo_addbillti_unit;
    private javax.swing.JComboBox combo_bills_currency;
    private javax.swing.JComboBox combo_billsi_add_search1;
    private javax.swing.JComboBox combo_billsi_currency;
    private javax.swing.JComboBox combo_billsi_shop_search1;
    private javax.swing.JComboBox combo_billt_currency;
    private javax.swing.JComboBox combo_billti_currency;
    private javax.swing.JComboBox combo_billtisearch1;
    private javax.swing.JComboBox combo_expenses_currency;
    private javax.swing.JComboBox<String> combo_item_search;
    private javax.swing.JComboBox<String> combo_item_search1;
    private javax.swing.JComboBox<String> combo_report_payement_name;
    private javax.swing.JComboBox combo_report_year;
    private javax.swing.JComboBox combo_reportitems_search;
    private javax.swing.JComboBox combo_reportitems_search1;
    private org.jdesktop.swingx.JXDatePicker date_addbills_date;
    private org.jdesktop.swingx.JXDatePicker date_addbillt_date;
    private org.jdesktop.swingx.JXDatePicker date_addexp_date;
    private org.jdesktop.swingx.JXDatePicker datepicker_billssearch_from;
    private org.jdesktop.swingx.JXDatePicker datepicker_billssearch_to;
    private org.jdesktop.swingx.JXDatePicker datepicker_billtsearch_from;
    private org.jdesktop.swingx.JXDatePicker datepicker_billtsearch_to;
    private org.jdesktop.swingx.JXDatePicker datepicker_report_from;
    private org.jdesktop.swingx.JXDatePicker datepicker_report_to;
    private org.jdesktop.swingx.JXDatePicker datepicker_reportitems_from;
    private org.jdesktop.swingx.JXDatePicker datepicker_reportitems_to;
    private org.jdesktop.swingx.JXDatePicker datepicker_searchexp_from;
    private org.jdesktop.swingx.JXDatePicker datepicker_searchexp_to;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel100;
    private javax.swing.JLabel jLabel101;
    private javax.swing.JLabel jLabel102;
    private javax.swing.JLabel jLabel103;
    private javax.swing.JLabel jLabel104;
    private javax.swing.JLabel jLabel105;
    private javax.swing.JLabel jLabel106;
    private javax.swing.JLabel jLabel107;
    private javax.swing.JLabel jLabel108;
    private javax.swing.JLabel jLabel109;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel110;
    private javax.swing.JLabel jLabel111;
    private javax.swing.JLabel jLabel112;
    private javax.swing.JLabel jLabel113;
    private javax.swing.JLabel jLabel114;
    private javax.swing.JLabel jLabel115;
    private javax.swing.JLabel jLabel116;
    private javax.swing.JLabel jLabel117;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel92;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JLabel jLabel95;
    private javax.swing.JLabel jLabel96;
    private javax.swing.JLabel jLabel97;
    private javax.swing.JLabel jLabel98;
    private javax.swing.JLabel jLabel99;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JLabel lb_add_bill_shop;
    private javax.swing.JLabel lb_add_bill_trader;
    private javax.swing.JLabel lb_add_expenses;
    private javax.swing.JLabel lb_report;
    private javax.swing.JLabel lbl_addbillti_type;
    private javax.swing.JLabel lbl_addbillti_unit;
    private javax.swing.JLabel lbl_backup_back_all;
    private javax.swing.JLabel lbl_backup_restore_all;
    private javax.swing.JLabel lbl_bills_add;
    private javax.swing.JLabel lbl_bills_delete;
    private javax.swing.JLabel lbl_bills_edit;
    private javax.swing.JLabel lbl_bills_searchs;
    private javax.swing.JLabel lbl_billsi_add;
    private javax.swing.JLabel lbl_billsi_delete;
    private javax.swing.JLabel lbl_billsi_idbills;
    private javax.swing.JLabel lbl_billsi_searchadd;
    private javax.swing.JLabel lbl_billsi_searchitem;
    private javax.swing.JLabel lbl_billsi_total;
    private javax.swing.JLabel lbl_billt_add;
    private javax.swing.JLabel lbl_billt_delete;
    private javax.swing.JLabel lbl_billt_edit;
    private javax.swing.JLabel lbl_billt_search;
    private javax.swing.JLabel lbl_billti_add;
    private javax.swing.JLabel lbl_billti_confirm;
    private javax.swing.JLabel lbl_billti_delete;
    private javax.swing.JLabel lbl_billti_edit;
    private javax.swing.JLabel lbl_billti_invoicenb;
    private javax.swing.JLabel lbl_billti_search;
    private javax.swing.JLabel lbl_control_backup;
    private javax.swing.JLabel lbl_control_exit;
    private javax.swing.JLabel lbl_control_expenses;
    private javax.swing.JLabel lbl_control_home;
    private javax.swing.JLabel lbl_control_items;
    private javax.swing.JLabel lbl_control_reports;
    private javax.swing.JLabel lbl_control_shop;
    private javax.swing.JLabel lbl_control_trader;
    private javax.swing.JLabel lbl_expenses_add;
    private javax.swing.JLabel lbl_expenses_delete;
    private javax.swing.JLabel lbl_expenses_edit;
    private javax.swing.JLabel lbl_expenses_search;
    private javax.swing.JLabel lbl_item_search;
    private javax.swing.JLabel lbl_passtate;
    private javax.swing.JLabel lbl_report_expenses;
    private javax.swing.JLabel lbl_report_items_shop;
    private javax.swing.JLabel lbl_report_items_trader;
    private javax.swing.JLabel lbl_report_payed;
    private javax.swing.JLabel lbl_report_remaining;
    private javax.swing.JLabel lbl_report_shop;
    private javax.swing.JLabel lbl_report_total;
    private javax.swing.JLabel lbl_report_total_payed;
    private javax.swing.JLabel lbl_report_total_paysell;
    private javax.swing.JLabel lbl_report_total_remaining;
    private javax.swing.JLabel lbl_report_total_remaining_get;
    private javax.swing.JLabel lbl_report_total_remaining_pay;
    private javax.swing.JLabel lbl_report_total_sell;
    private javax.swing.JLabel lbl_report_trader;
    private javax.swing.JPanel pan_Background;
    private javax.swing.JPanel pan_Bill_shop_card;
    private javax.swing.JPanel pan_Bill_trader;
    private javax.swing.JPanel pan_Bill_trader_card;
    private javax.swing.JPanel pan_Billt_item_card;
    private javax.swing.JPanel pan_backup;
    private javax.swing.JPanel pan_backup_card;
    private javax.swing.JPanel pan_backup_card_backup_all;
    private javax.swing.JPanel pan_backup_card_restore_all;
    private javax.swing.JPanel pan_backup_main;
    private javax.swing.JPanel pan_bill_shop;
    private javax.swing.JPanel pan_bill_shop_add;
    private javax.swing.JPanel pan_bill_shop_search;
    private javax.swing.JPanel pan_bill_trader_add;
    private javax.swing.JPanel pan_bill_trader_search;
    private javax.swing.JPanel pan_bills_control;
    private javax.swing.JPanel pan_bills_items;
    private javax.swing.JPanel pan_billsi_search_add;
    private javax.swing.JPanel pan_billsi_search_shop;
    private javax.swing.JPanel pan_billt_control;
    private javax.swing.JPanel pan_billt_items;
    private javax.swing.JPanel pan_billti_add;
    private javax.swing.JPanel pan_billti_control;
    private javax.swing.JPanel pan_billti_search;
    private javax.swing.JPanel pan_control_expenses_add;
    private javax.swing.JPanel pan_control_expenses_card;
    private javax.swing.JPanel pan_control_expenses_search;
    private javax.swing.JPanel pan_control_items;
    private javax.swing.JPanel pan_expenses;
    private javax.swing.JPanel pan_expenses_control;
    private javax.swing.JPanel pan_item_control_search;
    private javax.swing.JPanel pan_items;
    private javax.swing.JPanel pan_login;
    private javax.swing.JPanel pan_login_card;
    private javax.swing.JPanel pan_login_change;
    private javax.swing.JPanel pan_login_main;
    private javax.swing.JPanel pan_main;
    private javax.swing.JPanel pan_report;
    private javax.swing.JPanel pan_report_item;
    private javax.swing.JPanel pan_report_items_control;
    private javax.swing.JPanel pan_report_main;
    private javax.swing.JPanel pan_report_payment;
    private javax.swing.JPanel pan_report_payment_details;
    private javax.swing.JPanel pan_repot_payment_control;
    private javax.swing.JTable table_bill_shop;
    private javax.swing.JTable table_bill_trader;
    private javax.swing.JTable table_bills_item_add;
    private javax.swing.JTable table_bills_item_shop;
    private javax.swing.JTable table_billt_item;
    private javax.swing.JTable table_expenses;
    private javax.swing.JTable table_items;
    private javax.swing.JTable table_items_show;
    private javax.swing.JTable table_report_bills;
    private javax.swing.JTextField txt_addbills_invoicenb;
    private javax.swing.JTextField txt_addbills_name;
    private javax.swing.JFormattedTextField txt_addbills_payed;
    private javax.swing.JFormattedTextField txt_addbills_phone;
    private javax.swing.JFormattedTextField txt_addbills_remain;
    private javax.swing.JFormattedTextField txt_addbills_total_real;
    private javax.swing.JTextField txt_addbillt_invoicenb;
    private javax.swing.JTextField txt_addbillt_name;
    private javax.swing.JFormattedTextField txt_addbillt_payed;
    private javax.swing.JFormattedTextField txt_addbillt_phone;
    private javax.swing.JFormattedTextField txt_addbillt_remain;
    private javax.swing.JFormattedTextField txt_addbillt_total;
    private javax.swing.JFormattedTextField txt_addbillti_discount;
    private javax.swing.JTextField txt_addbillti_name;
    private javax.swing.JFormattedTextField txt_addbillti_qty;
    private javax.swing.JFormattedTextField txt_addbillti_real_total;
    private javax.swing.JTextField txt_addbillti_serial;
    private javax.swing.JFormattedTextField txt_addbillti_total;
    private javax.swing.JTextField txt_addbillti_type;
    private javax.swing.JTextField txt_addbillti_unit;
    private javax.swing.JFormattedTextField txt_addbillti_unitprice;
    private javax.swing.JTextField txt_addexp_invoicenb;
    private javax.swing.JTextField txt_addexp_name;
    private javax.swing.JFormattedTextField txt_addexp_payed;
    private javax.swing.JFormattedTextField txt_addexp_phone;
    private javax.swing.JFormattedTextField txt_addexp_remain;
    private javax.swing.JFormattedTextField txt_addexp_total;
    private javax.swing.JTextField txt_addexp_type;
    private javax.swing.JTextField txt_bckpath;
    private javax.swing.JTextField txt_billsi_add_search1;
    private javax.swing.JTextField txt_billsi_add_search2;
    private javax.swing.JTextField txt_billsi_shop_search1;
    private javax.swing.JTextField txt_billsi_shop_search2;
    private javax.swing.JTextField txt_billssearch1;
    private javax.swing.JTextField txt_billssearch2;
    private javax.swing.JTextField txt_billssearch3;
    private javax.swing.JTextField txt_billtisearch1;
    private javax.swing.JTextField txt_billtisearch2;
    private javax.swing.JTextField txt_billtsearch1;
    private javax.swing.JTextField txt_billtsearch2;
    private javax.swing.JTextField txt_billtsearch3;
    private javax.swing.JPasswordField txt_change_pass1;
    private javax.swing.JPasswordField txt_change_pass2;
    private javax.swing.JPasswordField txt_change_pass3;
    private javax.swing.JTextField txt_item_search;
    private javax.swing.JPasswordField txt_login_pass;
    private javax.swing.JTextField txt_login_user;
    private javax.swing.JTextField txt_restorepath;
    private javax.swing.JTextField txt_searchexp_search1;
    private javax.swing.JTextField txt_searchexp_search2;
    private javax.swing.JTextField txt_searchexp_search3;
    // End of variables declaration//GEN-END:variables

//    public void alt_shift() throws AWTException {
//        Robot robot = new Robot();
//        robot.keyPress(KeyEvent.VK_ALT);
//        robot.keyPress(KeyEvent.VK_SHIFT);
//    }
    private String Datesubstring(String date) {
        String newString = date.replace(" 00:00:00 EET ", " ");
        System.out.println(newString);
        return newString;
    }

    private void showHidePanel(JPanel pan) {

        if (!pan.isVisible()) {
            pan.setVisible(true);
        }

    }

    private String returnWhereBillsItem(String[] comp, String tablename) {
        String where = "";

        if (!comp[0].isEmpty()) {
            where = " " + tablename + "`الكود` like '%" + comp[0] + "%' ";
        }
        if (!comp[1].isEmpty()) {
            where = where.isEmpty() ? " " + tablename + "`الاسم` like '%" + comp[1] + "%' "
                    : where + " and " + tablename + "`الاسم` like '%" + comp[1] + "%' ";
        }
        if (!comp[2].isEmpty()) {
            where = where.isEmpty() ? " " + tablename + "`الصنف` like '" + comp[2] + "'"
                    : where + " and " + tablename + "`الصنف` like '" + comp[2] + "'";
        }

        return where;
    }

    private String returnWhereReportItem(Component[] comp, String tablename) {

        String where = "";

        JTextField f = (JTextField) comp[0];
        JComboBox c = (JComboBox) comp[1];

        where = where.isEmpty() ? "" : " " + tablename + "`الاسم` like '%" + f.getText() + "%' ";
        System.out.println(c.getSelectedIndex());
        if (c.getSelectedIndex() != -1 || c.getSelectedIndex() == 0) {
            if (c.getSelectedIndex() != 0) {
                where = where.isEmpty() ? " " + tablename + "`الصنف` like '" + c.getSelectedItem().toString() + "'"
                        : where + " and " + tablename + "`الصنف` like '" + c.getSelectedItem().toString() + "'";
            }
        }
        return where;
    }

    // used to compare one or more values of 2 tables,we send to table we want to compare to
    // and the values we want to be compared
    private int Compare2Tables(DefaultTableModel dtm, Object[] cellValue) {
        int row = -1;
        int counter = 0;
        if (dtm.getRowCount() == 0) {
        } else {
            for (int i = 0; i < dtm.getRowCount(); i++) {
                counter = 0;
                for (int j = 0; j < cellValue.length; j++) {
                    if (dtm.getValueAt(i, j).equals(cellValue[j])) {
                        counter++;
                    }

                }
                if (counter == cellValue.length) {
                    row = i;
                    break;
                }
            }
        }

        return row;
    }

    // used to calculate the qty in case the item to be added or deleted is in the table
    // so we add and substract the qty by one, dtm is the table we are adding from,so qty-1
    //dtm1 is the table we are adding to,so qty+1,cell contain the row and column index
    // in the order : cell_from_dtm,cell_from_dtm1.
    private void CalculateQty(DefaultTableModel dtm, DefaultTableModel dtm1, int[][] cells) {
        int qty;
        // add the value of qty of dtm by one.
        qty = Integer.parseInt(String.valueOf(dtm.getValueAt(cells[0][0], cells[0][1])));
        qty = qty - 1;
        dtm.setValueAt(qty, cells[0][0], cells[0][1]);

        // substract the value of qty of dtm1 by one.
        if (dtm1 != null) {
            qty = Integer.parseInt(String.valueOf(dtm1.getValueAt(cells[1][0], cells[1][1])));
            qty = qty + 1;
            dtm1.setValueAt(qty, cells[1][0], cells[1][1]);
        }

    }

    private void CalculateQtyDB(Object qty, Object code, Object type) throws SQLException {
        try {
            String where = "  `الكود` like '" + code + "' and `الصنف` like '" + type + "'";
            Vector v = dbfunc.searchBycolumn("items_qty", "`الكود`,`الصنف`", where);
            if (v.isEmpty()) {
                Object[] vals = {code, type, qty, qty, 0};
                dbfunc.insertData("items_qty", vals, "");
            } else {
                String query = "update items_qty set `الكمية` = `الكمية` + (" + qty + "),"
                        + "`كمية متبقية`=`كمية متبقية`+(" + qty + ")"
                        + " where  `الكود` like '" + code + "' and `الصنف` like '" + type + "'";
                Statement stmt = (Statement) conn.createStatement();
                stmt.executeUpdate(query);
            }

        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private float CalculateTotalSI(DefaultTableModel dtm, int[] cellscolumn) {
        float total = 0;
        for (int i = 0; i < dtm.getRowCount(); i++) {
            total = total + (Float.valueOf(dtm.getValueAt(i, cellscolumn[0]).toString())
                    * Float.valueOf(dtm.getValueAt(i, cellscolumn[1]).toString()));
        }
        return total;
    }

    private void CalculateTotalSIRow(DefaultTableModel dtm, int row) {
        float total = 0;
        total = total + (Float.valueOf(dtm.getValueAt(row, 3).toString())
                * Float.valueOf(dtm.getValueAt(row, 6).toString()));
        dtm.setValueAt(total, row, 7);
    }

    private void CheckZero(DefaultTableModel dtm, int[] cell) {
        if (dtm.getValueAt(cell[0], cell[1]).toString().equals("0")) {
            dtm.removeRow(cell[0]);
        }
    }

    private void CheckZeroItems(DefaultTableModel dtm, int[] cell, String id) {
        if (dtm.getValueAt(cell[0], cell[1]).toString().equals("0")) {

            Object[] fields = {"`الكود`", "`الصنف`", "`رقم الفاتورة`"};
            Object[] vals = {dtm4.getValueAt(cell[0], 0), dtm4.getValueAt(cell[0], 1), id};
            try {
                dbfunc.deleteData("bill_local_items", fields, vals);
            } catch (Exception e) {
                System.out.println(e);
            }
            dtm.removeRow(cell[0]);
        }
    }

    // function ro select items to pans, fill shop items and
    private void showBillt() {
        utility.resetTable(dtm);
        String column = " `رقم الفاتورة`,`الاسم`,`الهاتف`,`التاريخ`,`المجموع`,`المدفوع`,`المتبقي`,`تفاصيل` ";
        String table = "bill_trader";
        String orderBy = " `تاريخ` desc";
        LabelSelectionByColor(pan_billt_control, lbl_billt_add, lbl_billt_add.getName());

        dbfunc.selectData(dtm, table_bill_trader, column, table, "", "", "", "", "", orderBy);

        System.out.println(" in biit column:" + dtm.getColumnCount());
        pan_Bill_trader_card.setVisible(true);
        cmp.ClearCmp(pan_bill_trader_add);
        area_billt_add.setText("");
        btn_addbillt_add.setText("اضافة");
        txt_addbillt_invoicenb.requestFocusInWindow();
        cl.show(CardPanel, "bill_trader");
        cl1.show(pan_Bill_trader_card, "billt_add");
        System.out.println(" in biit column1:" + dtm.getColumnCount());
        utility.setupTable(dtm, table_bill_trader);
        table_bill_trader.setDefaultEditor(Object.class, null);

    }

    private void showLogin() {
        cl.show(CardPanel, "login");
        cl6.show(pan_login_card, "login_main");
        cmp.hideCmp(pan_control_items, false);
        jLabel111.setVisible(false);
        lbl_control_exit.setVisible(true);
        //txt_login_user.requestFocusInWindow();
    }

    private void LoginSuccess() {
        boolean success = false;
        try {
            success = login.CheckLogin(txt_login_user.getText(), txt_login_pass.getPassword());
            if (success) {
                cl.show(CardPanel, "main");
                cmp.hideCmp(pan_control_items, success);
                LabelSelectionByBorder(pan_control_items, lbl_control_home, lbl_control_home.getName());
            } else {
                JOptionPane.showMessageDialog(this, "كلمة المرور خطأ, الرجاء معاوة المحاولة");
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void showBills() {
        //MouseEvent evt = null;
        String column = "`رقم الفاتورة`,`الاسم`,`الهاتف`,`التاريخ`,`المجموع`,`المدفوع`,"
                + "`المتبقي`,`تفاصيل` ";
        String orderBy = " `تاريخ` desc";

        String table = "bill_local";
        utility.resetTable(dtm1);
        dbfunc.selectData(dtm1, table_bill_shop, column, table, "", "", "", "", "", orderBy);
        pan_Bill_shop_card.setVisible(true);
        cl.show(CardPanel, "bill_shop");
        utility.setupTable(dtm1, table_bill_shop);
        table_bill_shop.setDefaultEditor(Object.class, null);

    }

    private void newBillS() {
        showHidePanel(pan_Bill_shop_card);

        LabelSelectionByColor(pan_bills_control, lbl_bills_add, lbl_bills_add.getName());

        cmp.ClearCmp(pan_bill_shop_add);
        area_addbills_add.setText("");
        btn_addbills_add.setText("اضافة");
        txt_addbills_total_real.setText("0");
        txt_addbills_payed.setText("0");
        txt_addbills_remain.setText("0");
        cl2.show(pan_Bill_shop_card, "bills_add");
        try {
            IncrementID("bill_local", "`رقم الفاتورة`", txt_addbills_invoicenb);
        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        txt_addbills_name.requestFocusInWindow();
    }

    private void showPanBillsi() {
        String columns = " bit.`الكود`,bit.`الصنف`,bit.`الاسم`,qty.`كمية متبقية`,bit.`سعر المبيع` ";
        String on = " on bit.`الكود`= qty.`الكود` and bit.`الصنف`  = qty.`الصنف`";
        String columns1 = " `الكود`,`الصنف`,`الاسم`,`الكمية`,`السعر الفردي`,`حسم`,`سعر المبيع`,`المجموع`";
        pan_billsi_search_shop.setVisible(false);
        pan_billsi_search_add.setVisible(false);

        utility.resetTable(dtm3);
        utility.resetTable(dtm4);

        lbl_billsi_total.setText("0");
        combo_billsi_currency.setSelectedItem("$");

        dbfunc.selectData(dtm3, table_bills_item_shop, columns, "bill_trader_items as bit", " qty.`كمية متبقية` > 0 ",
                "left join items_qty as qty", on, "", "", "");

        dbfunc.selectData(dtm4, table_bills_item_add, columns1, "bill_local_items",
                "`رقم الفاتورة`= " + lbl_billsi_idbills.getText(),
                "", "", "", "", "");

        cl.show(CardPanel, "bills_items");
        utility.setupTable(dtm3, table_bills_item_shop);
        utility.setupTable(dtm4, table_bills_item_add);
        table_bills_item_shop.setDefaultEditor(Object.class, null);

    }

    private void ShowBilltitems() {
        utility.resetTable(dtm2);
        dbfunc.refreshCB(combo_addbillti_type, "type", "type");
        dbfunc.refreshCB(combo_addbillti_unit, "unit", "unit");

        txt_addbillti_serial.setEnabled(true);
        combo_addbillti_type.setEnabled(true);
        txt_addbillti_type.setVisible(false);
        txt_addbillti_unit.setVisible(false);
        btn_addbillti_add.setText("اضافة");
        txt_addbillti_serial.requestFocus();
        cmp.ClearCmp(pan_billti_add);
        area_billt_add.setText("");

        LabelSelectionByColor(pan_billti_control, lbl_billti_add, lbl_billti_add.getName());

        String column = "الكود,الصنف,الاسم,الوحدة,"
                + "الكمية,`السعر الفردي`,المجموع,`النسبة`,"
                + "`سعر المبيع` ,تفاصيل";
        String where = " `رقم الفاتورة` like '" + lbl_billti_invoicenb.getText() + "' ";
        dbfunc.selectData(dtm2, table_billt_item, column, "bill_trader_items", where, "", "", "", "", "");
        utility.setupTable(dtm2, table_billt_item);
        table_billt_item.setDefaultEditor(Object.class, null);
    }

    private void ShowExpensesBill() {
        utility.resetTable(dtm5);

        String column = "`رقم الفاتورة`,`العنوان`,`الاسم`,`الهاتف`,`التاريخ`,`المجموع`,"
                + "`المدفوع`,`المتبقي`,`تفاصيل`";
        String orderBy = " `تاريخ` desc";

        cmp.ClearCmp(pan_control_expenses_add);

        LabelSelectionByColor(pan_expenses_control, lbl_expenses_add, lbl_expenses_add.getName());

        txt_addexp_name.requestFocusInWindow();
        btn_addexp_add.setText("اضافة");
        combo_expenses_currency.setSelectedItem("$");

        dbfunc.selectData(dtm5, table_expenses, column, "expenses", "", "", "", "", "", orderBy);
        try {
            IncrementID("expenses", "`رقم الفاتورة`", txt_addexp_invoicenb);
        } catch (SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        cl.show(CardPanel, "expenses");
        cl4.show(pan_control_expenses_card, "expenses_add");
        utility.setupTable(dtm5, table_expenses);
        table_expenses.setDefaultEditor(Object.class, null);
    }

    private void ShowItemPan() throws SQLException {
        pan_item_control_search.setVisible(false);
        utility.resetTable(dtm7);
        items.selectItems(dtm7, table_items_show, "");
        utility.setupTable(dtm7, table_items_show);
        table_items_show.setDefaultEditor(Object.class, null);
        cl.show(CardPanel, "items");
    }

    private void IncrementID(String table, String column, JTextField text) throws SQLException {

        Vector v = dbfunc.searchBycolumn(table, " Max(" + column + ")", "");
        int id = v.isEmpty() ? 1 : (int) v.get(0) + 1;
        text.setText(String.valueOf(id));
    }

    private void CheckTextArea(JTextArea area) {
        if (area.getText().isEmpty()) {
            area.setText(" ");
        }
    }

    private void LabelSelectionByColor(JPanel pan, JLabel label, String labelnum) {
        Vector v1 = new Vector();
        v1.add(labelnum);
        v1.add(label);
        allpurpose.LabelSelection(v1, pan);
    }

    private void LabelSelectionByBorder(JPanel pan, JLabel label, String labelnum) {
        Vector v1 = new Vector();
        v1.add(labelnum);
        v1.add(label);
        allpurpose.LabelBorderSelection(v1, pan);
    }

    private String promptForDiscardValues(JPanel pan) {
        String valueToCechk = "";
        Vector v = allpurpose.CheckSelectedLabelInPanel(pan);
        if (!v.get(0).equals(-1)) {
            switch (v.get(0).toString()) {
                case "1":
                    valueToCechk = txt_addbillt_invoicenb.getText();
                    break;
                case "2":
                    valueToCechk = txt_addbills_invoicenb.getText();
                    break;
                case "3":
                    valueToCechk = txt_addexp_invoicenb.getText();
                    break;
                default:
                    break;
            }
        }
        return valueToCechk;
    }

}
