/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sabbah;

import Functions.dbFunc;
import Functions.utilityclass;
import com.mysql.jdbc.Connection;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author OnMac
 */
public class Bill_si {

    private Connection conn = null;
    private final dbFunc dbfunc;
    private final utilityclass utility = new utilityclass();
    private final String dbfilename = "dbcreds.config";

    public Bill_si() {
        conn = utility.connect2DB(dbfilename);
        dbfunc = new dbFunc(conn);
    }

    public void discountPrice(DefaultTableModel dtm, JTable table, int columnnum, int num) {
        if (!table.isEditing()) {
            int column = table.getSelectedColumn();

            if (column == columnnum) {
                int row = table.getSelectedRow();
                float value1 = Float.valueOf(String.valueOf(dtm.getValueAt(row, column)));
                float value2 = Float.valueOf(String.valueOf(dtm.getValueAt(row, column - 1)));
                float value3 = value2 - ((value1 / 100) * value2);
                float value4 = value3 * Float.valueOf(String.valueOf(dtm.getValueAt(row, column - 2)));
                dtm.setValueAt(String.format("%.2f", value3), row, column + 1);
                dtm.setValueAt(String.format("%.2f", value4), row, column + 2);
                updateAfterDiscount(dtm, row, num);
            }

        }
    }

    private void updateAfterDiscount(DefaultTableModel dtm, int row, int num) {
        Object[] fields = {"`حسم`", "`سعر المبيع`", "`المجموع`"};
        Object[] vals = {dtm.getValueAt(row, 5), dtm.getValueAt(row, 6), dtm.getValueAt(row, 7)};
        String where = "where `الكود` like '" + dtm.getValueAt(row, 0) + "' and"
                + " `الصنف` like '" + dtm.getValueAt(row, 1) + "' and"
                + " `رقم الفاتورة` = " + num + "";
        dbfunc.updateData("bill_local_items", fields, vals, where);
    }
}
