package Sabbah;

import Functions.dbFunc;
import Functions.utilityclass;
import com.mysql.jdbc.Connection;
import java.awt.Component;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.jdesktop.swingx.JXDatePicker;

public class Reports {

    private Connection conn = null;
    private final dbFunc dbfunc;
    private final AllpurposeFunctions allpurpose = new AllpurposeFunctions();
    private final utilityclass utility = new utilityclass();
    private final String dbfilename = "dbcreds.config";

    public Reports() {
        conn = utility.connect2DB(dbfilename);
        dbfunc = new dbFunc(conn);
    }

    //used for payement report
    public void CalculateMonthReport(String[] datecondition, Component[] cmp) throws SQLException {
        Vector all = new Vector();
        float sumpayed = 0, sumremain = 0, sumallpayed = 0, sumallremain = 0;// sumall = 0;
        String where = "  `تاريخ` >= " + datecondition[0] + " and `تاريخ` <= " + datecondition[1];
        Vector v = dbfunc.searchBycolumn("bill_trader", "sum(`المدفوع`),sum(`المتبقي`)", where);
        Vector v1 = dbfunc.searchBycolumn("bill_local", "sum(`المدفوع`),sum(`المتبقي`)", where);
        Vector v2 = dbfunc.searchBycolumn("expenses", "sum(`المدفوع`),sum(`المتبقي`)", where);

        sumpayed = Float.valueOf(v.get(0).toString()) + Float.valueOf(v2.get(0).toString());
        sumremain = Float.valueOf(v.get(1).toString()) + Float.valueOf(v2.get(1).toString());
        sumallpayed = Float.valueOf(v1.get(0).toString()) - sumpayed;
        sumallremain = Float.valueOf(v1.get(1).toString()) - sumremain;
        //sumall = sumallpayed - sumallremain;

        all.add(String.format("%.2f", sumpayed));
        all.add(String.format("%.2f", (float) v1.get(0)));
        all.add(String.format("%.2f", sumallpayed));
        all.add(String.format("%.2f", sumremain));
        all.add(String.format("%.2f", (float) v1.get(1)));
        all.add(String.format("%.2f", sumallremain));

        utility.ValueToField(cmp, all);

    }

    //used for payemnt report
    public void calculateTableReport(DefaultTableModel dtm, JTable tableName, String table,
            String[] dateCondtion, String condition,
            Component[] cmp) {
        String where = "  `تاريخ` >= " + dateCondtion[0] + " and `تاريخ` <= " + dateCondtion[1];
        String where1 = condition.isEmpty() ? where : "  `الاسم` like '" + condition + "' and " + where;
        utility.resetTable(dtm);
        dbfunc.selectData(dtm, tableName, "`الاسم`,`المجموع`,`المدفوع`,`المتبقي`", table, where1, "", "", "", "", "");
        utility.setupTable(dtm, tableName);
        tableName.setDefaultEditor(Object.class, null);
        try {
            Vector v = dbfunc.searchBycolumn(table, "sum(`المجموع`),sum(`المدفوع`),sum(`المتبقي`)", where1);
            v.set(0, String.format("%.2f", v.get(0)));
            v.set(1, String.format("%.2f", v.get(1)));
            v.set(2, String.format("%.2f", v.get(2)));
            utility.ValueToField(cmp, v);
        } catch (SQLException ex) {
            Logger.getLogger(Reports.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //used for items report
    public void monthItemreport(DefaultTableModel dtm, JTable tableName,
            String table1, String table2, String table1alias, String table2alias,
            String from, String to, String condition) {
        String where = "  " + table2alias + "`تاريخ` >= " + from + " and " + table2alias + "`تاريخ` <= " + to;
        where = condition.isEmpty() ? where : where + " and " + condition;
        utility.resetTable(dtm);
        String column = " " + table1alias + "`الصنف`," + table1alias + "`الاسم`," + table1alias + "`الكمية`"
                + "," + table1alias + "`سعر المبيع`," + table1alias + "`المجموع`," + table1alias + "`رقم الفاتورة` ";
        dbfunc.selectData(dtm, tableName, column, table1,
                where, " left join " + table2 + " ", " on " + table1alias + "`رقم الفاتورة`= " + table2alias + "`رقم الفاتورة` ",
                "", "", "");
        utility.setupTable(dtm, tableName);
        tableName.setDefaultEditor(Object.class, null);
    }

    //used for items report
    public void dateButtonClicked(JButton btn, JXDatePicker jdatepicker1, JXDatePicker jdatepicker2) {
        int[] fullDate = allpurpose.getFullDate();
        LocalDate[] getMonth = allpurpose.getYearOrMonth(btn.getText(), fullDate[0], fullDate[1], fullDate[2]);

        jdatepicker1.setDate(java.sql.Date.valueOf(getMonth[0]));
        jdatepicker2.setDate(java.sql.Date.valueOf(getMonth[1]));
    }

    public void ComboYearClicked(Object year, JXDatePicker jdatepicker1, JXDatePicker jdatepicker2) {

        LocalDate[] getMonth = allpurpose.getYearOrMonth("كشف سنوي", (int) year, 1, 1);

        jdatepicker1.setDate(java.sql.Date.valueOf(getMonth[0]));
        jdatepicker2.setDate(java.sql.Date.valueOf(getMonth[1]));
    }

    //used for items report
    public void SelectByConditionForItems(JPanel panel, JTable tableName,
            String[] compValue, String[] dateCondition, DefaultTableModel dtm) {
        String[] obj = null;
        String where = "";

        Vector v = allpurpose.CheckSelectedLabelInPanel(panel);

        if (v.get(0) != "-1") {
            if (v.get(0) == "1") {
                obj = new String[]{"bill_trader_items as bti ", "bill_trader as bt", "bti.", "bt."};
            } else if (v.get(0) == "0") {
                obj = new String[]{"bill_local_items as bli ", "bill_local as bl ", "bli.", "bl."};
            }
            where = returnWhereReportItem(compValue, obj[2]);

            monthItemreport(dtm, tableName, obj[0], obj[1], obj[2], obj[3], dateCondition[0], dateCondition[1], where);
        }
    }

    private String returnWhereReportItem(String[] compValue, String tablename) {

        String where = "";

        if (!compValue[0].isEmpty()) {
            where = " " + tablename + "`الصنف` like '" + compValue[0] + "'";
        }
        if (!compValue[1].isEmpty()) {
            where = where.isEmpty() ? " " + tablename + "`الاسم` like '" + compValue[1] + "'"
                    : where + " and " + tablename + "`الاسم` like '" + compValue[1] + "'";
        }

        return where;
    }
}
