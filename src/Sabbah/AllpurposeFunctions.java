/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sabbah;

import Functions.CmpFunctions;
import Functions.dbFunc;
import Functions.utilityclass;
import com.mysql.jdbc.Connection;
import java.awt.Color;
import java.awt.Component;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;
import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;
import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
import java.util.Date;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import org.jdesktop.swingx.JXDatePicker;

/**
 *
 * @author OnMac
 */
public class AllpurposeFunctions {

    private Connection conn = null;
    private final dbFunc dbfunc;
    private final utilityclass utility = new utilityclass();
    private final CmpFunctions cmpfunc = new CmpFunctions();
    private final String dbfilename = "dbcreds.config";

    private final SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");

    public AllpurposeFunctions() {
        conn = utility.connect2DB(dbfilename);
        dbfunc = new dbFunc(conn);
    }

    public int checkState(float total, float payed) {
        int state = 0;
        float remaining = total - payed;
        if (remaining == 0) {
            state = 1;
        }
        return state;
    }

    public String[] CreateDateCondition(JXDatePicker datepicker1, JXDatePicker datepicker2) {
        String[] condition;
        String datefrom = "";
        String dateto = "";
        if (datepicker1.getDate() == null) {
            datefrom = null;
        } else {
            datefrom = "'" + formater.format(datepicker1.getDate()) + "'";
        }
        if (datepicker2.getDate() == null) {
            dateto = null;
        } else {
            dateto = "'" + formater.format(datepicker2.getDate()) + "'";
        }
        condition = new String[]{datefrom, dateto};
        return condition;
    }

    public void IncrementID(String table, String column, JTextField text) throws SQLException {

        Vector v = dbfunc.searchBycolumn(table, " Max(" + column + ")", "");
        int id = (int) v.get(0) + 1;
        text.setText(String.valueOf(id));
    }

    public boolean showHidePanel(JPanel pan) {
        boolean state = false;
        if (!pan.isVisible()) {
            pan.setVisible(true);
            state = true;
        } else {
            pan.setVisible(false);
            state = false;
        }
        return state;
    }

    public int Compare2Tables(DefaultTableModel dtm, Object[] cellValue) {
        int row = -1;
        int counter = 0;
        if (dtm.getRowCount() == 0) {
        } else {
            for (int i = 0; i < dtm.getRowCount(); i++) {
                counter = 0;
                for (int j = 0; j < cellValue.length; j++) {
                    if (dtm.getValueAt(i, j).equals(cellValue[j])) {
                        counter++;
                    }

                }
                if (counter == cellValue.length) {
                    row = i;
                    break;
                }
            }
        }

        return row;
    }

    public void checkCurrency(String currency, Component[] cmp) {
        if (currency.equals("$")) {
            for (int i = 0; i < cmp.length; i++) {
                if (cmp[i] instanceof JTextField) {
                    JTextField jtf = (JTextField) cmp[i];
                    if (!jtf.getText().isEmpty()) {
                        float valueindollar = Float.valueOf(jtf.getText()) / 1500;
                        jtf.setText(String.format("%.2f", valueindollar));
                    }

                } else if (cmp[i] instanceof JLabel) {
                    JLabel Jl = (JLabel) cmp[i];
                    if (!Jl.getText().isEmpty()) {
                        float valueindollar = Float.valueOf(Jl.getText()) / 1500;
                        Jl.setText(String.format("%.2f", valueindollar));
                    }
                }
            }
        } else if (currency.equals("ليرة لبنانية")) {
            for (int i = 0; i < cmp.length; i++) {
                if (cmp[i] instanceof JTextField) {
                    JTextField jtf = (JTextField) cmp[i];
                    if (!jtf.getText().isEmpty()) {
                        float valueinlbp = Float.valueOf(jtf.getText()) * 1500;
                        jtf.setText(String.format("%.0f", valueinlbp));
                    }
                } else if (cmp[i] instanceof JLabel) {
                    JLabel Jl = (JLabel) cmp[i];
                    if (!Jl.getText().isEmpty()) {
                        float valueinlbp = Float.valueOf(Jl.getText()) * 1500;
                        Jl.setText(String.format("%.0f", valueinlbp));
                    }
                }
            }
        }

    }

    public void checkCurrencyForTable(String currency, DefaultTableModel dtm, int columnnum) {
        if (currency.equals("$")) {
            for (int i = 0; i < dtm.getRowCount(); i++) {
                if (dtm.getValueAt(i, columnnum) != null) {
                    float num = Float.valueOf(String.valueOf(dtm.getValueAt(i, columnnum)));

                    dtm.setValueAt(String.format("%.2f", num / 1500), i, columnnum);
                }
            }

        } else if (currency.equals("ليرة لبنانية")) {
            for (int i = 0; i < dtm.getRowCount(); i++) {
                if (dtm.getValueAt(i, columnnum) != null) {
                    float num = Float.valueOf(String.valueOf(dtm.getValueAt(i, columnnum)));

                    dtm.setValueAt(String.format("%.0f", num * 1500), i, columnnum);
                }
            }
        }
    }

    public LocalDate[] getYearOrMonth(String dateType, int year, int month, int day) {
        LocalDate start = null;
        LocalDate end = null;

        LocalDate initial = LocalDate.of(year, month, day);
        if (dateType.equals("كشف شهري")) {
            start = initial.with(firstDayOfMonth());

            end = initial.with(lastDayOfMonth());
        } else if (dateType.equals("كشف سنوي")) {
            start = initial.with(firstDayOfYear());
            end = initial.with(lastDayOfYear());
        }
        LocalDate[] dateValues = {start, end};

        return dateValues;

    }

    public int[] getFullDate() {
        Date date = new Date();
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int year = localDate.getYear();
        int month = localDate.getMonthValue();
        int day = localDate.getDayOfMonth();
        int[] fullDate = {year, month, day};
        return fullDate;
    }

    public void returnWhereBillsItem(Component[] comp, String tablename) {
//
//        String where = "";
//        Component c = null;
//
//        JTextField f = (JTextField) comp[0];
//        JTextField f1 = (JTextField) comp[1];
//        //JComboBox c = (JComboBox) comp[2];
//        for (int i = 0; i < comp.length; i++) {
//            c = cmpfunc.
//        }
//        if (!f.getText().isEmpty()) {
//            where = " " + tablename + "`كود` like '%" + f.getText() + "%' ";
//        }
//        if (!f1.getText().isEmpty()) {
//            where = where.isEmpty() ? " " + tablename + "`الاسم` like '%" + f1.getText() + "%' "
//                    : where + " and " + tablename + "`الاسم` like '%" + f1.getText() + "%' ";
//
//        }
//
//        if (c.getSelectedIndex() != -1 && c.getSelectedIndex() != 0) {
//            if (c.getSelectedIndex() != 0) {
//                where = where.isEmpty() ? " " + tablename + "`صنف` like '" + c.getSelectedItem().toString() + "'"
//                        : where + " and " + tablename + "`صنف` like '" + c.getSelectedItem().toString() + "'";
//            }
////            if (where.isEmpty()) {
////                System.out.println(where);
////                where = " " + tablename + "`صنف` like '" + c.getSelectedItem().toString() + "'";
////            } else {
////                System.out.println("this is where with : " + where);
////                where = where + " and " + tablename + "`صنف` like '" + c.getSelectedItem().toString() + "'";
////            }
//        }
//        return where;
    }

    public Vector CheckSelectedLabelInPanel(JPanel pan) {
        Vector selectedLabel = new Vector();
        Component[] cpanel = null;
        Color labelcolor = null;
        String index = "-1";
        cpanel = pan.getComponents();

        for (int i = 0; i < cpanel.length; i++) {
            selectedLabel.removeAllElements();
            if (cpanel[i] instanceof JLabel) {

                JLabel jl = (JLabel) cpanel[i];
                labelcolor = jl.getForeground();
                Border bd = jl.getBorder();

                if (labelcolor.getRGB() == -65536
                        || bd.equals(BorderFactory.createBevelBorder(BevelBorder.LOWERED))) {
                    index = jl.getName();
                    selectedLabel.add(index);
                    selectedLabel.add(jl);
                    break;
                }
                selectedLabel.add(index);
                selectedLabel.add(jl);
            }

        }

        return selectedLabel;
    }

    public Boolean CheckSelectedLabel(String selectItemIndex, String IndexOfItemToSelect) {
        Boolean deselect = false;
//        System.out.println(selectItemIndex + " and " + IndexOfItemToSelect);
        if (!selectItemIndex.equals(-1) || !IndexOfItemToSelect.equals(-1)) {
            if (!selectItemIndex.equals(IndexOfItemToSelect)) {
                deselect = true;
            }
        }
        return deselect;
    }

    public void SelectLabelinPanel(JLabel labelSelected, JLabel labeltoSelect, boolean state) {

        if (state) {
            labelSelected.setForeground(Color.WHITE);
            labeltoSelect.setForeground(Color.red);
        } else {
            labeltoSelect.setForeground(Color.red);
        }
    }

    public void SelectLabelBorderinPanel(JLabel labelSelected, JLabel labeltoSelect, boolean state) {
        if (state) {
            labelSelected.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
            labeltoSelect.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        } else {
            labeltoSelect.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        }
    }

    public void LabelSelection(Vector v1, JPanel panel) {
        Vector v = CheckSelectedLabelInPanel(panel);
        boolean state = CheckSelectedLabel(v.get(0).toString(), v1.get(0).toString());
        SelectLabelinPanel((JLabel) v.get(1), (JLabel) v1.get(1), state);
    }

    public void LabelBorderSelection(Vector v1, JPanel panel) {
        Vector v = CheckSelectedLabelInPanel(panel);
        boolean state = CheckSelectedLabel(v.get(0).toString(), v1.get(0).toString());
        SelectLabelBorderinPanel((JLabel) v.get(1), (JLabel) v1.get(1), state);
    }

}
