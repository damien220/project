package Sabbah;

import Functions.MySqlBackup;
import Functions.dbFunc;
import Functions.utilityclass;
import com.mysql.jdbc.Connection;
import java.awt.HeadlessException;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Damien
 */
public class Backup {

    private Connection conn = null;
    private final dbFunc dbfunc;

    private final utilityclass utility = new utilityclass();
    private final MySqlBackup sqlbck = new MySqlBackup();
    private final String dbfilename = "dbcreds.config";

    public Backup() {
        conn = utility.connect2DB(dbfilename);
        dbfunc = new dbFunc(conn);
    }

    private String[] checkOSname() {
        String mySQLdumpPath = "";
        String mySQLPath = "";
        String OSName = System.getProperty("os.name").toLowerCase();
        if (OSName.contains("mac")) {
            mySQLdumpPath = "/Applications/MAMP/Library/bin/mysqldump";
            mySQLPath = "/Applications/MAMP/Library/bin/mysql";
        } else if (OSName.contains("windows")) {
            mySQLdumpPath = "C:\\wamp\\bin\\mysql\\mysql5.6.17\\bin\\mysqldump.exe";
            mySQLPath = "C:\\wamp\\bin\\mysql\\mysql5.6.17\\bin\\mysql.exe";
        }
        String[] mySQLTools = {mySQLdumpPath, mySQLPath};
        return mySQLTools;
    }

    private String mysqlAction(String mySQLAction) {
        String action = "";
        String[] dumpExePath = checkOSname();
        switch (mySQLAction) {
            case "backup":
                action = dumpExePath[0];
                break;
            case "restore":
                action = dumpExePath[1];
                break;
            default:
                break;
        }
        return action;
    }

    private void credentialForBackup(String backupPath, String mySQLAction) {
        String[] fi = {dbfilename};
        String[] fields = null;
        String[] titles = {"username", "password", "ipaddress", "dbname", "port"};

        boolean check = utility.checkEmptyFields(fi);
        if (check) {
            fields = utility.readProperties(titles, dbfilename);

            String user = fields[0];
            String password = fields[1];
            String dbip = fields[2];
            String dbname = fields[3];
            String port = fields[4];
            try {
                boolean done = sqlbck.backupDataWithDatabase(mysqlAction(mySQLAction),
                        dbip, port, user, password, dbname, backupPath + "/");
                if (done) {
                    JOptionPane.showMessageDialog(null, "تمت العملية");
                } else {
                    JOptionPane.showMessageDialog(null, "فشلت العملية");
                }
            } catch (HeadlessException e) {
                e.printStackTrace();
            }

        }
    }

    public void doBackup(JTextField textField, String mySQLAction) {
        String backuppath = textField.getText();
        if (!backuppath.isEmpty()) {
            credentialForBackup(backuppath, mySQLAction);
        } else {
            JOptionPane.showMessageDialog(null, "يجب اختيار المسار ");
        }
    }

    public void dorestore(JTextField textField, String mySQLAction) {
        String restorepath = textField.getText();
        if (!restorepath.isEmpty()) {
            credentialForBackup(restorepath, mySQLAction);
        } else {
            JOptionPane.showMessageDialog(null, "يجب اختيار المسار ");
        }
    }

}
