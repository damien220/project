-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2017 at 04:46 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sabbah`
--

-- --------------------------------------------------------

--
-- Table structure for table `bill_local`
--

CREATE TABLE IF NOT EXISTS `bill_local` (
  `رقم الفاتورة` int(11) NOT NULL AUTO_INCREMENT,
  `الاسم` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `الهاتف` int(8) NOT NULL,
  `التاريخ` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `تاريخ` date NOT NULL,
  `المجموع` float NOT NULL,
  `حسم` int(6) NOT NULL,
  `مجموع مع حسم` float NOT NULL,
  `مدفوع` float NOT NULL,
  `متبقي` float NOT NULL,
  `الحالة` tinyint(1) NOT NULL,
  `تفاصيل` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`رقم الفاتورة`),
  FULLTEXT KEY `details` (`تفاصيل`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bill_local`
--

INSERT INTO `bill_local` (`رقم الفاتورة`, `الاسم`, `الهاتف`, `التاريخ`, `تاريخ`, `المجموع`, `حسم`, `مجموع مع حسم`, `مدفوع`, `متبقي`, `الحالة`, `تفاصيل`) VALUES
(1, 'dsags', 32235, 'Tue Oct 172017', '0000-00-00', 0, 0, 0, 0, 0, 1, NULL),
(2, 'sdfsdg', 34545, 'Wed Oct 182017', '2017-10-18', 10, 0, 10, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bill_local_items`
--

CREATE TABLE IF NOT EXISTS `bill_local_items` (
  `كود` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `صنف` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `الاسم` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `كمية` int(6) NOT NULL,
  `السعر` float NOT NULL,
  `رقم الفاتورة` int(11) NOT NULL,
  KEY `serial` (`كود`),
  KEY `serial_2` (`كود`),
  KEY `serial_3` (`كود`),
  KEY `serial_4` (`كود`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bill_local_items`
--

INSERT INTO `bill_local_items` (`كود`, `صنف`, `الاسم`, `كمية`, `السعر`, `رقم الفاتورة`) VALUES
('3ef3', 't1', 'asdewf', 11, 2, 1),
('3ef3', 't2', 'sdfsdg', 6, 2, 1),
('3ef3', 't1', 'asdewf', 3, 2, 2),
('DSWR', 't2', 'dsfdsg', 2, 2, 2),
('3ef3', 't1', 'asdewf', 3, 2, 2),
('DSWR', 't2', 'dsfdsg', 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `bill_trader`
--

CREATE TABLE IF NOT EXISTS `bill_trader` (
  `رقم الفاتورة` varchar(50) CHARACTER SET utf8 NOT NULL,
  `الاسم` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `الهاتف` int(8) NOT NULL,
  `التاريخ` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `تاريخ` date NOT NULL,
  `المجموع` float(10,3) NOT NULL,
  `مدفوع` float(10,3) NOT NULL,
  `متبقي` float(10,3) NOT NULL,
  `الحالة` tinyint(1) NOT NULL,
  `تفاصيل` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`رقم الفاتورة`),
  UNIQUE KEY `Bill_trader_id` (`رقم الفاتورة`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bill_trader`
--

INSERT INTO `bill_trader` (`رقم الفاتورة`, `الاسم`, `الهاتف`, `التاريخ`, `تاريخ`, `المجموع`, `مدفوع`, `متبقي`, `الحالة`, `تفاصيل`) VALUES
('a1', 'asd', 1235456, 'Tue Oct 172017', '0000-00-00', 10.000, 10.000, 0.000, 1, NULL),
('a10', 'efdcfw', 32432, 'Sun Oct 222017', '2017-10-22', 0.007, 0.000, 0.007, 0, 'null'),
('a11', 'asdfasf23', 23423, 'Sun Oct 222017', '2017-10-22', 10.000, 0.000, 10.000, 0, NULL),
('a12', 'fsdf', 23432, 'Sun Oct 222017', '2017-10-22', 10.000, 0.000, 10.000, 0, NULL),
('a13', 'sdsdfsd', 324234, 'Sun Oct 222017', '2017-10-22', 0.007, 0.000, 0.007, 0, NULL),
('a14', 'dfsf', 3423, 'Sun Oct 222017', '2017-10-22', 0.007, 0.000, 0.007, 0, NULL),
('a15', 'sdfsr', 2423, 'Sun Oct 222017', '2017-10-22', 0.007, 0.000, 0.007, 0, 'null'),
('a2', 'rgfr', 42323, 'Tue Oct 172017', '0000-00-00', 10.000, 5.000, 5.000, 0, NULL),
('a3', 'sdfsg', 234534, 'Wed Oct 182017', '0000-00-00', 20.000, 10.000, 10.000, 0, 'dsfdsgdg'),
('a4', 'sdafsd', 325434, 'Wed Oct 182017', '2017-10-18', 210.000, 10.000, 200.000, 0, 'dsfdsf'),
('a55', 'sdfsd', 45343, 'Tue Oct 032017', '2017-10-03', 10.000, 5.000, 5.000, 0, NULL),
('a6', 'fdfgdsf', 53453, 'Thu Oct 192017', '2017-10-19', 20.000, 10.000, 10.000, 0, NULL),
('a7', 'dsfdsg', 234435, 'Wed Oct 182017', '2017-10-18', 10.000, 0.000, 10.000, 0, 'fvsxfvsdfsdf'),
('a8', 'efdsf3', 234234, 'Sun Oct 222017', '2017-10-22', 10.000, 0.000, 10.000, 0, NULL),
('a9', 'df3r3w', 3243654, 'Sun Oct 222017', '2017-10-22', 0.007, 0.000, 0.007, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bill_trader_items`
--

CREATE TABLE IF NOT EXISTS `bill_trader_items` (
  `كود` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `صنف` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `الاسم` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `الوحدة` char(10) COLLATE utf8_unicode_ci NOT NULL,
  `الكمية` int(6) NOT NULL,
  `سعر القطعة` float NOT NULL,
  `المجموع` float NOT NULL,
  `حسم` int(6) NOT NULL,
  `مجموع مع الحسم` float NOT NULL,
  `تفاصيل` mediumtext COLLATE utf8_unicode_ci,
  `رقم الفاتورة` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  KEY `serial` (`كود`),
  KEY `serial_2` (`كود`),
  KEY `serial_3` (`كود`),
  KEY `serial_4` (`كود`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bill_trader_items`
--

INSERT INTO `bill_trader_items` (`كود`, `صنف`, `الاسم`, `الوحدة`, `الكمية`, `سعر القطعة`, `المجموع`, `حسم`, `مجموع مع الحسم`, `تفاصيل`, `رقم الفاتورة`) VALUES
('3ef3', 't1', 'asdewf', 'u', 6, 2, 10, 0, 10, NULL, 'a1'),
('3ef3', 't2', 'sdfsdg', 'u1', 6, 2, 10, 0, 10, NULL, 'a1'),
('423d', 't2', 'essdgd', 'u1', 5, 2, 10, 0, 10, NULL, 'a1'),
('4gfd', 't1', 'sdgdsfg', 'u', 5, 2, 10, 0, 10, NULL, 'a2'),
('DSWR', 't2', 'dsfdsg', 'u1', 5, 2, 10, 0, 10, NULL, 'a2'),
('3ef3', 't1', 'asdewf', 'u1', 6, 1.66667, 10, 0, 10, NULL, 'a2'),
('sf3r3', 't1', 'sdfsdf', 'u', 6, 2, 10, 0, 10, NULL, 'a6');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE IF NOT EXISTS `expenses` (
  `رقم الفاتورة` int(11) NOT NULL AUTO_INCREMENT,
  `فاتورة` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `الاسم` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `الهاتف` int(8) DEFAULT NULL,
  `التاريخ` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `تاريخ` date NOT NULL,
  `المجموع` float NOT NULL,
  `مدفوع` float NOT NULL,
  `متبقي` float NOT NULL,
  `الحالة` tinyint(1) NOT NULL,
  `تفاصيل` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`رقم الفاتورة`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`رقم الفاتورة`, `فاتورة`, `الاسم`, `الهاتف`, `التاريخ`, `تاريخ`, `المجموع`, `مدفوع`, `متبقي`, `الحالة`, `تفاصيل`) VALUES
(1, 'dsgse', 'ftgewr', 21323, 'Tue Oct 17 00:00:00 EEST 2017', '0000-00-00', 20, 20, 0, 1, NULL),
(2, 'sfgdsf', 'sdgsfdg', 542353, 'Wed Oct 182017', '2017-10-18', 10, 0, 10, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `items_qty`
--

CREATE TABLE IF NOT EXISTS `items_qty` (
  `كود` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `صنف` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `كمية` int(9) NOT NULL,
  `كمية متبقية` int(6) NOT NULL,
  `كمية البيع` int(9) NOT NULL DEFAULT '0',
  UNIQUE KEY `serial` (`كود`,`صنف`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items_qty`
--

INSERT INTO `items_qty` (`كود`, `صنف`, `كمية`, `كمية متبقية`, `كمية البيع`) VALUES
('3ef3', 't1', 24, 10, 14),
('3ef3', 't2', 18, 12, 6),
('423d', 't2', 15, 15, 0),
('4gfd', 't1', 15, 15, 0),
('DSWR', 't2', 15, 13, 2),
('sf3r3', 't1', 6, 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hash` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `hardware_hash` varchar(200) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `type`) VALUES
(1, 't1'),
(2, 't2');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE IF NOT EXISTS `unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `unit`) VALUES
(1, 'u'),
(2, 'u1');

-- --------------------------------------------------------

--
-- Table structure for table `year`
--

CREATE TABLE IF NOT EXISTS `year` (
  `id` int(11) NOT NULL,
  `year` smallint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
